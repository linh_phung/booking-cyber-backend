﻿using CyberBook_API.Dal.Repositories;
using CyberBook_API.Enum;
using CyberBook_API.Interfaces;
using CyberBook_API.Models;
using CyberBook_API.Services.Interfaces;
using CyberBook_API.ViewModel.RateUserViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CyberBook_API.Services
{
    public class CyberRateUserService : ICyberRateUserService
    {
        private readonly ICyberRateUserRepository _cyberRateUserRepository = new CyberRateUserRepository();
        private readonly ICybersRepository _cybersRepository = new CybersRepository();
        private readonly IUsersRepository _usersRepository = new UsersRepository();
        private readonly IAccountsRepository _accountsRepository = new AccountsRepository();

        public async Task<ServiceResponse<RateUserOutViewModel>> AddNewRateUser(string usernameCrr, RatingUser ratingUser)
        {
            var output = new ServiceResponse<RateUserOutViewModel>();
            try
            {
                var accountCrr = await _accountsRepository.GetAccountByUsername(usernameCrr);
                if (accountCrr != null)
                {
                    var cyber = await _cybersRepository.GetCyberById(ratingUser.CyberId);
                    if (cyber != null)
                    {
                        var currentUser = await _usersRepository.GetUserByAccountID(accountCrr.Id);
                        //check user hiện tại có quyền trong Cyber này không?
                        if ((await _cybersRepository.IsBossCyber(currentUser.Id, cyber.Id)))
                        {
                            if (ratingUser.CyberId != null && ratingUser.UsersId != null)
                            {
                                //user được rate có tồn tại không?
                                var user = await _usersRepository.GetUserByUserID(ratingUser.UsersId);
                                if (cyber != null && user != null)
                                {
                                    var rating = await _cyberRateUserRepository.RateUserExist(ratingUser.CyberId, ratingUser.UsersId, ratingUser.OrderId);
                                    if (rating == null)
                                    {
                                        var rcRateUser = new RatingUser
                                        {
                                            CyberId = ratingUser.CyberId,
                                            RatePoint = ratingUser.RatePoint,
                                            CommentContent = ratingUser.CommentContent,
                                            UsersId = ratingUser.UsersId,
                                            CreatedDate = DateTime.Now,
                                            UpdatedDate = DateTime.Now,
                                            Edited = false,
                                            OrderId = ratingUser.OrderId
                                        };
                                        var newRating = await _cyberRateUserRepository.Create(rcRateUser);
                                        if (newRating != null)
                                        {
                                            var lstRate = (await _cyberRateUserRepository.FindBy(x => x.UsersId == user.Id)).ToList();
                                            double sum = 0;
                                            int count = 0;
                                            foreach (var x in lstRate)
                                            {
                                                if (x.RatePoint != 0)
                                                {
                                                    sum += x.RatePoint;
                                                }
                                                count++;
                                            }
                                            user.RatingPoint = sum / count;
                                            await _usersRepository.Update(user, user.Id);

                                            var rateOut = new RateUserOutViewModel
                                            {
                                                RateId = newRating.Id,
                                                UsersId = newRating.UsersId,
                                                RatePoint = newRating.RatePoint,
                                                CommentContent = newRating.CommentContent,
                                                CyberId = newRating.CyberId,
                                                CreatedDate = Convert.ToDateTime(newRating.CreatedDate).ToString("HH:mm:ss dd/MM/yyyy"),
                                                UpdatedDate = Convert.ToDateTime(newRating.UpdatedDate).ToString("HH:mm:ss dd/MM/yyyy"),
                                                Edited = newRating.Edited,
                                                OrderId = newRating.OrderId,
                                            };


                                            output.Data = rateOut;
                                            output.Success = true;
                                            output.Message = "Create new Rating Successfull";
                                            return output;
                                        }
                                        output.Message = "Create new rating Failed";
                                        return output;
                                    }
                                    output.Message = "Rating has existing";
                                    return output;
                                }
                                output.Message = "Cyber or User not exist";
                                return output;
                            }
                            output.Message = "CyberId || UsersId not allow Null ";
                            return output;
                        }
                        output.Message = "You NOT Permission this Function";
                        return output;
                    }
                    output.Message = "Cyber NOT exist";
                    return output;
                }
                output.Message = "You NOT LOGIN";
                return output;
            }
            catch (Exception e)
            {
                output.Message = "Rating User Exception: " + e.Message;
            }
            return output;
        }

        public async Task<ServiceResponse<RateUserOutViewModel>> GetRateById(int rateId)
        {
            var output = new ServiceResponse<RateUserOutViewModel>();
            try
            {
                var rating = await _cyberRateUserRepository.GetRateUserById(rateId);
                if (rating != null)
                {
                    var rateOut = new RateUserOutViewModel
                    {
                        RateId = rating.Id,
                        UsersId = rating.UsersId,
                        RatePoint = rating.RatePoint,
                        CommentContent = rating.CommentContent,
                        CyberId = rating.CyberId,
                        CreatedDate = Convert.ToDateTime(rating.CreatedDate).ToString("HH:mm:ss dd/MM/yyyy"),
                        UpdatedDate = Convert.ToDateTime(rating.UpdatedDate).ToString("HH:mm:ss dd/MM/yyyy"),
                        Edited = rating.Edited,
                        OrderId = rating.OrderId,
                    };

                    output.Data = rateOut;
                    output.Success = true;
                    output.Message = "Successfull";
                    return output;
                }
                output.Message = "Rating not exist";
                return output;
            }
            catch (Exception e)
            {
                output.Message = "UpdateRating Exception: " + e.Message;
            }
            return output;
        }

        public async Task<ServiceResponse<string>> RemoveRateById(string usernameCrr, int rateId)
        {
            var output = new ServiceResponse<string>();
            try
            {
                var accountCrr = await _accountsRepository.GetAccountByUsername(usernameCrr);
                if (accountCrr != null)
                {
                    var currentUser = await _usersRepository.GetUserByAccountID(accountCrr.Id);
                    if(currentUser.RoleId == Convert.ToInt32(UsersEnum.UserRole.CyberManager))
                    {
                        var cyber = await _cybersRepository.GetCyberByBossCyberId(currentUser.Id);
                        var rateRecord = await _cyberRateUserRepository.GetRateUserById(rateId);
                        if (rateRecord.CyberId == cyber.Id)
                        {
                            if ((await _cyberRateUserRepository.Delete(rateRecord)) != -1)
                            {
                                var lstRateOfUser = (await _cyberRateUserRepository.FindBy(x => x.UsersId == currentUser.Id)).ToList();
                                double sum = 0;
                                int count = 0;
                                foreach (var x in lstRateOfUser)
                                {
                                    if (x.RatePoint != 0)
                                    {
                                        sum += x.RatePoint;
                                    }
                                    count++;
                                }
                                currentUser.RatingPoint = sum / count;
                                await _usersRepository.Update(currentUser, currentUser.Id);

                                output.Message = "Successful";
                                output.Success = true;
                                return output;
                            }
                            output.Message = "Remove Rate Record FAIL";
                            return output;
                        }
                        output.Message = "You NOT Permission for this Function";
                        return output;
                    }
                    output.Message = "You NOT a CyberAdmin";
                    return output;
                }
                output.Message = "You NOT LOGIN";
                return output;
            }
            catch (Exception e)
            {
                output.Message = "Remove RateUser record exception: " + e.Message;
            }
            return output;
        }

        public async Task<ServiceResponse<RateUserOutViewModel>> UpdateRateUser(string usernameCrr, RatingUser ratingUser)
        {
            var output = new ServiceResponse<RateUserOutViewModel>();
            try
            {
                var accountCrr = await _accountsRepository.GetAccountByUsername(usernameCrr);
                if (accountCrr != null)
                {
                    var currentUser = await _usersRepository.GetUserByAccountID(accountCrr.Id);
                    var cyber = await _cybersRepository.GetCyberById(ratingUser.CyberId);
                    if (cyber != null)
                    {
                        //check user hiện tại có quyền trong Cyber này không?
                        if ((await _cybersRepository.IsBossCyber(currentUser.Id, cyber.Id)))
                        {
                            var user = await _usersRepository.GetUserByUserID(ratingUser.UsersId);
                            if (cyber != null && user != null)
                            {
                                var rating = await _cyberRateUserRepository.RateUserExist(ratingUser.CyberId, ratingUser.UsersId, ratingUser.OrderId);
                                if (rating == null)
                                {
                                    rating.RatePoint = ratingUser.RatePoint;
                                    rating.CommentContent = ratingUser.CommentContent;
                                    rating.UpdatedDate = DateTime.Now;
                                    rating.Edited = true;
                                    var rs = await _cyberRateUserRepository.Update(rating, rating.Id);
                                    if (rs != -1)
                                    {
                                        var lstRate = (await _cyberRateUserRepository.FindBy(x => x.UsersId == user.Id)).ToList();
                                        double sum = 0;
                                        int count = 0;
                                        foreach (var x in lstRate)
                                        {
                                            if (x.RatePoint != 0)
                                            {
                                                sum += x.RatePoint;
                                            }
                                            count++;
                                        }
                                        user.RatingPoint = sum / count;
                                        await _usersRepository.Update(user, user.Id);

                                        var rateOut = new RateUserOutViewModel
                                        {
                                            RateId = rating.Id,
                                            UsersId = rating.UsersId,
                                            RatePoint = rating.RatePoint,
                                            CommentContent = rating.CommentContent,
                                            CyberId = rating.CyberId,
                                            CreatedDate = Convert.ToDateTime(rating.CreatedDate).ToString("HH:mm:ss dd/MM/yyyy"),
                                            UpdatedDate = Convert.ToDateTime(rating.UpdatedDate).ToString("HH:mm:ss dd/MM/yyyy"),
                                            Edited = rating.Edited,
                                            OrderId = rating.OrderId,
                                        };

                                        output.Data = rateOut;
                                        output.Success = true;
                                        output.Message = "Update Successfull";
                                        return output;
                                    }
                                    output.Message = "Update Failed";
                                    return output;
                                }
                                output.Message = "Rating not exist";
                                return output;
                            }
                            output.Message = "User or Cyber not exist";
                            return output;
                        }
                        output.Message = "You NOT Permission this Function";
                        return output;
                    }
                    output.Message = "Cyber NOT exist";
                    return output;
                }
                output.Message = "You NOT LOGIN";
                return output;
            }
            catch (Exception e)
            {
                output.Message = "UpdateRating Exception: " + e.Message;
            }
            return output;
        }
    }
}
