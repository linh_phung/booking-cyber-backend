﻿using CyberBook_API.Dal.Repositories;
using CyberBook_API.Interfaces;
using CyberBook_API.Models;
using CyberBook_API.Services.Interfaces;
using CyberBook_API.ViewModel.PagingView;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CyberBook_API.Services
{
    public class AdminUsersService : IAdminUsersService
    {
        private readonly IUsersRepository _usersRepository = new UsersRepository();
        private readonly IAccountsRepository _accountsRepository = new AccountsRepository();
        private readonly IAdminsRepository _adminsRepository = new AdminsRepository();


        public async Task<PagingOutput<IEnumerable<Users>>> ListUser(string usernameCrr, int pageIndex, int pageSize)
        {
            var output = new PagingOutput<IEnumerable<Users>>();
            try
            {
                var accountCrr = await _accountsRepository.GetAccountByUsername(usernameCrr);
                if (accountCrr != null)
                {
                    var currentUser = await _usersRepository.GetUserByAccountID(accountCrr.Id);
                    if ((await _adminsRepository.IsSuperAdmin(currentUser.Id)))
                    {
                        var users = await _usersRepository.SearchAllUser(pageIndex, pageSize);
                        if (users != null)
                        {
                            output = users;
                            output.Message = "Success";
                            return output;
                        }
                        output.Message = "No user";
                        return output;
                    }
                    output.Message = "YOU NOT PERMISSTION for this Function";
                    return output;
                }
                output.Message = "YOU NOT LOGIN";
                return output;
            }
            catch (Exception ex)
            {
                output.Message = "SearchUserByName Exception: " + ex.Message;
            }
            return output;
        }

        public async Task<ServiceResponse<Users>> LockUserById(string usernameCrr, int userId)
        {
            var output = new ServiceResponse<Users>();
            var adminsRepo = new AdminsRepository();
            
            try
            {
                var accountCrr = await _accountsRepository.GetAccountByUsername(usernameCrr);
                if (accountCrr != null)
                {
                    var currentUser = await _usersRepository.GetUserByAccountID(accountCrr.Id);
                    if ((await _adminsRepository.IsSuperAdmin(currentUser.Id)))
                    {
                        var result = await _adminsRepository.AdminLockUserById(userId);
                        if (result != null)
                        {
                            output.Data = result;
                            output.Message = "Block Successfull";
                            output.Success = true;

                            string messageBody = "<p> Dear anh/chị " + result.Fullname + ", <p>"
                + "<p>Tài khoản của bạn đã bị khóa do nhận được quá nhiều lượt đánh giá thấp nên chúng tôi buộc lòng phải tạm dừng việc hoạt động của bạn ở gocyber.xyz để giữ cho cộng đồng văn minh, trong sạch.</p>"
                + "Mọi phản hồi, thắc mắc xin vui lòng liên hệ:"
                + "<p>0846846886 – Mr.Đăng – contact@gocyber.xyz</p>";
                            string mailTo = result.Email;
                            string title = "[ GoCyber ] Thông báo về việc tài khoản bị khóa";
                            await adminsRepo.SendMail(messageBody, mailTo, title);
                            return output;
                        }
                        output.Message = "Block FAIL";
                        return output;
                    }
                    output.Message = "YOU NOT PERMISSTION for this Function";
                    return output;
                }
                output.Message = "YOU NOT LOGIN";
                return output;
            }
            catch (Exception ex)
            {
                output.Message = "SearchUserByName Exception: " + ex.Message;
            }
            return output;
        }

        public async Task<ServiceResponse<Users>> UnLockUserById(string usernameCrr, int userId)
        {
            var output = new ServiceResponse<Users>();
            var adminsRepo = new AdminsRepository();
            try
            {
                var accountCrr = await _accountsRepository.GetAccountByUsername(usernameCrr);
                if (accountCrr != null)
                {
                    var currentUser = await _usersRepository.GetUserByAccountID(accountCrr.Id);
                    if ((await _adminsRepository.IsSuperAdmin(currentUser.Id)))
                    {
                        var result = await _adminsRepository.AdminUnLockUserById(userId);
                        if (result != null)
                        {
                            output.Data = result;
                            output.Message = "Unlock Successfull";
                            output.Success = true;

                            string messageBody = "<p> Dear anh/chị " + result.Fullname + ", <p>"
                + "<p>Tài khoản của bạn đã được mở cho việc hoạt động của bạn ở gocyber.xyz.</p>"
                + "Mọi phản hồi, thắc mắc xin vui lòng liên hệ:"
                + "<p>0846846886 – Mr.Đăng – contact@gocyber.xyz</p>";
                            string mailTo = result.Email;
                            string title = "[ GoCyber ] Thông báo về việc tài khoản bị khóa";
                            await adminsRepo.SendMail(messageBody, mailTo, title);
                            return output;
                        }
                        output.Message = "UnLock FAIL";
                        return output;
                    }
                    output.Message = "YOU NOT PERMISSTION for this Function";
                    return output;
                }
                output.Message = "YOU NOT LOGIN";
                return output;
            }
            catch (Exception ex)
            {
                output.Message = "SearchUserByName Exception: " + ex.Message;
            }
            return output;
        }

        public async Task<PagingOutput<IEnumerable<Users>>> SearchUserByName(string usernameCrr, int pageIndex, int pageSize, string username)
        {
            var output = new PagingOutput<IEnumerable<Users>>();
            try
            {
                var accountCrr = await _accountsRepository.GetAccountByUsername(usernameCrr);
                if (accountCrr != null)
                {
                    var currentUser = await _usersRepository.GetUserByAccountID(accountCrr.Id);
                    if ((await _adminsRepository.IsSuperAdmin(currentUser.Id)))
                    {
                        var users = await _usersRepository.SearchAllUserByUsername(username, pageIndex, pageSize);
                        if (users != null)
                        {
                            output = users;
                            output.Message = "Success";
                            return output;
                        }
                        output.Message = "No user";
                        return output;
                    }
                    output.Message = "YOU NOT PERMISSTION for this Function";
                    return output;
                }
                output.Message = "YOU NOT LOGIN";
                return output;
            }
            catch (Exception ex)
            {
                output.Message = "SearchUserByName Exception: " + ex.Message;
            }
            return output;
        }

        public async Task<ServiceResponse<Users>> ViewDetailUser(string usernameCrr, int userId)
        {
            var output = new ServiceResponse<Users>();
            try
            {
                var accountCrr = await _accountsRepository.GetAccountByUsername(usernameCrr);
                if (accountCrr != null)
                {
                    var currentUser = await _usersRepository.GetUserByAccountID(accountCrr.Id);
                    if ((await _adminsRepository.IsSuperAdmin(currentUser.Id)))
                    {
                        var detailUser = await _usersRepository.GetUserByUserID(userId);
                        if (detailUser != null)
                        {
                            output.Data = detailUser;
                            output.Message = "Success";
                            output.Success = true;
                            return output;
                        }
                        output.Message = "can not find";
                        return output;
                    }
                    output.Message = "YOU NOT PERMISSTION for this Function";
                    return output;
                }
                output.Message = "YOU NOT LOGIN";
                return output;
            }
            catch (Exception ex)
            {
                output.Message = "SearchUserByName Exception: " + ex.Message;
            }
            return output;
        }
    }
}
