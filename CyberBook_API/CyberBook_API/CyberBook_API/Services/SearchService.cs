﻿using CyberBook_API.Dal.Repositories;
using CyberBook_API.Enum;
using CyberBook_API.Interfaces;
using CyberBook_API.Models;
using CyberBook_API.Services.Interfaces;
using CyberBook_API.ViewModel.CyberViewModel;
using CyberBook_API.ViewModel.PagingView;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CyberBook_API.Services
{
    public class SearchService : ISearchService
    {
        private readonly ICybersRepository _cybersRepository = new CybersRepository();
        private readonly ISlotsRepository _slotsRepository = new SlotsRepository();
        private readonly IRoomRepository _roomRepository = new RoomRepository();

        public async Task<PagingOutput<IEnumerable<CyberViewModelOut>>> GetAllCybers(int pageIndex, int pageSize)
        {
            var output = new PagingOutput<IEnumerable<CyberViewModelOut>>();
            try
            {
                var result = await _cybersRepository.GetAllCyberAvaiable(pageIndex, pageSize);
                if (result != null)
                {
                    var lstCyberOut = new List<CyberViewModelOut>();

                    foreach (var x in result.Data)
                    {
                        var lstRoomInCyber = await _roomRepository.GetListRoomByCyberId(x.Id);
                        int countSlotReadyInCyber = 0;
                        foreach (var s in lstRoomInCyber)
                        {
                            countSlotReadyInCyber += await _slotsRepository.CountSlotReadyByRoomId(s.Id);
                        }


                        var cyberOut = new CyberViewModelOut
                        {
                            CyberId = x.Id,
                            Address = x.Address,
                            BossCyberID = x.BossCyberID,
                            BossCyberName = x.BossCyberName,
                            BusinessLicense = x.BusinessLicense,
                            CreatedDate = x.CreatedDate.ToString("HH:mm:ss dd/MM/yyyy"),
                            CyberDescription = x.CyberDescription,
                            CyberName = x.CyberName,
                            image = x.image,
                            lat = x.lat,
                            lng = x.lng,
                            PhoneNumber = x.PhoneNumber,
                            RatingPoint = Convert.ToDouble(String.Format("{0:0.0}", x.RatingPoint / 2)),
                            status = x.status,
                            SlotReady = countSlotReadyInCyber
                        };
                        lstCyberOut.Add(cyberOut);
                    }

                    output.Message = "Success";
                    output.Index = result.Index;
                    output.PageSize = result.PageSize;
                    output.TotalItem = result.TotalItem;
                    output.TotalPage = result.TotalPage;
                    output.Data = lstCyberOut;
                    return output;
                }
                output.Message = "No Cyber";
                return output;
            }
            catch (Exception e)
            {
                output.Message = "Exception get All Cyber: " + e.Message;
            }
            return output;
        }

        public async Task<ServiceResponse<CyberViewModelOut>> GetCyberById(int id)
        {
            var output = new ServiceResponse<CyberViewModelOut>();
            try
            {
                var cybers = await _cybersRepository.GetCyberById(id);
                if (cybers != null)
                {
                    var lstCyberOut = new List<CyberViewModelOut>();

                    var lstRoomInCyber = await _roomRepository.GetListRoomByCyberId(cybers.Id);
                    int countSlotReadyInCyber = 0;
                    foreach (var s in lstRoomInCyber)
                    {
                        countSlotReadyInCyber += await _slotsRepository.CountSlotReadyByRoomId(s.Id);
                    }

                    var cyberOut = new CyberViewModelOut
                    {
                        CyberId = cybers.Id,
                        Address = cybers.Address,
                        BossCyberID = cybers.BossCyberID,
                        BossCyberName = cybers.BossCyberName,
                        BusinessLicense = cybers.BusinessLicense,
                        CreatedDate = cybers.CreatedDate.ToString("HH:mm:ss dd/MM/yyyy"),
                        CyberDescription = cybers.CyberDescription,
                        CyberName = cybers.CyberName,
                        image = cybers.image,
                        lat = cybers.lat,
                        lng = cybers.lng,
                        PhoneNumber = cybers.PhoneNumber,
                        RatingPoint = Convert.ToDouble(String.Format("{0:0.0}", cybers.RatingPoint / 2)),
                        status = cybers.status,
                        SlotReady = countSlotReadyInCyber
                    };
                    lstCyberOut.Add(cyberOut);
                    output.Success = true;
                    output.Message = "Success";
                    output.Data = cyberOut;
                    return output;
                }
                output.Message = "No Cyber";
                return output;
            }
            catch (Exception e)
            {
                output.Message = "GetCyberById Exception: " + e.Message;
            }
            return output;
        }

        public async Task<PagingOutput<IEnumerable<CyberViewModelOut>>> SearchCyberByName(int pageIndex, int pageSize, string searchContent)
        {
            var output = new PagingOutput<IEnumerable<CyberViewModelOut>>();
            try
            {
                var result = new PagingOutput<IEnumerable<Cyber>>();
                if (string.IsNullOrWhiteSpace(searchContent))
                {
                    result = await _cybersRepository.GetAllCyberAvaiable(pageIndex, pageSize);
                }
                else
                {
                    result = await _cybersRepository.GetCyberByName(searchContent, pageIndex, pageSize);
                }

                if (result != null)
                {
                    var lstCyberOut = new List<CyberViewModelOut>();
                    foreach (var x in result.Data)
                    {
                        var lstRoomInCyber = await _roomRepository.GetListRoomByCyberId(x.Id);
                        if(lstRoomInCyber.Any())
                        {
                            int countSlotReadyInCyber = 0;
                            int slotExistInRoom = 0;
                            foreach (var s in lstRoomInCyber)
                            {
                                var slotInRoom = (await _slotsRepository.FindBy(x => x.RoomId == s.Id 
                                && x.StatusId != Convert.ToInt32(SlotsEnum.SlotStatus.Unload))).ToList();

                                if (slotInRoom.Any())
                                {
                                    slotExistInRoom = await _slotsRepository.CountAllSlotInRoomByRoomId(s.Id);
                                    countSlotReadyInCyber += await _slotsRepository.CountSlotReadyByRoomId(s.Id);
                                }
                            }
                            if (slotExistInRoom > 0)
                            {
                                var cyberOut = new CyberViewModelOut
                                {
                                    CyberId = x.Id,
                                    Address = x.Address,
                                    BossCyberID = x.BossCyberID,
                                    BossCyberName = x.BossCyberName,
                                    BusinessLicense = x.BusinessLicense,
                                    CreatedDate = x.CreatedDate.ToString("HH:mm:ss dd/MM/yyyy"),
                                    CyberDescription = x.CyberDescription,
                                    CyberName = x.CyberName,
                                    image = x.image,
                                    lat = x.lat,
                                    lng = x.lng,
                                    PhoneNumber = x.PhoneNumber,
                                    RatingPoint = Convert.ToDouble(String.Format("{0:0.0}", x.RatingPoint / 2)),
                                    status = x.status,
                                    SlotReady = countSlotReadyInCyber,
                                    CoverImage = x.CoverImage
                                };
                                lstCyberOut.Add(cyberOut);
                            }
                        }
                    }

                    output.Message = "Success";
                    output.Index = result.Index;
                    output.PageSize = result.PageSize;
                    output.TotalItem = result.TotalItem;
                    output.TotalPage = result.TotalPage;
                    output.Data = lstCyberOut;
                    return output;
                }
                output.Message = "No Cyber";
                return output;
            }
            catch (Exception e)
            {
                output.Message = "SearchCyberByName Exception: " + e.Message;
            }
            return output;
        }

        public async Task<PagingOutput<IEnumerable<CyberViewModelOut>>> SearchCyberByRate(int pageIndex, int pageSize)
        {
            var output = new PagingOutput<IEnumerable<CyberViewModelOut>>();
            try
            {
                var result = await _cybersRepository.GetCyberByRatePoint(pageIndex, pageSize);
                if (result != null)
                {
                    var lstCyberOut = new List<CyberViewModelOut>();
                    foreach (var x in result.Data)
                    {
                        var lstRoomInCyber = await _roomRepository.GetListRoomByCyberId(x.Id);
                        int countSlotReadyInCyber = 0;
                        foreach (var s in lstRoomInCyber)
                        {
                            countSlotReadyInCyber += await _slotsRepository.CountSlotReadyByRoomId(s.Id);
                        }

                        var cyberOut = new CyberViewModelOut
                        {
                            CyberId = x.Id,
                            Address = x.Address,
                            BossCyberID = x.BossCyberID,
                            BossCyberName = x.BossCyberName,
                            BusinessLicense = x.BusinessLicense,
                            CreatedDate = x.CreatedDate.ToString("HH:mm:ss dd/MM/yyyy"),
                            CyberDescription = x.CyberDescription,
                            CyberName = x.CyberName,
                            image = x.image,
                            lat = x.lat,
                            lng = x.lng,
                            PhoneNumber = x.PhoneNumber,
                            RatingPoint = Convert.ToDouble(String.Format("{0:0.0}", x.RatingPoint / 2)),
                            status = x.status,
                            SlotReady = countSlotReadyInCyber,
                            CoverImage = x.CoverImage
                        };
                        lstCyberOut.Add(cyberOut);
                    }

                    output.Message = "Success";
                    output.Index = result.Index;
                    output.PageSize = result.PageSize;
                    output.TotalItem = result.TotalItem;
                    output.TotalPage = result.TotalPage;
                    output.Data = lstCyberOut;
                    return output;
                }
                output.Message = "No Cyber";
                return output;
            }
            catch (Exception e)
            {
                output.Message = "Exception get All Cyber" + e.Message;
            }
            return output;
        }
    }
}
