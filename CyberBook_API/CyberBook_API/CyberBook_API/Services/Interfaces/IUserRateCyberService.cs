﻿using CyberBook_API.Models;
using CyberBook_API.ViewModel.PagingView;
using CyberBook_API.ViewModel.RateCybersViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CyberBook_API.Services.Interfaces
{
    public interface IUserRateCyberService
    {
        public Task<ServiceResponse<RateCyberOutViewModel>> AddUserRateCyber(string usernameCrr, RatingCyber rateCyber);
        public Task<ServiceResponse<RateCyberOutViewModel>> UpdateRateCyber(string usernameCrr, RatingCyber rateCyber);
        public Task<ServiceResponse<string>> RemoveRateById(string usernameCrr, int id);
        public Task<ServiceResponse<RateCyberOutViewModel>> GetRateCyberById(int id);
        public Task<PagingOutput<IEnumerable<RateCyberOutViewModel>>> GetAllRateByCyberId(int pageIndex, int pageSize, int cyberId);
    }
}
