﻿using CyberBook_API.Models;
using CyberBook_API.ViewModel.AccountView;
using CyberBook_API.ViewModel.AccountViewModel;
using CyberBook_API.ViewModel.TokenViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CyberBook_API.Services.Interfaces
{
    public interface IAuthenticationService
    {
        public Task<ServiceResponse<AccountViewModel>> Login(Account account);
        public Task<ServiceResponse<TokenViewModel>> GetToken(Account account);
        public Task<ServiceResponse<TokenViewModel>> RefreshToken(string username, string token);
        public Task<ServiceResponse<AccountViewModel>> Reigster(AccountRegisterViewModel accountRegisterIn);
        public Task<ServiceResponse<Users>> SendCodeResetPassword(string email);
        public Task<ServiceResponse<Account>> ConfirmPass(string email, string code);
        public Task<ServiceResponse<string>> SetPassword(int accountId, string pass);
        public Task<ServiceResponse<string>> ChangePassword(string user, string oldPass, string newPass);
    }
}
