﻿using CyberBook_API.Models;
using CyberBook_API.ViewModel.PagingView;
using CyberBook_API.ViewModel.SlotViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CyberBook_API.Services.Interfaces
{
    public interface ISlotsService
    {
        public Task<ServiceResponse<Slot>> CreateNewSlot(string usernameCrr, SlotCreateViewModelIn slotCreateViewModelIn);
        public Task<ServiceResponse<Slot>> EditSlot(string usernameCrr, SlotViewModelIn slotViewModelIn);
        public Task<ServiceResponse<int>> RemoveSlot(string usernameCrr, int slotId);
        public Task<ServiceResponse<Slot>> GetSlotById(int slotId);
        public Task<PagingOutput<IEnumerable<Slot>>> GetListSlotByRoom(int pageIndex, int pageSize, int roomId);
        public Task<ServiceResponse<Slot>> EditStatusSlot(string usernameCrr, int slotId, int statusSlot);
    }
}
