﻿using CyberBook_API.Models;
using CyberBook_API.ViewModel.OrderViewModel;
using CyberBook_API.ViewModel.PagingView;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CyberBook_API.Services.Interfaces
{
    public interface IManageBookingService
    {
        public Task<PagingOutput<IEnumerable<OrderViewModelOut>>> GetListBookingByCyberId(string usernameCrr, int pageIndex, int pageSize, int cyberId);
        public Task<PagingOutput<IEnumerable<OrderViewModelOut>>> GetMyListBooking(string usernameCrr, int pageIndex, int pageSize);
        public Task<ServiceResponse<OrderViewModelOut>> GetBookingById(string usernameCrr, int orderId);
        public Task<ServiceResponse<OrderViewModelOut>> CreateNewOrder(string usernameCrr, OrderViewModelIn orderViewModelIn);
        public Task<ServiceResponse<OrderViewModelOut>> ChangeStatusOrder(string usernameCrr, int orderId, int statusOrder);
        public Task<PagingOutput<IEnumerable<OrderViewModelOut>>> FilterOrderManager(string usernameCrr ,int orderStatus, string phoneNumber, int roomId, int cyberId, int pageIndex, int pageSize);
        public Task<ServiceResponse<IEnumerable<Slot>>> CheckSplitSlotInOrder(int orderId);
        public Task<ServiceResponse<IEnumerable<Cyber>>> GetAllCyberBookedByUserId(string usernameCrr);
    }
}
