﻿using CyberBook_API.Models;
using CyberBook_API.ViewModel.PagingView;
using CyberBook_API.ViewModel.RoomViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CyberBook_API.Services.Interfaces
{
    public interface IRoomsService
    {
        public Task<ServiceResponse<RoomOutViewModel>> CreateNewRoom(string usernameCrr, RoomCreateViewModel roomIn);
        public Task<ServiceResponse<RoomOutViewModel>> EditRoomInfor(string usernameCrr, RoomEditViewModel roomIn);
        public Task<ServiceResponse<RoomOutViewModel>> EditSizeRoomAllow(string usernameCrr, int roomId, int maxX, int maxY);
        public Task<ServiceResponse<RoomOutViewModel>> EditSizeRoom(string usernameCrr, int roomId, int maxX, int maxY);
        public Task<ServiceResponse<int>> RemoveRoom(string usernameCrr, int roomId);
        public Task<ServiceResponse<RoomOutViewModel>> GetRoomById(int roomId);
        public Task<PagingOutput<IEnumerable<RoomOutViewModel>>> GetListRoomByCyberId(int pageIndex, int pageSize, int cyberId);
    }
}
