﻿using CyberBook_API.Models;
using CyberBook_API.ViewModel.PagingView;
using CyberBook_API.ViewModel.UserViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CyberBook_API.Services.Interfaces
{
   public interface IUsersService
    {
        public Task<ServiceResponse<CyberAccount>> AddNewCyberAccount(string usernameCrr, CyberAccount cyberAccount);
        public Task<ServiceResponse<CyberAccount>> EditCyberAccount(string usernameCrr, CyberAccount cyberAccount);
        public Task<ServiceResponse<int>> RemoveCyberAccount(string usernameCrr, int cyberAccountId);
        public Task<ServiceResponse<CyberAccount>> GetDetailCyberAccount(string usernameCrr, int cyberAccountId);
        public Task<PagingOutput<IEnumerable<CyberAccount>>> ViewMyListCyberAccount(string usernameCrr, int pageIndex, int pageSize);
        public Task<ServiceResponse<UsersOutViewModel>> EditUserInfor(string usernameCrr, UserEditViewModel userIn);
        public Task<ServiceResponse<UsersOutViewModel>> GetMyInfor(string usernameCrr);

    }
}
