﻿using CyberBook_API.Models;
using CyberBook_API.ViewModel.RateUserViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CyberBook_API.Services.Interfaces
{
    public interface ICyberRateUserService
    {
        public Task<ServiceResponse<RateUserOutViewModel>> AddNewRateUser(string usernameCrr, RatingUser ratingUser);
        public Task<ServiceResponse<RateUserOutViewModel>> UpdateRateUser(string usernameCrr, RatingUser ratingUser);
        public Task<ServiceResponse<RateUserOutViewModel>> GetRateById(int rateId);
        public Task<ServiceResponse<string>> RemoveRateById(string usernameCrr, int rateId);
    }
}
