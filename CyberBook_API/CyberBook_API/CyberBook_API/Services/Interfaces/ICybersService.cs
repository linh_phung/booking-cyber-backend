﻿using CyberBook_API.Models;
using CyberBook_API.ViewModel.CyberViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CyberBook_API.Services.Interfaces
{
    public interface ICybersService 
    {
        public Task<ServiceResponse<Cyber>> GetMyCyber(string usernameCrr);
        public Task<ServiceResponse<Cyber>> EditCyberInfor(string usernameCrr, CyberEditViewModel cyberIn);
        public Task<ServiceResponse<Cyber>> GetCyberById(int cyberId);
    }
}
