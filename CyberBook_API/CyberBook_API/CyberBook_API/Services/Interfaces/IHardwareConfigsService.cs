﻿using CyberBook_API.Models;
using CyberBook_API.ViewModel.ConfigHardwareViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CyberBook_API.Services.Interfaces
{
    public interface IHardwareConfigsService
    {
        public Task<ServiceResponse<SlotHardwareConfig>> CreateSlotHardwareConfig(string usernameCrr,SlotHardwareConfig hardwareIN);
        public Task<ServiceResponse<SlotHardwareConfig>> UpdateSlotHardwareConfig(string usernameCrr, HardwareConfigEditViewModel hardwareIN);
        public Task<ServiceResponse<int>> RemoveSlotHardwareConfig(string usernameCrr,int hardwareId);
        public Task<ServiceResponse<IEnumerable<SlotHardwareConfigOutViewModel>>> GetHardwaresByCyberId(int cyberId);
        public Task<ServiceResponse<SlotHardwareConfig>> GetHardwaresById(int hardwareId);

    }
}
