﻿using CyberBook_API.Models;
using CyberBook_API.ViewModel.CyberViewModel;
using CyberBook_API.ViewModel.PagingView;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CyberBook_API.Services.Interfaces
{
    public interface ISearchService
    {
        public Task<PagingOutput<IEnumerable<CyberViewModelOut>>> GetAllCybers(int pageIndex, int pageSize);
        public Task<PagingOutput<IEnumerable<CyberViewModelOut>>> SearchCyberByName(int pageIndex, int pageSize, string searchContent);
        public Task<ServiceResponse<CyberViewModelOut>> GetCyberById(int id);
        public Task<PagingOutput<IEnumerable<CyberViewModelOut>>> SearchCyberByRate(int pageIndex, int pageSize);

    }
}
