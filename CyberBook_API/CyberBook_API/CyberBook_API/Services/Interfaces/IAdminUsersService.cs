﻿using CyberBook_API.Models;
using CyberBook_API.ViewModel.PagingView;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CyberBook_API.Services.Interfaces
{
   public interface IAdminUsersService
    {
        public Task<PagingOutput<IEnumerable<Users>>> ListUser(string usernameCrr, int pageIndex, int pageSize);
        public Task<ServiceResponse<Users>> ViewDetailUser(string usernameCrr, int userId);
        public Task<PagingOutput<IEnumerable<Users>>> SearchUserByName(string usernameCrr, int pageIndex, int pageSize, string username);
        public Task<ServiceResponse<Users>> LockUserById(string usernameCrr, int userId);
        public Task<ServiceResponse<Users>> UnLockUserById(string usernameCrr, int userId);
    }
}
