﻿using CyberBook_API.Models;
using CyberBook_API.ViewModel.CyberViewModel;
using CyberBook_API.ViewModel.PagingView;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CyberBook_API.Services.Interfaces
{
    public interface IAdminCybersService
    {
        public Task<ServiceResponse<IEnumerable<CyberViewModelOut>>> AdminListCyber(string usernameCrr);
        public Task<ServiceResponse<CyberViewModelOut>> ViewDetailCyber(int cyberId);
        public Task<ServiceResponse<CyberViewModelOut>> LockCyber(string usernameCrr, int cyberId);
        public Task<ServiceResponse<CyberViewModelOut>> UnLockCyber(string usernameCrr, int cyberId);
        public Task<ServiceResponse<CyberViewModelOut>> RegisterCyber(CyberViewModel cyberViewModel);
        public Task<ServiceResponse<string>> RejectCyber(string usernameCrr, int cyberId);
        public Task<ServiceResponse<CyberViewModelOut>> VerifyCyber(string usernameCrr, int cyberId);
        public Task<PagingOutput<IEnumerable<CyberViewModelOut>>> ViewListCyberRegisterPending(string usernameCrr, int pageIndex, int pageSize);
        public Task<PagingOutput<IEnumerable<CyberViewModelOut>>> ViewListCyberByFilter(string usernameCrr, int pageIndex, int pageSize, string statusCyberId, string cyberName);
    }
}
