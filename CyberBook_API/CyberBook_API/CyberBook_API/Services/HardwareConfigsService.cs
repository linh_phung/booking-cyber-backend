﻿using CyberBook_API.Dal.Repositories;
using CyberBook_API.Interfaces;
using CyberBook_API.Models;
using CyberBook_API.Services.Interfaces;
using CyberBook_API.ViewModel.ConfigHardwareViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CyberBook_API.Services
{
    public class HardwareConfigsService : IHardwareConfigsService
    {
        private readonly IHardwareConfigRepository _hardwareConfigRepository = new HardwareConfigRepository();
        private readonly ICybersRepository _cybersRepository = new CybersRepository();
        private readonly IUsersRepository _usersRepository = new UsersRepository();
        private readonly IAccountsRepository _accountsRepository = new AccountsRepository();
        private readonly IRoomRepository _roomRepository = new RoomRepository();
        private readonly ISlotsRepository _slotsRepository = new SlotsRepository();

        public async Task<ServiceResponse<SlotHardwareConfig>> CreateSlotHardwareConfig(string usernameCrr, SlotHardwareConfig hardwareIN)
        {
            var output = new ServiceResponse<SlotHardwareConfig>();
            try
            {
                var accountCrr = await _accountsRepository.GetAccountByUsername(usernameCrr);
                if (accountCrr != null)
                {
                    var currentUser = await _usersRepository.GetUserByAccountID(accountCrr.Id);
                    var cyber = await _cybersRepository.GetCyberByBossCyberId(currentUser.Id);
                    if (cyber != null)
                    {
                        //check user hiện tại có quyền trong Cyber này không?
                        if ((await _cybersRepository.IsBossCyber(currentUser.Id, cyber.Id)))
                        {
                            if (!hardwareIN.NameHardware.Equals(""))
                            {
                                var hardwwareCheck = await _hardwareConfigRepository.GetSlotHardwareByNameAndCyber(hardwareIN.NameHardware, cyber.Id);
                                if (hardwwareCheck == null)
                                {
                                    var hardwareTemp = new SlotHardwareConfig
                                    {
                                        Gpu = hardwareIN.Gpu,
                                        CyberID = cyber.Id,
                                        Cpu = hardwareIN.Cpu,
                                        Monitor = hardwareIN.Monitor,
                                        NameHardware = hardwareIN.NameHardware,
                                        Ram = hardwareIN.Ram,
                                        CreatedDate = DateTime.Now,
                                        UpdateDate = DateTime.Now
                                    };
                                    var newHardware = await _hardwareConfigRepository.Create(hardwareTemp);
                                    if (newHardware != null)
                                    {
                                        output.Data = newHardware;
                                        output.Message = "Successful";
                                        output.Success = true;
                                        return output;
                                    }
                                    output.Message = "CAN NOT Create new Hardwares";
                                    return output;
                                }
                                output.Message = "Hardware has exist";
                                return output;
                            }
                            output.Message = "Name of Hardware NOT allow empty";
                            return output;
                        }
                        output.Message = "You dont have Permission for this Function";
                        return output;
                    }
                    output.Message = "Cyber NOT exist";
                    return output;
                }
                output.Message = "You NOT Login";
                return output;
            }
            catch (Exception)
            {
                output.Message = "Create NEW slot has exception";
            }
            return output;
        }

        public async Task<ServiceResponse<IEnumerable<SlotHardwareConfigOutViewModel>>> GetHardwaresByCyberId(int cyberId)
        {
            var output = new ServiceResponse<IEnumerable<SlotHardwareConfigOutViewModel>>();
            try
            {
                if ((await _cybersRepository.FindBy(x => x.Id == cyberId)) != null)
                {
                    var lstHardware = await _hardwareConfigRepository.GetSlotHardwareByCyberId(cyberId);
                    if (lstHardware.Any())
                    {
                        var lstHardwareOut = new List<SlotHardwareConfigOutViewModel>();
                        foreach (var x in lstHardware)
                        {
                            var shc = new SlotHardwareConfigOutViewModel
                            {
                                Id = x.Id,
                                Cpu = x.Cpu,
                                CreatedDate = Convert.ToDateTime(x.CreatedDate).ToString("HH:mm:ss dd/MM/yyyy"),
                                UpdateDate = Convert.ToDateTime(x.UpdateDate).ToString("HH:mm:ss dd/MM/yyyy"),
                                CyberID = x.CyberID,
                                Gpu = x.Gpu,
                                Monitor = x.Monitor,
                                NameHardware = x.NameHardware,
                                Ram = x.Ram,
                            };
                            lstHardwareOut.Add(shc);
                        }
                        output.Data = lstHardwareOut;
                        output.Message = "Successful";
                        output.Success = true;
                        return output;
                    }
                    output.Message = "dont any hardware";
                    return output;
                }
                output.Message = "Cyber NOT Exist";
                return output;
            }
            catch (Exception)
            {
                output.Message = "GetHardwaresByCyberId Exception";
            }
            return output;
        }

        public async Task<ServiceResponse<SlotHardwareConfig>> GetHardwaresById(int hardwareId)
        {
            var output = new ServiceResponse<SlotHardwareConfig>();
            try
            {
                var hardware = await _hardwareConfigRepository.GetSlotHardwareById(hardwareId);
                if (hardware != null)
                {
                    output.Success = true;
                    output.Message = "Success";
                    output.Data = hardware;
                }
                output.Message = "hardware not exist";
                return output;
            }
            catch (Exception e)
            {
                output.Message = "GetHardwaresById Exception: " + e.Message;
            }
            return output;
        }

        public async Task<ServiceResponse<int>> RemoveSlotHardwareConfig(string usernameCrr, int hardwareId)
        {
            var output = new ServiceResponse<int>();
            try
            {
                var accountCrr = await _accountsRepository.GetAccountByUsername(usernameCrr);
                if (accountCrr != null)
                {
                    var currentUser = await _usersRepository.GetUserByAccountID(accountCrr.Id);
                    var cyber = await _cybersRepository.GetCyberByBossCyberId(currentUser.Id);
                    if (cyber != null)
                    {
                        //check user hiện tại có quyền trong Cyber này không?
                        if ((await _cybersRepository.IsBossCyber(currentUser.Id, cyber.Id)))
                        {
                            var hardware = (await _hardwareConfigRepository.FindBy(x => x.Id == hardwareId)).FirstOrDefault();
                            if (hardware != null)
                            {
                                // check hardware co link vs rooom nao k 
                                if (!(await _roomRepository.IsSlotHardwareNoRoom(hardware.Id)))
                                {
                                    output.Message = "CAN NOT Remove SlotHardwareConfig, SlotHardwareConfig has a Room";
                                    return output;
                                }
                                if (!(await _slotsRepository.IsHardwareNoSlot(hardware.Id)))
                                {
                                    output.Message = "CAN NOT Remove SlotHardwareConfig, SlotHardwareConfig has a Slot";
                                    return output;
                                }

                                if ((await _hardwareConfigRepository.Delete(hardware)) != -1)
                                {
                                    output.Message = "Successful";
                                    output.Data = 1;
                                    output.Success = true;
                                    return output;
                                }
                                output.Message = "CAN NOT Remove SlotHardwareConfig";
                                return output;
                            }
                            output.Message = "HardWare NOT exist";
                            return output;
                        }
                        output.Message = "You NOT permission for this Function";
                        return output;
                    }
                    output.Message = "Cyber NOT exist";
                    return output;
                }
                output.Message = "You NOT LOGIN";
                return output;
            }
            catch (Exception)
            {
                output.Message = "Remove SlotHardwareConfig Exception";
            }
            return output;
        }

        public async Task<ServiceResponse<SlotHardwareConfig>> UpdateSlotHardwareConfig(string usernameCrr, HardwareConfigEditViewModel hardwareIN)
        {
            var output = new ServiceResponse<SlotHardwareConfig>();
            try
            {
                var accountCrr = await _accountsRepository.GetAccountByUsername(usernameCrr);
                if (accountCrr != null)
                {
                    var currentUser = await _usersRepository.GetUserByAccountID(accountCrr.Id);
                    var cyber = await _cybersRepository.GetCyberByBossCyberId(currentUser.Id);
                    if (cyber != null)
                    {
                        //check user hiện tại có quyền trong Cyber này không?
                        if ((await _cybersRepository.IsBossCyber(currentUser.Id, cyber.Id)))
                        {
                            var hardware = await _hardwareConfigRepository.GetSlotHardwareById(hardwareIN.Id);
                            if (hardware != null)
                            {
                                hardware.Monitor = hardwareIN.Monitor;
                                hardware.Gpu = hardwareIN.Gpu;
                                hardware.Cpu = hardwareIN.Cpu;
                                hardware.Ram = hardwareIN.Ram;
                                hardware.NameHardware = hardwareIN.NameHardware;
                                hardware.UpdateDate = DateTime.Now;
                                if ((await _hardwareConfigRepository.Update(hardware, hardware.Id)) != -1)
                                {
                                    output.Data = hardware;
                                    output.Message = "Successful";
                                    output.Success = true;
                                    return output;
                                }
                                output.Message = "Update Fail";
                                return output;
                            }
                            output.Message = "Hardware NOT exist";
                            return output;
                        }
                        output.Message = "You NOT permission for this Function";
                        return output;
                    }
                    output.Message = "Cyber NOT exist";
                    return output;
                }
                output.Message = "You NOT LOGIN";
                return output;
            }
            catch (Exception)
            {
                output.Message = "Update Exception";
            }
            return output;
        }
    }
}
