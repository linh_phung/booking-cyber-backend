﻿using CyberBook_API.Dal.Repositories;
using CyberBook_API.Enum;
using CyberBook_API.Interfaces;
using CyberBook_API.Models;
using CyberBook_API.Services.Interfaces;
using CyberBook_API.ViewModel.PagingView;
using CyberBook_API.ViewModel.SlotViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CyberBook_API.Services
{
    public class SlotsService : ISlotsService
    {
        private readonly ISlotsRepository _slotsRepository = new SlotsRepository();
        private readonly ICybersRepository _cybersRepository = new CybersRepository();
        private readonly IAccountsRepository _accountsRepository = new AccountsRepository();
        private readonly IUsersRepository _usersRepository = new UsersRepository();
        private readonly IRoomRepository _roomRepository = new RoomRepository();
        private readonly IHardwareConfigRepository _hardwareConfigRepository = new HardwareConfigRepository();
        private readonly IStatusSlotRepository _statusSlotRepository = new StatusSlotRepository();


        public async Task<ServiceResponse<Slot>> CreateNewSlot(string usernameCrr, SlotCreateViewModelIn slotCreateViewModelIn)
        {
            var output = new ServiceResponse<Slot>();
            try
            {
                var cyber = await _cybersRepository.GetCyberById(slotCreateViewModelIn.CyberId);
                if (cyber != null)
                {
                    //check user hiện tại
                    var accountCrr = await _accountsRepository.GetAccountByUsername(usernameCrr);
                    if (accountCrr != null)
                    {
                        var currentUser = await _usersRepository.GetUserByAccountID(accountCrr.Id);
                        //check user hiện tại có quyền trong Cyber này không?
                        if ((await _cybersRepository.IsBossCyber(currentUser.Id, cyber.Id)))
                        {
                            var roomCheck = await _roomRepository.GetRoomById(slotCreateViewModelIn.RoomId);
                            if (roomCheck != null)
                            {
                                //check xem cái thông tin phần cứng đấy có thuộc trong cyber đó không
                                var hardwareConfig = await _hardwareConfigRepository.GetSlotHardwareById(roomCheck.HardwareConfigId);
                                if (hardwareConfig != null)
                                {
                                    if (!slotCreateViewModelIn.SlotName.Equals(""))
                                    {
                                        var slotTempt = new Slot
                                        {
                                            RoomId = slotCreateViewModelIn.RoomId,
                                            SlotHardwareConfigId = roomCheck.HardwareConfigId,
                                            SlotHardwareName = hardwareConfig.NameHardware,
                                            StatusId = Convert.ToInt32(SlotsEnum.SlotStatus.Ready),
                                            SlotName = slotCreateViewModelIn.SlotName,
                                            SlotDescription = slotCreateViewModelIn.SlotDescription,
                                            SlotPositionX = slotCreateViewModelIn.SlotPositionX,
                                            SlotPositionY = slotCreateViewModelIn.SlotPositionY
                                        };

                                        if ((!await _slotsRepository.SlotPositionExistCheck(slotTempt.RoomId, slotTempt.SlotPositionX, slotTempt.SlotPositionY))
                                            || (await _slotsRepository.SlotPositionIsUnload(slotTempt.RoomId, slotTempt.SlotPositionX, slotTempt.SlotPositionY)))
                                        {
                                            var newSlot = await _slotsRepository.Create(slotTempt);
                                            if (newSlot != null)
                                            {
                                                output.Data = newSlot;
                                                output.Message = "Create New Slot Successfully";
                                                output.Success = true;
                                                return output;
                                            }
                                            output.Message = "Create New Slot Fail";
                                            return output;
                                        }
                                        output.Message = "Position has a slot";
                                        return output;
                                    }
                                    output.Message = "SlotName CAN NOT is Empty";
                                    return output;

                                }
                                output.Message = "hardwareConfig NOT exist in this Cyber";
                                return output;
                            }
                            output.Message = "Room NOT exist";
                            return output;
                        }
                        output.Message = "You NOT permission for this Function";
                        return output;
                    }
                    output.Message = "You NOT Login";
                    return output;
                }
                output.Message = "Cyber NOT exist";
                return output;
            }
            catch (Exception e)
            {
                output.Message = "CreateNewSlot Exception: " + e.Message;
            }
            return output;
        }

        public async Task<ServiceResponse<Slot>> EditSlot(string usernameCrr, SlotViewModelIn slotViewModelIn)
        {
            var output = new ServiceResponse<Slot>();
            try
            {
                var cyber = await _cybersRepository.GetCyberById(slotViewModelIn.CyberId);
                if (cyber != null)
                {
                    var accountCrr = await _accountsRepository.GetAccountByUsername(usernameCrr);
                    if (accountCrr != null)
                    {
                        var currentUser = await _usersRepository.GetUserByAccountID(accountCrr.Id);
                        //check user hiện tại có quyền trong Cyber này không?
                        if ((await _cybersRepository.IsBossCyber(currentUser.Id, cyber.Id)))
                        {
                            var roomCheck = await _roomRepository.GetRoomById(slotViewModelIn.RoomId);
                            if (roomCheck == null)
                            {
                                output.Message = "Room NOT exist";
                                return output;
                            }
                            var slotCheck = await _slotsRepository.GetSlotById(slotViewModelIn.Slot.Id);
                            if (slotCheck == null)
                            {
                                output.Message = "Slot NOT exist";
                                return output;
                            }
                            //check xem cái thông tin phần cứng đấy có thuộc trong cyber đó không
                            var hardwareConfig = await _hardwareConfigRepository.GetSlotHardwareByCyberId(slotViewModelIn.CyberId);
                            if (hardwareConfig == null)
                            {
                                output.Message = "hardwareConfig NOT exist in this Cyber";
                                return output;
                            }
                            //check trạng thái của máy có tồn tại không?
                            var statusSlot = await _statusSlotRepository.GetStatusSlotsById(slotViewModelIn.Slot.StatusId);
                            if (statusSlot == null)
                            {
                                output.Message = "statusSlot NOT exist";
                                return output;
                            }
                            if (!slotViewModelIn.Slot.SlotName.Equals(""))
                            {
                                slotCheck.SlotHardwareConfigId = slotViewModelIn.Slot.SlotHardwareConfigId;
                                slotCheck.SlotHardwareName = slotViewModelIn.Slot.SlotHardwareName;
                                slotCheck.StatusId = slotViewModelIn.Slot.StatusId;
                                slotCheck.SlotName = slotViewModelIn.Slot.SlotName;
                                slotCheck.SlotDescription = slotViewModelIn.Slot.SlotDescription;
                                slotCheck.SlotPositionX = slotViewModelIn.Slot.SlotPositionX;
                                slotCheck.SlotPositionY = slotViewModelIn.Slot.SlotPositionY;
                                var newSlot = await _slotsRepository.Update(slotCheck, slotCheck.Id);
                                if (newSlot != -1)
                                {
                                    output.Data = slotCheck;
                                    output.Message = "Update Slot Successfully";
                                    output.Success = true;
                                    return output;
                                }
                                output.Message = "Update Slot Fail";
                                return output;
                            }
                            output.Message = "SlotName CAN NOT is Empty";
                            return output;
                        }
                        output.Message = "You NOT permission for this Function";
                        return output;
                    }
                    output.Message = "You NOT Login";
                    return output;
                }
                output.Message = "Cyber NOT exist";
                return output;
            }
            catch (Exception e)
            {
                output.Message = "EditSlot Exception: " + e.Message;
            }
            return output;
        }

        public async Task<ServiceResponse<Slot>> EditStatusSlot(string usernameCrr, int slotId, int statusSlot)
        {
            var output = new ServiceResponse<Slot>();
            try
            {
                var slotCheck = await _slotsRepository.GetSlotById(slotId);
                if (slotCheck != null)
                {
                    var roomCheck = await _roomRepository.GetRoomById(slotCheck.RoomId);
                    if (roomCheck != null)
                    {
                        var cyber = await _cybersRepository.GetCyberById(roomCheck.CyberId);
                        if (cyber != null)
                        {
                            var accountCrr = await _accountsRepository.GetAccountByUsername(usernameCrr);
                            if (accountCrr != null)
                            {
                                var currentUser = await _usersRepository.GetUserByAccountID(accountCrr.Id);
                                //check user hiện tại có quyền trong Cyber này không?
                                if ((await _cybersRepository.IsBossCyber(currentUser.Id, cyber.Id)))
                                {
                                    var slotUpdate = await _slotsRepository.UpdateStatusSlot(slotCheck.Id, statusSlot);
                                    if (slotUpdate != null)
                                    {
                                        output.Data = slotUpdate;
                                        output.Success = true;
                                        output.Message = "Successful";
                                        return output;
                                    }
                                    output.Message = "CAN NOT update Slot";
                                    return output;
                                }
                                output.Message = "Slot NOT exist";
                                return output;
                            }
                            output.Message = "Room NOT exist";
                            return output;
                        }
                        output.Message = "You NOT permission for this Function";
                        return output;
                    }
                    output.Message = "You NOT LOGIN";
                    return output;
                }
                output.Message = "Cyber NOT exist";
                return output;
            }
            catch (Exception e)
            {
                output.Message = e.Message;
            }
            return output;
        }

        public async Task<PagingOutput<IEnumerable<Slot>>> GetListSlotByRoom(int pageIndex, int pageSize, int roomId)
        {
            var output = new PagingOutput<IEnumerable<Slot>>();
            try
            {
                var room = await _roomRepository.GetRoomById(roomId);
                if (room != null)
                {
                    var result = await _slotsRepository.GetAllSlotByRoomId(room.Id, pageIndex, pageSize);
                    if (result != null)
                    {
                        output = result;
                        output.Message = "Successfully";
                        return output;
                    }
                    output.Message = "Slot NOT exist";
                    return output;
                }
                output.Message = "Room NOT exist";
                return output;
            }
            catch (Exception e)
            {
                output.Message = "GetListSlotByRoom Exception: " + e.Message;
            }
            return output;
        }

        public async Task<ServiceResponse<Slot>> GetSlotById(int slotId)
        {
            var output = new ServiceResponse<Slot>();
            try
            {
                var slot = await _slotsRepository.GetSlotById(slotId);
                if (slot != null)
                {
                    output.Data = slot;
                    output.Message = "Successful";
                    output.Success = false;
                    return output;
                }
                output.Message = "Slot NOT exist";
                return output;
            }
            catch (Exception e)
            {
                output.Message = "GetSlotById Exception: " + e.Message;
            }
            return output;
        }

        public async Task<ServiceResponse<int>> RemoveSlot(string usernameCrr, int slotId)
        {
            var output = new ServiceResponse<int>();
            try
            {
                var accountCrr = await _accountsRepository.GetAccountByUsername(usernameCrr);
                if (accountCrr != null)
                {
                    var currentUser = await _usersRepository.GetUserByAccountID(accountCrr.Id);

                    var slotCheck = await _slotsRepository.GetSlotById(slotId);
                    if (slotCheck != null)
                    {
                        var roomCheck = await _roomRepository.GetRoomById(slotCheck.RoomId);
                        if (roomCheck != null)
                        {
                            var cyber = await _cybersRepository.GetCyberById(roomCheck.CyberId);
                            if (cyber != null)
                            {
                                //check user hiện tại có quyền trong Cyber này không?
                                if ((await _cybersRepository.IsBossCyber(currentUser.Id, cyber.Id)))
                                {
                                    slotCheck.StatusId = Convert.ToInt32(SlotsEnum.SlotStatus.Unload);
                                    if ((await _slotsRepository.Update(slotCheck, slotCheck.Id)) != -1)
                                    {
                                        output.Message = "RemoveSlot OK";
                                        output.Data = 1;
                                        output.Success = true;
                                        return output;
                                    }
                                    output.Message = "CAN NOT Remove Slot";
                                    return output;
                                }
                                output.Message = "You NOT permission for this Function";
                                return output;
                            }
                            output.Message = "Slot NOT exist";
                            return output;
                        }
                        output.Message = "Room NOT exist";
                        return output;
                    }
                    output.Message = "Slot NOT exist";
                    return output;
                }
                output.Message = "You NOT LOGIN";
                return output;
            }
            catch (Exception e)
            {
                output.Message = "RemoveSlot Exception: " + e.Message;
            }
            return output;
        }
    }
}
