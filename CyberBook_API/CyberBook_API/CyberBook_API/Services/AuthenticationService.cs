﻿using CyberBook_API.Dal.Repositories;
using CyberBook_API.Enum;
using CyberBook_API.Interfaces;
using CyberBook_API.Models;
using CyberBook_API.Services.Interfaces;
using CyberBook_API.ViewModel.AccountView;
using CyberBook_API.ViewModel.AccountViewModel;
using CyberBook_API.ViewModel.TokenViewModel;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace CyberBook_API.Services
{
    public class AuthenticationService : IAuthenticationService
    {
        private readonly IAccountsRepository _accountRepository = new AccountsRepository();
        private readonly IUsersRepository _usersRepository = new UsersRepository();
        public AuthenticationService() { }

        public AuthenticationService(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public async Task<ServiceResponse<TokenViewModel>> GetToken(Account account)
        {
            var output = new ServiceResponse<TokenViewModel>();
            var tokenView = new TokenViewModel();
            try
            {
                if (_accountRepository.CheckNull(account.Username, account.Password))
                {
                    string pass = account.Password;
                    MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
                    UTF8Encoding utf8 = new UTF8Encoding();
                    byte[] data = md5.ComputeHash(utf8.GetBytes(pass));
                    var passHasConvert = Convert.ToBase64String(data);

                    var acc = await _accountRepository.CheckLogin(account.Username, passHasConvert);
                    if (acc != null)
                    {
                        var user = await _usersRepository.GetUserByAccountID(acc.Id);
                        if (user != null)
                        {
                            if (user.Status == Convert.ToInt32(UsersEnum.UserStatus.Block))
                            {
                                output.Message = "User has BLOCK";
                                //output.Success = true;
                                //output.Code = 400;
                                return output;
                            }
                            //create New Token
                            var tokenHandler = new JwtSecurityTokenHandler();
                            var key = Encoding.ASCII.GetBytes(Configuration["JWTSettings:SecretKey"]);
                            var tokenDescription = new SecurityTokenDescriptor
                            {
                                //add user session
                                Subject = new ClaimsIdentity(new Claim[] {
                                    new Claim(ClaimTypes.Name, acc.Username)
                                }),
                                Expires = DateTime.Now.AddMinutes(30),
                                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key),
                                SecurityAlgorithms.HmacSha256Signature)
                            };
                            var token = tokenHandler.CreateToken(tokenDescription);
                            tokenView.Token = tokenHandler.WriteToken(token);
                            tokenView.UserId = user.Id;
                            tokenView.UserAccount = acc.Username;
                            tokenView.CreatedTime = DateTime.Now;
                            tokenView.ExpiredTime = DateTime.Now.AddMinutes(30);
                            tokenView.RefreshToken = GenerateRefreshToken();

                            output.Message = "Success";
                            output.Success = true;
                            output.Data = tokenView;
                            //output.Code = 200;
                            return output;
                        }
                        output.Message = "User not exist";
                        return output;
                    }
                    output.Message = "Login FAIL";
                    return output;
                }
                output.Message = "Username password not allow empty";
                return output;
            }
            catch (Exception e)
            {
                output.Message = "Login Exception: " + e.Message;
            }
            return output;
        }

        public async Task<ServiceResponse<AccountViewModel>> Login(Account account)
        {
            var output = new ServiceResponse<AccountViewModel>();
            var accountView = new AccountViewModel();
            try
            {
                if (_accountRepository.CheckNull(account.Username, account.Password))
                {
                    string pass = account.Password;
                    MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
                    UTF8Encoding utf8 = new UTF8Encoding();
                    byte[] data = md5.ComputeHash(utf8.GetBytes(pass));
                    var passHasConvert = Convert.ToBase64String(data);

                    var acc = await _accountRepository.CheckLogin(account.Username, passHasConvert);
                    if (acc != null)
                    {
                        var user = await _usersRepository.GetUserByAccountID(acc.Id);
                        if (user != null)
                        {
                            if (user.Status == Convert.ToInt32(UsersEnum.UserStatus.Block))
                            {
                                output.Message = "User has BLOCK";
                                //output.Code = 400;
                                //output.Success = true;
                                return output;
                            }

                            accountView.Username = account.Username;
                            accountView.Password = null;
                            accountView.UserId = user.Id;
                            accountView.Fullname = user.Fullname;
                            accountView.Address = user.Address;
                            accountView.Email = user.Email;
                            accountView.PhoneNumber = user.PhoneNumber;
                            accountView.Bio = user.Bio;
                            accountView.RoleId = user.RoleId;
                            accountView.Image = user.Image;
                            accountView.Dob = user.Dob;
                            accountView.RatingPoint = user.RatingPoint;
                            accountView.AccountID = user.AccountID;

                            //create New Token
                            var tokenHandler = new JwtSecurityTokenHandler();
                            var key = Encoding.ASCII.GetBytes(Configuration["JWTSettings:SecretKey"]);
                            var tokenDescription = new SecurityTokenDescriptor
                            {
                                //add user session
                                Subject = new ClaimsIdentity(new Claim[] {
                                    new Claim(ClaimTypes.Name, accountView.Username)
                                }),
                                Expires = DateTime.Now.AddMinutes(30),
                                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key),
                                SecurityAlgorithms.HmacSha256Signature)
                            };
                            var token = tokenHandler.CreateToken(tokenDescription);
                            accountView.Token = tokenHandler.WriteToken(token);


                            output.Message = "Success";
                            output.Success = true;
                            output.Data = accountView;
                            //output.Code = 200;
                            return output;
                        }
                        output.Message = "User not exist";
                        return output;
                    }
                    output.Message = "Login FAIL";
                    return output;
                }
                output.Message = "Username password not allow empty";
                return output;
            }
            catch (Exception e)
            {
                output.Message = "Login Exception: " + e.Message;
            }
            return output;
        }

        public async Task<ServiceResponse<AccountViewModel>> Reigster(AccountRegisterViewModel accountRegisterIn)
        {
            var output = new ServiceResponse<AccountViewModel>();
            try
            {
                if (_accountRepository.CheckNull(accountRegisterIn.Email, accountRegisterIn.Password))
                {
                    var a = await _accountRepository.GetAccountByUsername(accountRegisterIn.Email);
                    if (a == null)
                    {
                        if (_usersRepository.IsValidEmail(accountRegisterIn.Email))
                        {

                            var u = await _usersRepository.GetUserByEmail(accountRegisterIn.Email);
                            if (u == null)
                            {
                                var account = new Account
                                {
                                    Username = accountRegisterIn.Email,
                                    Password = accountRegisterIn.Password
                                };
                                var user = new Users
                                {
                                    Fullname = accountRegisterIn.Fullname,
                                    Email = accountRegisterIn.Email,
                                    PhoneNumber = accountRegisterIn.PhoneNumber,
                                    Image = "Qmdkw8rDq9YjQtaKacXYZVWLLoUGvpDZM2gtRf11TaP9eS",
                                    RoleId = Convert.ToInt32(UsersEnum.UserRole.UserNormal),
                                    Status = Convert.ToInt32(UsersEnum.UserStatus.Active)
                                };

                                //hash password to md5
                                string pass = account.Password;
                                MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
                                UTF8Encoding utf8 = new UTF8Encoding();
                                byte[] data = md5.ComputeHash(utf8.GetBytes(pass));
                                var passHasConvert = Convert.ToBase64String(data);
                                account.Password = passHasConvert;

                                //Create new account
                                var newacc = await _accountRepository.Create(account);
                                if (newacc != null)
                                {
                                    user.AccountID = newacc.Id;
                                    //Create New User
                                    var newUser = await _usersRepository.Create(user);
                                    //int rsCreateUser = await _usersRepository.Save();
                                    if (newUser != null)
                                    {
                                        var accountView = new AccountViewModel
                                        {
                                            Username = newacc.Username,
                                            Password = null,
                                            UserId = newUser.Id,
                                            Fullname = newUser.Fullname,
                                            Address = newUser.Address,
                                            Email = newUser.Email,
                                            PhoneNumber = newUser.PhoneNumber,
                                            Bio = newUser.Bio,
                                            RoleId = newUser.RoleId,
                                            Image = newUser.Image,
                                            Dob = newUser.Dob,
                                            RatingPoint = newUser.RatingPoint,
                                            AccountID = newUser.AccountID
                                        };
                                        output.Data = accountView;
                                        output.Message = "Register OK";
                                        output.Success = true;
                                        return output;
                                    }
                                    await _accountRepository.Delete(account);
                                    await _accountRepository.Save();
                                    output.Message = "Cannot create new user";
                                    return output;
                                }
                                output.Message = "Cannot create new Account";
                                return output;
                            }
                            output.Message = "email or phone number has exist";
                            return output;

                        }
                        output.Message = "email not correct form";
                        return output;
                    }
                    output.Message = "username has exist";
                    return output;
                }
                output.Message = "Username or password not allow empty";
                return output;
            }
            catch (Exception e)
            {
                output.Message = "Register Exception: " + e.Message;
            }
            return output;
        }

        public async Task<ServiceResponse<Users>> SendCodeResetPassword(string email)
        {
            var output = new ServiceResponse<Users>();

            Random rd = new Random();
            var code = rd.Next(100000, 999999);

            var addCode = (await _usersRepository.FindBy(x => x.Email == email)).FirstOrDefault();

            if (addCode != null)
            {
                addCode.ComfirmPassword = code.ToString();

                var rs = await _usersRepository.Update(addCode, addCode.Id);

                MailMessage message = new MailMessage("wegocyber@gmail.com", addCode.Email, "Yêu cầu reset mật khẩu", "Xin chào");
                message.Body = "Xin chào " + addCode.Fullname +
                    "<p>Chúng tôi đã nhận được yêu cầu lấy lại mật khẩu của bạn. Dưới đây là mã xác nhận</p><br>" + "<h1>" + code.ToString() + "</h1>" +
                    "<br>" +
                    "<p>Vui lòng nhập mã vào ô nhập mã tại màn hình yêu cầu lấy lại mật khaair của bạn để hoàn thành</p><br>" +
                    "<p>Trân trọng,</p>" +
                    "<p>GoCyber.</p>";
                message.BodyEncoding = System.Text.Encoding.UTF8;
                message.SubjectEncoding = System.Text.Encoding.UTF8;
                message.IsBodyHtml = true;

                message.ReplyToList.Add(new MailAddress("wegocyber@gmail.com"));
                message.Sender = new MailAddress("wegocyber@gmail.com");

                using var smtpClient = new SmtpClient("smtp.gmail.com");
                smtpClient.Port = 587;
                smtpClient.EnableSsl = true;
                smtpClient.Credentials = new NetworkCredential("wegocyber@gmail.com", "gocyber123");

                await smtpClient.SendMailAsync(message);
                output.Message = "Success";
            }
            else
            {
                output.Message = "mail not exist";
            }
            return output;
        }

        public async Task<ServiceResponse<string>> SetPassword(int accountId, string pass)
        {
            var output = new ServiceResponse<string>();
            var account1 = (await _accountRepository.FindBy(x => x.Id == accountId)).FirstOrDefault();
            //await _accountRepository.GetAccountByID(accountId);

            MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
            UTF8Encoding utf8 = new UTF8Encoding();
            byte[] data = md5.ComputeHash(utf8.GetBytes(pass));

            var passToData = Convert.ToBase64String(data);

            if (account1 != null)
            {
                account1.Password = passToData.ToString();
                var rs = await _accountRepository.Update(account1, account1.Id);
                if (rs != -1)
                {
                    output.Message = "Set Password OK ";
                    output.Success = true;
                    return output;
                }
                output.Message = "Set Password FAIL ";
            }
            return output;
        }

        public async Task<ServiceResponse<Account>> ConfirmPass(string email, string code)
        {
            var output = new ServiceResponse<Account>();
            var userConfirm = (await _usersRepository.FindBy(x => x.Email == email)).FirstOrDefault();
            if (userConfirm != null)
            {
                if (userConfirm.Email == email && userConfirm.ComfirmPassword == code)
                {
                    var accountOut = (await _accountRepository.FindBy(x => x.Id == userConfirm.AccountID)).FirstOrDefault();
                    if (accountOut != null)
                    {
                        output.Message = "verified";
                        output.Success = true;
                        output.Data = accountOut;
                        return output;
                    }
                }
                output.Message = "wrong code";
            }
            return output;
        }

        public async Task<ServiceResponse<string>> ChangePassword(string user, string oldPass, string newPass)
        {
            var output = new ServiceResponse<string>();
            var account1 = (await _accountRepository.FindBy(x => x.Username == user)).FirstOrDefault();
            //await _accountRepository.GetAccountByID(accountId);

            MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
            UTF8Encoding utf8 = new UTF8Encoding();
            byte[] data = md5.ComputeHash(utf8.GetBytes(oldPass));
            var passOldHash = Convert.ToBase64String(data);

            if (account1 != null)
            {
                if (account1.Password.Equals(passOldHash))
                {
                    byte[] data1 = md5.ComputeHash(utf8.GetBytes(newPass));
                    var passNewHash = Convert.ToBase64String(data1);

                    account1.Password = passNewHash.ToString();
                    var rs = await _accountRepository.Update(account1, account1.Id);
                    if (rs != -1)
                    {
                        output.Message = "Change Password OK ";
                        output.Success = true;
                        return output;
                    }
                    output.Message = "Change Password FAIL ";
                    return output;
                }
                output.Message = "Password has wrong";
                return output;
            }
            output.Message = "Account not exist";
            return output;
        }

        public async Task<ServiceResponse<TokenViewModel>> RefreshToken(string accessToken, string refreshToken)
        {
            Users user = await GetUserFromAccessToken(accessToken);
            if (user != null && ValidateRefreshToken(user, refreshToken))
            {

            }

            return null;
        }

        private async Task<Users> GetUserFromAccessToken(string accessToken)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(Configuration["JWTSettings:SecretKey"]);

            var tokenValidationParameters = new TokenValidationParameters
            {
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new SymmetricSecurityKey(key),
                ValidateIssuer = false,
                ValidateAudience = false
            };

            SecurityToken securityToken;

            var principle = tokenHandler.ValidateToken(accessToken, tokenValidationParameters, out securityToken);

            JwtSecurityToken jwtSecurityToken = securityToken as JwtSecurityToken;
            if (jwtSecurityToken != null && jwtSecurityToken.Header.Alg.Equals(SecurityAlgorithms.HmacSha256Signature,
                StringComparison.InvariantCultureIgnoreCase))
            {
                var username = principle.FindFirst(ClaimTypes.Name)?.Value;
                var accountCrr = await _accountRepository.GetAccountByUsername(username);
                return (await _usersRepository.GetUserByAccountID(accountCrr.Id));
            }
            return null;
        }

        private bool ValidateRefreshToken(Users user, string refreshToken)
        {



            return false;
        }

        private string GenerateRefreshToken()
        {
            var randomNumber = new byte[32];
            using (var rng = RandomNumberGenerator.Create())
            {
                rng.GetBytes(randomNumber);
                return Convert.ToBase64String(randomNumber);
            }
        }
    }
}
