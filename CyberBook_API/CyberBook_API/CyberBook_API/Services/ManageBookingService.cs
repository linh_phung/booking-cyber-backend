﻿using CyberBook_API.Dal.Repositories;
using CyberBook_API.Enum;
using CyberBook_API.Interfaces;
using CyberBook_API.Models;
using CyberBook_API.Services.Interfaces;
using CyberBook_API.ViewModel.OrderViewModel;
using CyberBook_API.ViewModel.PagingView;
using CyberBook_API.ViewModel.RateCybersViewModel;
using CyberBook_API.ViewModel.RateUserViewModel;
using CyberBook_API.ViewModel.SlotViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CyberBook_API.Services
{
    public class ManageBookingService : IManageBookingService
    {
        private readonly ICybersRepository _cybersRepository = new CybersRepository();
        private readonly IOrderRepository _orderRepository = new OrderRepository();
        private readonly IAccountsRepository _accountsRepository = new AccountsRepository();
        private readonly IUsersRepository _usersRepository = new UsersRepository();
        private readonly IRoomRepository _roomRepository = new RoomRepository();
        private readonly ICyberAccountRepository _cyberAccountRepository = new CyberAccountRepository();
        private readonly ISlotsRepository _slotsRepository = new SlotsRepository();
        private readonly IStatusSlotRepository _statusSlotRepository = new StatusSlotRepository();
        private readonly IStatusOrderRepository _statusOrderRepository = new StatusOrderRepository();
        private readonly ICyberRateUserRepository _cyberRateUserRepository = new CyberRateUserRepository();
        private readonly IUserRateCyberRepository _userRateCyberRepository = new UserRateCyberRepository();

        public async Task<ServiceResponse<OrderViewModelOut>> ChangeStatusOrder(string usernameCrr, int orderId, int statusOrder)
        {
            var output = new ServiceResponse<OrderViewModelOut>();
            try
            {
                var accountCrr = await _accountsRepository.GetAccountByUsername(usernameCrr);
                if (accountCrr != null)
                {
                    var currentUser = await _usersRepository.GetUserByAccountID(accountCrr.Id);
                    var order = await _orderRepository.GetOrderById(orderId);
                    if (order != null)
                    {
                        var cyber = await _cybersRepository.GetCyberById(order.CyberId);
                        if (cyber != null)
                        {
                            if (((await _cybersRepository.IsBossCyber(currentUser.Id, cyber.Id)) && cyber.Id == order.CyberId) ||
                                     order.CreatedBy == currentUser.Id)
                            {
                                var resultOrder = await _orderRepository.ChangeStatusOrder(order, statusOrder);
                                if (resultOrder != null)
                                {
                                    string[] lstSlotId = order.SlotOrderId.Split('|');
                                    var lstSlots = new List<SlotsOrderViewModelOut>();
                                    string listSlotName = string.Empty;

                                    if (statusOrder == Convert.ToInt32(OrdersEnum.StatusOrder.Reject) ||
                                        statusOrder == Convert.ToInt32(OrdersEnum.StatusOrder.NotDone) ||
                                        statusOrder == Convert.ToInt32(OrdersEnum.StatusOrder.Done) ||
                                        statusOrder == Convert.ToInt32(OrdersEnum.StatusOrder.Approve))
                                    {
                                        //Check quyền admin Cyber 
                                        if ((await _cybersRepository.IsBossCyber(currentUser.Id, cyber.Id)) && cyber.Id == order.CyberId)
                                        {
                                            //người chơi Reject hoặc đổi trạng thái của order là NOT DONE, người dùng đặt máy nhưng không đến
                                            if (statusOrder == Convert.ToInt32(OrdersEnum.StatusOrder.Reject) ||
                                                statusOrder == Convert.ToInt32(OrdersEnum.StatusOrder.NotDone))
                                            {
                                                foreach (var s in lstSlotId)
                                                {
                                                    var slotCheck = (await _slotsRepository.FindBy(x => x.Id == Convert.ToInt32(s))).FirstOrDefault();
                                                    var slotStatus = (await _statusSlotRepository.FindBy(x => x.Id == slotCheck.StatusId)).FirstOrDefault();

                                                    if (slotCheck == null)
                                                    {
                                                        output.Message = "Slot not exist";
                                                        return output;
                                                    }

                                                    //trả lại trạng thái của các slot là RẢNH, sẵn sàng để cho người khác book 
                                                    //Slot Status = 2 = RẢNH
                                                    slotCheck.StatusId = Convert.ToInt32(SlotsEnum.SlotStatus.Ready);
                                                    await _slotsRepository.Update(slotCheck, slotCheck.Id);
                                                    var slotView = new SlotsOrderViewModelOut
                                                    {
                                                        SlotId = slotCheck.Id,
                                                        RoomId = slotCheck.RoomId,
                                                        SlotDescription = slotCheck.SlotDescription,
                                                        SlotHardwareConfigId = slotCheck.SlotHardwareConfigId,
                                                        SlotHardwareName = slotCheck.SlotHardwareName,
                                                        SlotName = slotCheck.SlotName,
                                                        SlotPositionX = slotCheck.SlotPositionX,
                                                        SlotPositionY = slotCheck.SlotPositionY,
                                                        StatusId = slotCheck.StatusId,
                                                        StatusName = slotStatus.StatusSlotDescription
                                                    };
                                                    listSlotName += slotCheck.SlotName + ", ";
                                                    lstSlots.Add(slotView);
                                                }
                                            }

                                            //người chơi đến quán và xác nhận việc đặt chỗ
                                            if (statusOrder == Convert.ToInt32(OrdersEnum.StatusOrder.Done))
                                            {
                                                foreach (var s in lstSlotId)
                                                {
                                                    var slotCheck = (await _slotsRepository.FindBy(x => x.Id == Convert.ToInt32(s))).FirstOrDefault();
                                                    var slotStatus = (await _statusSlotRepository.FindBy(x => x.Id == slotCheck.StatusId)).FirstOrDefault();
                                                    if (slotCheck != null)
                                                    {
                                                        // SET trạng thái các máy đã được book là đã có người chơi
                                                        slotCheck.StatusId = Convert.ToInt32(SlotsEnum.SlotStatus.NotReady);
                                                        await _slotsRepository.Update(slotCheck, slotCheck.Id);
                                                        var slotView = new SlotsOrderViewModelOut
                                                        {
                                                            SlotId = slotCheck.Id,
                                                            RoomId = slotCheck.RoomId,
                                                            SlotDescription = slotCheck.SlotDescription,
                                                            SlotHardwareConfigId = slotCheck.SlotHardwareConfigId,
                                                            SlotHardwareName = slotCheck.SlotHardwareName,
                                                            SlotName = slotCheck.SlotName,
                                                            SlotPositionX = slotCheck.SlotPositionX,
                                                            SlotPositionY = slotCheck.SlotPositionY,
                                                            StatusId = slotCheck.StatusId,
                                                            StatusName = slotStatus.StatusSlotDescription
                                                        };
                                                        listSlotName += slotCheck.SlotName + ", ";
                                                        lstSlots.Add(slotView);
                                                    }
                                                }
                                            }

                                            //chấp nhận việc đặt chỗ của các order
                                            if (statusOrder == Convert.ToInt32(OrdersEnum.StatusOrder.Approve))
                                            {
                                                foreach (var s in lstSlotId)
                                                {
                                                    var slotCheck = (await _slotsRepository.FindBy(x => x.Id == Convert.ToInt32(s))).FirstOrDefault();
                                                    var slotStatus = (await _statusSlotRepository.FindBy(x => x.Id == slotCheck.StatusId)).FirstOrDefault();
                                                    if (slotCheck != null)
                                                    {
                                                        slotCheck.StatusId = Convert.ToInt32(SlotsEnum.SlotStatus.NotReady);
                                                        await _slotsRepository.Update(slotCheck, slotCheck.Id);
                                                        var slotView = new SlotsOrderViewModelOut
                                                        {
                                                            SlotId = slotCheck.Id,
                                                            RoomId = slotCheck.RoomId,
                                                            SlotDescription = slotCheck.SlotDescription,
                                                            SlotHardwareConfigId = slotCheck.SlotHardwareConfigId,
                                                            SlotHardwareName = slotCheck.SlotHardwareName,
                                                            SlotName = slotCheck.SlotName,
                                                            SlotPositionX = slotCheck.SlotPositionX,
                                                            SlotPositionY = slotCheck.SlotPositionY,
                                                            StatusId = slotCheck.StatusId,
                                                            StatusName = slotStatus.StatusSlotDescription
                                                        };
                                                        listSlotName += slotCheck.SlotName + ", ";
                                                        lstSlots.Add(slotView);
                                                    }
                                                }
                                            }
                                        }
                                    }


                                    if (statusOrder == Convert.ToInt32(OrdersEnum.StatusOrder.Cancel))
                                    {
                                        //check user hiện tại có quyền trong Order này không?
                                        if (order.CreatedBy == currentUser.Id)
                                        {
                                            foreach (var s in lstSlotId)
                                            {
                                                var slotCheck = (await _slotsRepository.FindBy(x => x.Id == Convert.ToInt32(s))).FirstOrDefault();
                                                var slotStatus = (await _statusSlotRepository.FindBy(x => x.Id == slotCheck.StatusId)).FirstOrDefault();
                                                if (slotCheck != null)
                                                {
                                                    //trả lại trạng thái của các slot là RẢNH, sẵn sàng để cho người khác book 
                                                    //Slot Status = 2 = RẢNH
                                                    slotCheck.StatusId = Convert.ToInt32(SlotsEnum.SlotStatus.Ready);
                                                    await _slotsRepository.Update(slotCheck, slotCheck.Id);
                                                    var slotView = new SlotsOrderViewModelOut
                                                    {
                                                        SlotId = slotCheck.Id,
                                                        RoomId = slotCheck.RoomId,
                                                        SlotDescription = slotCheck.SlotDescription,
                                                        SlotHardwareConfigId = slotCheck.SlotHardwareConfigId,
                                                        SlotHardwareName = slotCheck.SlotHardwareName,
                                                        SlotName = slotCheck.SlotName,
                                                        SlotPositionX = slotCheck.SlotPositionX,
                                                        SlotPositionY = slotCheck.SlotPositionY,
                                                        StatusId = slotCheck.StatusId,
                                                        StatusName = slotStatus.StatusSlotDescription
                                                    };
                                                    listSlotName += slotCheck.SlotName + ", ";
                                                    lstSlots.Add(slotView);
                                                }
                                            }
                                        }
                                    }

                                    var ownerOrder = await _usersRepository.GetUserByUserID(resultOrder.CreatedBy);
                                    var statusOrderCheck = (await _statusOrderRepository.FindBy(x => x.Id == order.StatusOrder)).FirstOrDefault();
                                    var roomOrder = (await _roomRepository.FindBy(x => x.Id == lstSlots[0].RoomId)).FirstOrDefault();
                                    var orderData = new OrderViewModelOut
                                    {
                                        OrderId = resultOrder.Id,
                                        CreatedBy = resultOrder.CreatedBy,
                                        CreatedDate = resultOrder.CreatedDate.ToString("HH:mm:ss dd/MM/yyyy"),
                                        CyberId = resultOrder.CyberId,
                                        CyberName = cyber.CyberName,
                                        EndAt = resultOrder.EndAt.ToString("HH:mm:ss dd/MM/yyyy"),
                                        StartAt = resultOrder.StartAt.ToString("HH:mm:ss dd/MM/yyyy"),
                                        StatusOrder = resultOrder.StatusOrder,
                                        StatusOrderName = statusOrderCheck.StatusOrderDescription,
                                        PhoneUser = ownerOrder.PhoneNumber,
                                        EmailUser = ownerOrder.Email,
                                        ListSlotName = listSlotName,
                                        ListSlotsOrder = lstSlots
                                    };
                                    if (roomOrder != null)
                                    {
                                        orderData.RoomName = roomOrder.RoomName;
                                    }
                                    else
                                    {
                                        orderData.RoomName = "Room NOT exist";
                                    }

                                    output.Data = orderData;
                                    output.Success = true;
                                    output.Message = "Change Status Order successfull";
                                    return output;
                                }
                                output.Message = "Change Status Order FAIL";
                                return output;
                            }
                            output.Message = "You NOT permission for this function";
                            return output;
                        }
                        output.Message = "Cyber NOT exist";
                        return output;
                    }
                    output.Message = "Order NOT exist";
                    return output;
                }
                output.Message = "YOU NOT LOGIN";
                return output;
            }
            catch (Exception e)
            {
                output.Message = e.Message;
            }
            return output;
        }

        public async Task<ServiceResponse<OrderViewModelOut>> CreateNewOrder(string usernameCrr, OrderViewModelIn orderViewModelIn)
        {
            var output = new ServiceResponse<OrderViewModelOut>();
            try
            {
                var accountCrr = await _accountsRepository.GetAccountByUsername(usernameCrr);
                if (accountCrr != null)
                {
                    var currentUser = await _usersRepository.GetUserByAccountID(accountCrr.Id);
                    var cyber = await _cybersRepository.GetCyberById(orderViewModelIn.CyberId);
                    if (cyber != null)
                    {
                        var slotOrderIdToString = string.Empty;
                        //check Slot in list slot of order has exist
                        foreach (var s in orderViewModelIn.ListSlotsOrder)
                        {
                            if (!slotOrderIdToString.Equals("") || !String.IsNullOrEmpty(slotOrderIdToString))
                            {
                                slotOrderIdToString += "|";
                            }

                            var slotCheck = await _slotsRepository.IsSlotReady(s.SlotId);
                            if (slotCheck == null)
                            {
                                output.Message = "Slot Not Ready for Book";
                                return output;
                            }
                            slotOrderIdToString += s.SlotId;
                        }
                        // kiểm tra xem người đó có đang book không VÀ không cho tạo mới 1 order khi đang có 1 order đang chờ duyệt
                        // Status = 2 = PENDING
                        //var scrrOrderCheck = await _orderRepository.GetOrderByCreatorAndStatus(currentUser.Id, Convert.ToInt32(OrdersEnum.StatusOrder.Pending));
                        var crrOrderCheck = (await _orderRepository.FindBy(x => x.CreatedBy == currentUser.Id
                        && x.StatusOrder == Convert.ToInt32(OrdersEnum.StatusOrder.Pending)
                        && x.CyberId == orderViewModelIn.CyberId)).FirstOrDefault();

                        if (crrOrderCheck != null)
                        {
                            output.Message = "You can't create new Order when has another Order";
                            return output;
                        }
                        var slotChecker = (await _slotsRepository.FindBy(x => x.Id == orderViewModelIn.ListSlotsOrder[0].SlotId)).FirstOrDefault();

                        var orderTempt = new Order
                        {
                            StartAt = orderViewModelIn.StartAt,
                            EndAt = orderViewModelIn.EndAt,
                            CreatedDate = DateTime.Now,
                            CreatedBy = currentUser.Id,
                            StatusOrder = Convert.ToInt32(OrdersEnum.StatusOrder.Pending), //PENDING  trạng thái của Order đang chờ duyệt
                            CyberId = cyber.Id,
                            PhoneNumber = currentUser.PhoneNumber,
                            RoomId = Convert.ToInt32(slotChecker.RoomId),
                            SlotOrderId = slotOrderIdToString
                        };
                        var resultOrder = (await _orderRepository.Create(orderTempt));
                        if (resultOrder != null)
                        {
                            var lstSlot = new List<SlotsOrderViewModelOut>();
                            string listSlotName = string.Empty;
                            foreach (var s in orderViewModelIn.ListSlotsOrder)
                            {
                                var slotCheck = await _slotsRepository.IsSlotReady(s.SlotId);
                                if (slotCheck != null)
                                {
                                    //update status slot
                                    //Status = 4 = đã có người book
                                    slotCheck.StatusId = Convert.ToInt32(SlotsEnum.SlotStatus.Booked);
                                    if ((await _slotsRepository.Update(slotCheck, slotCheck.Id) == -1))
                                    {
                                        output.Message = "update status slot Fail";
                                        return output;
                                    }
                                    var slotStatus = (await _statusSlotRepository.FindBy(x => x.Id == slotCheck.StatusId)).FirstOrDefault();
                                    var slotView = new SlotsOrderViewModelOut
                                    {
                                        SlotId = slotCheck.Id,
                                        RoomId = slotCheck.RoomId,
                                        SlotDescription = slotCheck.SlotDescription,
                                        SlotHardwareConfigId = slotCheck.SlotHardwareConfigId,
                                        SlotHardwareName = slotCheck.SlotHardwareName,
                                        SlotName = slotCheck.SlotName,
                                        SlotPositionX = slotCheck.SlotPositionX,
                                        SlotPositionY = slotCheck.SlotPositionY,
                                        StatusId = slotCheck.StatusId,
                                        StatusName = slotStatus.StatusSlotDescription
                                    };
                                    listSlotName += slotCheck.SlotName + ", ";
                                    lstSlot.Add(slotView);
                                }
                            }
                            var statusOrder = (await _statusOrderRepository.FindBy(x => x.Id == resultOrder.StatusOrder)).FirstOrDefault();
                            var roomOrder = (await _roomRepository.FindBy(x => x.Id == lstSlot[0].RoomId)).FirstOrDefault();
                            var orderData = new OrderViewModelOut
                            {
                                OrderId = resultOrder.Id,
                                CreatedBy = resultOrder.CreatedBy,
                                EndAt = resultOrder.EndAt.ToString("HH:mm:ss dd/MM/yyyy"),
                                StartAt = resultOrder.StartAt.ToString("HH:mm:ss dd/MM/yyyy"),
                                CreatedDate = resultOrder.CreatedDate.ToString("HH:mm:ss dd/MM/yyyy"),
                                CyberId = resultOrder.CyberId,
                                CyberName = cyber.CyberName,
                                StatusOrder = resultOrder.StatusOrder,
                                StatusOrderName = statusOrder.StatusOrderDescription,
                                PhoneUser = currentUser.PhoneNumber,
                                EmailUser = currentUser.Email,
                                ListSlotName = listSlotName,
                                RoomName = roomOrder.RoomName,
                                ListSlotsOrder = lstSlot
                            };

                            output.Data = orderData;
                            output.Message = "Crete new Order OK";
                            output.Success = true;
                            return output;
                        }
                        output.Message = "Create order FAIL";
                        return output;
                    }
                    output.Message = "Cyber Not exist";
                    return output;
                }
                output.Message = "You NOT Login";
                return output;
            }
            catch (Exception e)
            {
                output.Message = "CreateNewOrder Exception: " + e.Message;
            }
            return output;
        }

        public async Task<PagingOutput<IEnumerable<OrderViewModelOut>>> FilterOrderManager(string usernameCrr, int orderStatus, string phoneNumber, int roomId, int cyberId, int pageIndex, int pageSize)
        {
            var output = new PagingOutput<IEnumerable<OrderViewModelOut>>();
            try
            {
                var accountCrr = await _accountsRepository.GetAccountByUsername(usernameCrr);
                if (accountCrr != null)
                {
                    var currentUser = await _usersRepository.GetUserByAccountID(accountCrr.Id);
                    var cyber = await _cybersRepository.GetCyberById(cyberId);
                    if (cyber != null)
                    {
                        if ((await _cybersRepository.IsBossCyber(currentUser.Id, cyber.Id)))
                        {
                            var result = await _orderRepository.GetFilterOrdersManager(orderStatus, phoneNumber, cyber.Id, roomId, pageIndex, pageSize);

                            if (result != null)
                            {
                                var lstOrder = result.Data.ToList();
                                if (lstOrder.Count > 0)
                                {
                                    var resultListOrder = new List<OrderViewModelOut>();
                                    foreach (var o in lstOrder)
                                    {
                                        string[] lstSlotId = o.SlotOrderId.Split('|');

                                        var lstSlots = new List<SlotsOrderViewModelOut>();
                                        string listSlotName = string.Empty;
                                        foreach (var s in lstSlotId)
                                        {
                                            int i = 0;
                                            string slotIdStr = new String(s);
                                            var slot = (await _slotsRepository.FindBy(x => x.Id == Convert.ToInt32(slotIdStr))).FirstOrDefault();
                                            if (slot == null)
                                            {
                                                output.Message = "Slot not exist";
                                                return output;
                                            }

                                            var statusSlot = (await _statusSlotRepository.FindBy(x => x.Id == slot.StatusId)).FirstOrDefault();
                                            var slotView = new SlotsOrderViewModelOut
                                            {
                                                SlotId = slot.Id,
                                                RoomId = slot.RoomId,
                                                SlotName = slot.SlotName,
                                                SlotDescription = slot.SlotDescription,
                                                SlotHardwareConfigId = slot.SlotHardwareConfigId,
                                                SlotHardwareName = slot.SlotHardwareName,
                                                SlotPositionX = slot.SlotPositionX,
                                                SlotPositionY = slot.SlotPositionY,
                                                StatusId = slot.StatusId,
                                                StatusName = statusSlot.StatusSlotDescription
                                            };
                                            listSlotName += slot.SlotName + ", ";
                                            lstSlots.Add(slotView);
                                            i++;
                                        }

                                        var roomOrder = (await _roomRepository.FindBy(x => x.Id == o.RoomId)).FirstOrDefault();
                                        var ownerOrder = (await _usersRepository.GetUserByUserID(o.CreatedBy));
                                        var statusOrder = (await _statusOrderRepository.FindBy(x => x.Id == o.StatusOrder)).FirstOrDefault();

                                        var oTemp = new OrderViewModelOut
                                        {
                                            OrderId = o.Id,
                                            CreatedBy = o.CreatedBy,
                                            CreatedDate = o.CreatedDate.ToString("HH:mm:ss dd/MM/yyyy"),
                                            CyberId = cyber.Id,
                                            CyberName = cyber.CyberName,
                                            StartAt = o.StartAt.ToString("HH:mm:ss dd/MM/yyyy"),
                                            EndAt = o.EndAt.ToString("HH:mm:ss dd/MM/yyyy"),
                                            StatusOrder = statusOrder.Id,
                                            StatusOrderName = statusOrder.StatusOrderDescription,
                                            ListSlotName = listSlotName,
                                            ListSlotsOrder = lstSlots
                                        };
                                        if (ownerOrder != null)
                                        {
                                            oTemp.EmailUser = ownerOrder.Email;
                                            oTemp.PhoneUser = ownerOrder.PhoneNumber;
                                        }
                                        else
                                        {
                                            oTemp.EmailUser = "email deleted";
                                            oTemp.PhoneUser = "phone deleted";
                                        }

                                        if (roomOrder != null)
                                        {
                                            oTemp.RoomName = roomOrder.RoomName;
                                        }
                                        else
                                        {
                                            oTemp.RoomName = "Phòng Đã Xóa";
                                        }

                                        var rateUser = (await _cyberRateUserRepository.FindBy(x => x.OrderId == oTemp.OrderId)).FirstOrDefault();
                                        if (rateUser != null)
                                        {
                                            var rateOut = new RateUserOutViewModel
                                            {
                                                RateId = rateUser.Id,
                                                UsersId = rateUser.UsersId,
                                                RatePoint = rateUser.RatePoint,
                                                CommentContent = rateUser.CommentContent,
                                                CyberId = rateUser.CyberId,
                                                CreatedDate = Convert.ToDateTime(rateUser.CreatedDate).ToString("HH:mm:ss dd/MM/yyyy"),
                                                UpdatedDate = Convert.ToDateTime(rateUser.UpdatedDate).ToString("HH:mm:ss dd/MM/yyyy"),
                                                Edited = rateUser.Edited,
                                                OrderId = rateUser.OrderId,
                                            };
                                            oTemp.RateUser = rateOut;
                                        }
                                        resultListOrder.Add(oTemp);
                                    }
                                    output.Data = resultListOrder;
                                    output.Index = result.Index;
                                    output.PageSize = result.PageSize;
                                    output.TotalItem = result.TotalItem;
                                    output.TotalPage = result.TotalPage;
                                    output.Message = "successfull";
                                    return output;
                                }
                            }
                            output.Message = "No Orders in this Cyber";
                            return output;
                        }
                        output.Message = "You NOT permission this Function";
                        return output;
                    }
                    output.Message = "Cyber NOT exist";
                    return output;
                }
                output.Message = "You NOT LOGIN";
                return output;
            }
            catch (Exception e)
            {
                output.Message = e.Message;
            }
            return output;
        }

        public async Task<ServiceResponse<OrderViewModelOut>> GetBookingById(string usernameCrr, int orderId)
        {
            var output = new ServiceResponse<OrderViewModelOut>();
            try
            {
                var accountCrr = await _accountsRepository.GetAccountByUsername(usernameCrr);
                if (accountCrr != null)
                {
                    var currentUser = await _usersRepository.GetUserByAccountID(accountCrr.Id);
                    var order = await _orderRepository.GetOrderById(orderId);
                    if (order != null)
                    {
                        var cyber = await _cybersRepository.GetCyberById(order.CyberId);
                        if (cyber != null)
                        {
                            //check user hiện tại có quyền trong Cyber này không?
                            //Hoặc là chủ Cyber của order này chủ sở hữu của order này
                            if (cyber.BossCyberID == currentUser.Id || order.CreatedBy == currentUser.Id)
                            {
                                string[] lstSlotId = order.SlotOrderId.Split('|');
                                var lstSlots = new List<SlotsOrderViewModelOut>();
                                string listSlotName = string.Empty;
                                foreach (var s in lstSlotId)
                                {
                                    int i = 0;
                                    string slotIdStr = new String(s);
                                    var slot = (await _slotsRepository.FindBy(x => x.Id == Convert.ToInt32(slotIdStr))).FirstOrDefault();
                                    if (slot == null)
                                    {
                                        output.Message = "Slot not exist";
                                        return output;
                                    }

                                    var statusSlot = (await _statusSlotRepository.FindBy(x => x.Id == slot.StatusId)).FirstOrDefault();
                                    var slotView = new SlotsOrderViewModelOut
                                    {
                                        SlotId = slot.Id,
                                        RoomId = slot.RoomId,
                                        SlotName = slot.SlotName,
                                        SlotDescription = slot.SlotDescription,
                                        SlotHardwareConfigId = slot.SlotHardwareConfigId,
                                        SlotHardwareName = slot.SlotHardwareName,
                                        SlotPositionX = slot.SlotPositionX,
                                        SlotPositionY = slot.SlotPositionY,
                                        StatusId = slot.StatusId,
                                        StatusName = statusSlot.StatusSlotDescription
                                    };
                                    listSlotName += slot.SlotName + ", ";
                                    lstSlots.Add(slotView);
                                    i++;
                                }

                                var ownerOrder = await _usersRepository.GetUserByUserID(order.CreatedBy);
                                var statusOrder = (await _statusOrderRepository.FindBy(x => x.Id == order.StatusOrder)).FirstOrDefault();
                                var roomOrder = (await _roomRepository.FindBy(x => x.Id == lstSlots[0].RoomId)).FirstOrDefault();
                                var orderViewOut = new OrderViewModelOut
                                {
                                    OrderId = order.Id,
                                    StartAt = order.StartAt.ToString("HH:mm:ss dd/MM/yyyy"),
                                    EndAt = order.EndAt.ToString("HH:mm:ss dd/MM/yyyy"),
                                    CreatedDate = order.CreatedDate.ToString("HH:mm:ss dd/MM/yyyy"),
                                    CreatedBy = order.CreatedBy,
                                    StatusOrder = order.StatusOrder,
                                    CyberId = order.CyberId,
                                    CyberName = cyber.CyberName,
                                    EmailUser = ownerOrder.Email,
                                    PhoneUser = ownerOrder.PhoneNumber,
                                    StatusOrderName = statusOrder.StatusOrderDescription,
                                    RoomName = roomOrder.RoomName,
                                    ListSlotName = listSlotName,
                                    ListSlotsOrder = lstSlots
                                };
                                output.Data = orderViewOut;
                                output.Message = "successfull";
                                output.Success = true;
                                return output;
                            }
                            output.Message = "You NOT permission this Function";
                            return output;
                        }
                        output.Message = "Cyber NOT Exist";
                        return output;
                    }
                    output.Message = "Order NOT exist";
                    return output;
                }
                output.Message = "You NOT LOGIN";
                return output;
            }
            catch (Exception ex)
            {
                output.Message = "GetBookingById: " + ex.Message;
            }
            return output;
        }

        public async Task<PagingOutput<IEnumerable<OrderViewModelOut>>> GetListBookingByCyberId(string usernameCrr, int pageIndex, int pageSize, int cyberId)
        {
            var output = new PagingOutput<IEnumerable<OrderViewModelOut>>();
            try
            {
                var accountCrr = await _accountsRepository.GetAccountByUsername(usernameCrr);
                if (accountCrr != null)
                {
                    var currentUser = await _usersRepository.GetUserByAccountID(accountCrr.Id);
                    var cyber = await _cybersRepository.GetCyberById(cyberId);
                    if (cyber != null)
                    {
                        if ((await _cybersRepository.IsBossCyber(currentUser.Id, cyber.Id)))
                        {
                            var result = await _orderRepository.GetAllOrderByCyberId(cyberId, pageIndex, pageSize);
                            if (result != null)
                            {
                                var lstOrder = result.Data.ToList();
                                if (lstOrder.Count > 0)
                                {
                                    var resultListOrder = new List<OrderViewModelOut>();
                                    foreach (var o in lstOrder)
                                    {
                                        string[] lstSlotId = o.SlotOrderId.Split('|');

                                        var lstSlots = new List<SlotsOrderViewModelOut>();
                                        string listSlotName = string.Empty;
                                        foreach (var s in lstSlotId)
                                        {
                                            int i = 0;
                                            string slotIdStr = new String(s);
                                            var slot = (await _slotsRepository.FindBy(x => x.Id == Convert.ToInt32(slotIdStr))).FirstOrDefault();
                                            if (slot == null)
                                            {
                                                output.Message = "Slot not exist";
                                                return output;
                                            }
                                            var statusSlot = (await _statusSlotRepository.FindBy(x => x.Id == slot.StatusId)).FirstOrDefault();
                                            var slotView = new SlotsOrderViewModelOut
                                            {
                                                SlotId = slot.Id,
                                                RoomId = slot.RoomId,
                                                SlotName = slot.SlotName,
                                                SlotDescription = slot.SlotDescription,
                                                SlotHardwareConfigId = slot.SlotHardwareConfigId,
                                                SlotHardwareName = slot.SlotHardwareName,
                                                SlotPositionX = slot.SlotPositionX,
                                                SlotPositionY = slot.SlotPositionY,
                                                StatusId = slot.StatusId,
                                                StatusName = statusSlot.StatusSlotDescription
                                            };
                                            listSlotName += slot.SlotName + ", ";
                                            lstSlots.Add(slotView);
                                            i++;
                                        }

                                        var roomOrder = (await _roomRepository.FindBy(x => x.Id == lstSlots[0].RoomId)).FirstOrDefault();
                                        var ownerOrder = (await _usersRepository.GetUserByUserID(o.CreatedBy));
                                        var statusOrder = (await _statusOrderRepository.FindBy(x => x.Id == o.StatusOrder)).FirstOrDefault();

                                        var oTemp = new OrderViewModelOut
                                        {
                                            OrderId = o.Id,
                                            CreatedBy = o.CreatedBy,
                                            CreatedDate = o.CreatedDate.ToString("HH:mm:ss dd/MM/yyyy"),
                                            CyberId = cyber.Id,
                                            CyberName = cyber.CyberName,
                                            StartAt = o.StartAt.ToString("HH:mm:ss dd/MM/yyyy"),
                                            EndAt = o.EndAt.ToString("HH:mm:ss dd/MM/yyyy"),
                                            StatusOrder = statusOrder.Id,
                                            StatusOrderName = statusOrder.StatusOrderDescription,
                                            ListSlotName = listSlotName,
                                            ListSlotsOrder = lstSlots
                                        };

                                        if (ownerOrder != null)
                                        {
                                            oTemp.EmailUser = ownerOrder.Email;
                                            oTemp.PhoneUser = ownerOrder.PhoneNumber;
                                        }
                                        else
                                        {
                                            oTemp.EmailUser = "email deleted";
                                            oTemp.PhoneUser = "phone deleted";
                                        }

                                        if (roomOrder != null)
                                        {
                                            oTemp.RoomName = roomOrder.RoomName;
                                        }
                                        else
                                        {
                                            oTemp.RoomName = "Phòng Đã Xóa";
                                        }

                                        resultListOrder.Add(oTemp);
                                    }

                                    output.Data = resultListOrder;
                                    output.Index = result.Index;
                                    output.PageSize = result.PageSize;
                                    output.TotalItem = result.TotalItem;
                                    output.TotalPage = result.TotalPage;
                                    output.Message = "successfull";
                                    return output;
                                }
                            }
                            output.Message = "No Orders in this Cyber";
                            return output;
                        }
                        output.Message = "You NOT permission this Function";
                        return output;
                    }
                    output.Message = "Cyber NOT exist";
                    return output;
                }
                output.Message = "You NOT LOGIN";
                return output;
            }
            catch (Exception ex)
            {
                output.Message = "GetListBookingByCyberId Exception: " + ex.Message;
            }
            return output;
        }

        public async Task<PagingOutput<IEnumerable<OrderViewModelOut>>> GetMyListBooking(string usernameCrr, int pageIndex, int pageSize)
        {
            var output = new PagingOutput<IEnumerable<OrderViewModelOut>>();
            try
            {
                var accountCrr = await _accountsRepository.GetAccountByUsername(usernameCrr);
                if (accountCrr != null)
                {
                    var currentUser = await _usersRepository.GetUserByAccountID(accountCrr.Id);
                    var result = await _orderRepository.GetAllOrderByUserId(currentUser.Id, pageIndex, pageSize);
                    if (result != null)
                    {
                        var lstOrder = result.Data.ToList();
                        if (lstOrder.Count > 0)
                        {
                            var resultListOrder = new List<OrderViewModelOut>();
                            foreach (var o in lstOrder)
                            {
                                string[] lstSlotId = o.SlotOrderId.Split('|');
                                var lstSlots = new List<SlotsOrderViewModelOut>();
                                string listSlotName = string.Empty;
                                foreach (var s in lstSlotId)
                                {
                                    int i = 0;
                                    string slotIdStr = new String(s);
                                    var slot = (await _slotsRepository.FindBy(x => x.Id == Convert.ToInt32(slotIdStr))).FirstOrDefault();
                                    if (slot == null)
                                    {
                                        output.Message = "Slot not exist";
                                        return output;
                                    }
                                    var statusSlot = (await _statusSlotRepository.FindBy(x => x.Id == slot.StatusId)).FirstOrDefault();
                                    var slotView = new SlotsOrderViewModelOut
                                    {
                                        SlotId = slot.Id,
                                        RoomId = slot.RoomId,
                                        SlotName = slot.SlotName,
                                        SlotDescription = slot.SlotDescription,
                                        SlotHardwareConfigId = slot.SlotHardwareConfigId,
                                        SlotHardwareName = slot.SlotHardwareName,
                                        SlotPositionX = slot.SlotPositionX,
                                        SlotPositionY = slot.SlotPositionY,
                                        StatusId = slot.StatusId,
                                        StatusName = statusSlot.StatusSlotDescription
                                    };
                                    listSlotName += slot.SlotName + ", ";
                                    lstSlots.Add(slotView);
                                    i++;
                                }

                                var roomOrder = (await _roomRepository.FindBy(x => x.Id == lstSlots[0].RoomId)).FirstOrDefault();
                                var ownerOrder = (await _usersRepository.GetUserByUserID(o.CreatedBy));
                                var statusOrder = (await _statusOrderRepository.FindBy(x => x.Id == o.StatusOrder)).FirstOrDefault();
                                var cyber = (await _cybersRepository.GetCyberById(o.CyberId));
                                var oTemp = new OrderViewModelOut
                                {
                                    OrderId = o.Id,
                                    CreatedBy = o.CreatedBy,
                                    CreatedDate = o.CreatedDate.ToString("HH:mm:ss dd/MM/yyyy"),
                                    CyberId = cyber.Id,
                                    CyberName = cyber.CyberName,
                                    EmailUser = ownerOrder.Email,
                                    PhoneUser = ownerOrder.PhoneNumber,
                                    StartAt = o.StartAt.ToString("HH:mm:ss dd/MM/yyyy"),
                                    EndAt = o.EndAt.ToString("HH:mm:ss dd/MM/yyyy"),
                                    StatusOrder = statusOrder.Id,
                                    StatusOrderName = statusOrder.StatusOrderDescription,
                                    ListSlotName = listSlotName,
                                    ListSlotsOrder = lstSlots,
                                };

                                if (roomOrder != null)
                                {
                                    oTemp.RoomName = roomOrder.RoomName;
                                }
                                else
                                {
                                    oTemp.RoomName = "Phòng Đã Xóa";
                                }

                                var rateCyber = (await _userRateCyberRepository.FindBy(x => x.OrderId == oTemp.OrderId)).FirstOrDefault();
                                if (rateCyber != null)
                                {
                                    var rateOut = new RateCyberOutViewModel
                                    {
                                        RateId = rateCyber.Id,
                                        UserId = rateCyber.UserId,
                                        RatePoint = rateCyber.RatePoint,
                                        CommentContent = rateCyber.CommentContent,
                                        CyberId = rateCyber.CyberId,
                                        CreatedDate = Convert.ToDateTime(rateCyber.CreatedDate).ToString("HH:mm:ss dd/MM/yyyy"),
                                        UpdatedDate = Convert.ToDateTime(rateCyber.UpdatedDate).ToString("HH:mm:ss dd/MM/yyyy"),
                                        Edited = rateCyber.Edited,
                                        OrderId = rateCyber.OrderId,
                                    };
                                    oTemp.RateCyber = rateOut;
                                }
                                resultListOrder.Add(oTemp);
                            }

                            output.Data = resultListOrder;
                            output.Index = result.Index;
                            output.PageSize = result.PageSize;
                            output.TotalItem = result.TotalItem;
                            output.TotalPage = result.TotalPage;
                            output.Message = "successfull";
                            return output;
                        }
                    }
                    output.Message = "No Orders in this Cyber";
                    return output;
                }
                output.Message = "You NOT LOGIN";
                return output;
            }
            catch (Exception ex)
            {
                output.Message = ex.Message;
            }
            return output;
        }

        public async Task<ServiceResponse<IEnumerable<Slot>>> CheckSplitSlotInOrder(int orderId)
        {
            var output = new ServiceResponse<IEnumerable<Slot>>();

            var order = (await _orderRepository.FindBy(x => x.Id == orderId)).FirstOrDefault();
            string[] lstSlotId = order.SlotOrderId.Split('|');
            var lstSlots = new List<Slot>();
            foreach (var s in lstSlotId)
            {
                int i = 0;
                var slot = (await _slotsRepository.FindBy(x => x.Id == Convert.ToInt32(s[i].ToString()))).FirstOrDefault();
                if (slot == null)
                {
                    output.Message = "Slot not exist";
                    return output;
                }
                lstSlots.Add(slot);
                i++;
            }
            output.Data = lstSlots;
            return output;
        }

        public async Task<ServiceResponse<IEnumerable<Cyber>>> GetAllCyberBookedByUserId(string usernameCrr)
        {
            var output = new ServiceResponse<IEnumerable<Cyber>>();
            try
            {
                var accountCrr = await _accountsRepository.GetAccountByUsername(usernameCrr);
                if (accountCrr != null)
                {
                    var currentUser = await _usersRepository.GetUserByAccountID(accountCrr.Id);
                    if (usernameCrr != null)
                    {
                        var lstOrderUser = (await _orderRepository.FindBy(x => x.CreatedBy == currentUser.Id)).ToList();

                        var lstOrderTemp = lstOrderUser.GroupBy(x => x.CyberId)
                                                        .Select(s => s.First())
                                                        .ToList();
                        List<Cyber> lstCyber = new List<Cyber>();
                        foreach(var z in lstOrderTemp)
                        {
                            var a = (await _cybersRepository.GetCyberById(z.CyberId));
                            lstCyber.Add(a);
                        }
                        output.Data = lstCyber;
                        output.Message = "Get GetAllCyberBookedByUserId OK";
                        output.Success = true;
                        return output;
                    }
                }
                output.Message = "You NOT Login";
                return output;
            }
            catch (Exception e)
            {
                output.Message = "GetAllCyberBookedByUserId exception: " + e.Message;
            }
            return output;
        }
    }
}
