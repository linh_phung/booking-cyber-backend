﻿using CyberBook_API.Dal.Repositories;
using CyberBook_API.Enum;
using CyberBook_API.Interfaces;
using CyberBook_API.Models;
using CyberBook_API.Services.Interfaces;
using CyberBook_API.ViewModel.PagingView;
using CyberBook_API.ViewModel.RoomViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CyberBook_API.Services
{
    public class RoomsService : IRoomsService
    {
        private readonly ISlotsRepository _slotsRepository = new SlotsRepository();
        private readonly ICybersRepository _cybersRepository = new CybersRepository();
        private readonly IAccountsRepository _accountsRepository = new AccountsRepository();
        private readonly IUsersRepository _usersRepository = new UsersRepository();
        private readonly IRoomRepository _roomRepository = new RoomRepository();
        private readonly IRoomTypeRepository _roomTypeRepository = new RoomTypeRepository();
        private readonly IHardwareConfigRepository _hardwareConfigRepository = new HardwareConfigRepository();

        public async Task<ServiceResponse<RoomOutViewModel>> CreateNewRoom(string usernameCrr, RoomCreateViewModel roomIn)
        {
            var output = new ServiceResponse<RoomOutViewModel>();
            try
            {
                var accountCrr = await _accountsRepository.GetAccountByUsername(usernameCrr);
                if (accountCrr != null)
                {
                    var currentUser = await _usersRepository.GetUserByAccountID(accountCrr.Id);
                    var cyber = await _cybersRepository.GetCyberById(roomIn.CyberId);
                    if (cyber != null)
                    {
                        var lstHardwareInCyber = (await _hardwareConfigRepository.GetSlotHardwareByCyberId(cyber.Id)).ToList();
                        if (lstHardwareInCyber.Count <= 0)
                        {
                            output.Message = "No hardware in Cyber";
                            return output;
                        }

                        if (await _cybersRepository.IsBossCyber(currentUser.Id, cyber.Id))
                        {
                            if (!roomIn.RoomName.Equals(""))
                            {
                                var roomTempt = new Room
                                {
                                    CyberId = cyber.Id,
                                    RoomName = roomIn.RoomName,
                                    //RoomType = roomIn.RoomType,
                                    RoomTypeName = roomIn.RoomTypeName,
                                    RoomPosition = roomIn.RoomPosition,
                                    PriceRoom = roomIn.PriceRoom,
                                    MaxX = roomIn.MaxX,
                                    MaxY = roomIn.MaxY,
                                    HardwareConfigId = roomIn.HardwareConfigId,
                                    RoomImage = roomIn.RoomImage ?? "QmUZA7AKDXkvs6YBmkWRZe6uiu9iRvBa5EjbNMqRpBX86n"
                                };

                                var newRoom = await _roomRepository.Create(roomTempt);
                                if (newRoom != null)
                                {
                                    var cyberRoom = await _cybersRepository.GetCyberById(newRoom.CyberId);
                                    var hardwareRoom = await _hardwareConfigRepository.GetSlotHardwareById(newRoom.HardwareConfigId);
                                    var roomOut = new RoomOutViewModel
                                    {
                                        CyberId = cyberRoom.Id,
                                        RoomId = newRoom.Id,
                                        CyberName = cyberRoom.CyberName,
                                        HardwareConfigId = hardwareRoom.Id,
                                        HardwareName = hardwareRoom.NameHardware,
                                        MaxX = newRoom.MaxX,
                                        MaxY = newRoom.MaxY,
                                        PriceRoom = newRoom.PriceRoom,
                                        RoomName = newRoom.RoomName,
                                        RoomPosition = newRoom.RoomPosition,
                                        RoomTypeName = newRoom.RoomTypeName,
                                        RoomImage = newRoom.RoomImage,
                                        CPU = hardwareRoom.Ram,
                                        GPU = hardwareRoom.Gpu,
                                        Ram = hardwareRoom.Ram,
                                        Monitor = hardwareRoom.Monitor,
                                        SlotReadyInRoom = await _slotsRepository.CountSlotReadyByRoomId(newRoom.Id)
                                    };

                                    output.Data = roomOut;
                                    output.Message = "Create New Room Successfully";
                                    output.Success = true;
                                    return output;
                                }
                                output.Message = "Create New Room Fail";
                                return output;
                            }
                            output.Message = "RoomName CAN NOT empty";
                            return output;
                        }
                        output.Message = "You NOT permission for this Function";
                        return output;

                    }
                    output.Message = "Cyber NOT exist";
                    return output;
                }
                output.Message = "YOU NOT LOGIN";
                return output;
            }
            catch (Exception e)
            {
                output.Message = "CreateNewRoom Exception: " + e.Message;
            }
            return output;
        }

        public async Task<ServiceResponse<RoomOutViewModel>> EditRoomInfor(string usernameCrr, RoomEditViewModel roomIn)
        {
            var output = new ServiceResponse<RoomOutViewModel>();
            try
            {
                var accountCrr = await _accountsRepository.GetAccountByUsername(usernameCrr);
                if (accountCrr != null)
                {
                    var currentUser = await _usersRepository.GetUserByAccountID(accountCrr.Id);
                    var room = await _roomRepository.GetRoomById(roomIn.RoomId);
                    if (room != null)
                    {
                        var cyber = await _cybersRepository.GetCyberById(room.CyberId);
                        if (cyber != null)
                        {
                            //check user hiện tại có quyền trong Cyber này không?
                            if (await _cybersRepository.IsBossCyber(currentUser.Id, cyber.Id))
                            {
                                //var roomType = (await _roomTypeRepository.FindBy(x => x.Id == roomViewModelIn.Room.RoomType)).FirstOrDefault();
                                if (!roomIn.RoomName.Equals(""))
                                {
                                    room.RoomName = roomIn.RoomName;
                                    room.RoomPosition = roomIn.RoomPosition;
                                    room.RoomTypeName = roomIn.RoomTypeName;
                                    room.PriceRoom = roomIn.PriceRoom;
                                    room.RoomImage = roomIn.RoomImage;
                                    room.HardwareConfigId = roomIn.HardwareConfigId;
                                    var newRoom = await _roomRepository.Update(room, room.Id);
                                    if (newRoom != -1)
                                    {
                                        var cyberRoom = await _cybersRepository.GetCyberById(room.CyberId);
                                        var hardwareRoom = await _hardwareConfigRepository.GetSlotHardwareById(room.HardwareConfigId);

                                        var config = await _hardwareConfigRepository.GetSlotHardwareById(room.HardwareConfigId);

                                        var roomOut = new RoomOutViewModel
                                        {
                                            CyberId = room.Id,
                                            RoomId = room.Id,
                                            CyberName = cyberRoom.CyberName,
                                            HardwareConfigId = hardwareRoom.Id,
                                            HardwareName = hardwareRoom.NameHardware,
                                            MaxX = room.MaxX,
                                            MaxY = room.MaxY,
                                            PriceRoom = roomIn.PriceRoom,
                                            RoomName = roomIn.RoomName,
                                            RoomPosition = roomIn.RoomPosition,
                                            RoomTypeName = room.RoomTypeName,
                                            RoomImage = roomIn.RoomImage,
                                            SlotReadyInRoom = await _slotsRepository.CountSlotReadyByRoomId(room.Id),
                                            CPU = config.Cpu,
                                            GPU = config.Gpu,
                                            Monitor = config.Monitor,
                                            Ram = config.Ram
                                        };

                                        output.Data = roomOut;
                                        output.Message = "Èdit Room Successfully";
                                        output.Success = true;
                                        return output;
                                    }
                                    output.Message = "Create New Room Fail";
                                    return output;
                                }
                                output.Message = "RoomName CAN NOT empty";
                                return output;
                            }
                            output.Message = "You NOT permission for this Function";
                            return output;
                        }
                        output.Message = "Cyber NOT exist";
                        return output;
                    }
                    output.Message = "Room NOT exist";
                    return output;
                }
                output.Message = "YOU NOT LOGIN";
                return output;
            }
            catch (Exception e)
            {
                output.Message = "EditRoom Exception: " + e.Message;
            }
            return output;
        }

        public async Task<ServiceResponse<RoomOutViewModel>> EditSizeRoom(string usernameCrr, int roomId, int maxX, int maxY)
        {
            var output = new ServiceResponse<RoomOutViewModel>();
            try
            {
                var accountCrr = await _accountsRepository.GetAccountByUsername(usernameCrr);
                if (accountCrr != null)
                {
                    var currentUser = await _usersRepository.GetUserByAccountID(accountCrr.Id);
                    var cyber = await _cybersRepository.GetCyberByBossCyberId(currentUser.Id);
                    //check user hiện tại có quyền trong Cyber này không?
                    if ((await _cybersRepository.IsBossCyber(currentUser.Id, cyber.Id)))
                    {
                        var room = await _roomRepository.GetRoomById(roomId);
                        if (room != null)
                        {
                            var newRooms = await _roomRepository.EditRoomSize(room.Id, maxX, maxY);
                            if (newRooms != null)
                            {
                                var cyberRoom = await _cybersRepository.GetCyberById(newRooms.CyberId);
                                var hardwareRoom = await _hardwareConfigRepository.GetSlotHardwareById(newRooms.HardwareConfigId);
                                var roomOut = new RoomOutViewModel
                                {
                                    CyberId = cyberRoom.Id,
                                    RoomId = newRooms.Id,
                                    CyberName = cyberRoom.CyberName,
                                    HardwareConfigId = hardwareRoom.Id,
                                    HardwareName = hardwareRoom.NameHardware,
                                    MaxX = newRooms.MaxX,
                                    MaxY = newRooms.MaxY,
                                    PriceRoom = newRooms.PriceRoom,
                                    RoomName = newRooms.RoomName,
                                    RoomPosition = newRooms.RoomPosition,
                                    RoomTypeName = newRooms.RoomTypeName,
                                    RoomImage = newRooms.RoomImage,
                                    SlotReadyInRoom = await _slotsRepository.CountSlotReadyByRoomId(room.Id)
                                };

                                output.Data = roomOut;
                                output.Message = "Update Size Room Successfully";
                                output.Success = true;
                                return output;
                            }
                            output.Message = "Size Room Fail";
                            return output;
                        }
                        output.Message = "Room NOT exist";
                        return output;
                    }
                    output.Message = "You NOT permission for this Function";
                    return output;
                }
                output.Message = "You NOT LOGIN";
                return output;
            }
            catch (Exception e)
            {
                output.Message = "EditSizeRoom Exception: " + e.Message;
            }
            return output;
        }

        public async Task<ServiceResponse<RoomOutViewModel>> EditSizeRoomAllow(string usernameCrr, int roomId, int maxX, int maxY)
        {
            var output = new ServiceResponse<RoomOutViewModel>();
            try
            {
                var accountCrr = await _accountsRepository.GetAccountByUsername(usernameCrr);
                if (accountCrr != null)
                {
                    var currentUser = await _usersRepository.GetUserByAccountID(accountCrr.Id);
                    var cyber = await _cybersRepository.GetCyberByBossCyberId(currentUser.Id);
                    if (cyber != null)
                    {
                        //check user hiện tại có quyền trong Cyber này không?
                        if ((await _cybersRepository.IsBossCyber(currentUser.Id, cyber.Id)))
                        {
                            var room = await _roomRepository.GetRoomById(roomId);
                            if (room != null)
                            {
                                var newRooms = await _roomRepository.EditRoomSizeAllow(room.Id, maxX, maxY);
                                if (newRooms != null)
                                {
                                    var cyberRoom = await _cybersRepository.GetCyberById(newRooms.CyberId);
                                    var hardwareRoom = await _hardwareConfigRepository.GetSlotHardwareById(newRooms.HardwareConfigId);
                                    var roomOut = new RoomOutViewModel
                                    {
                                        CyberId = cyberRoom.Id,
                                        RoomId = newRooms.Id,
                                        CyberName = cyberRoom.CyberName,
                                        HardwareConfigId = hardwareRoom.Id,
                                        HardwareName = hardwareRoom.NameHardware,
                                        MaxX = newRooms.MaxX,
                                        MaxY = newRooms.MaxY,
                                        PriceRoom = newRooms.PriceRoom,
                                        RoomName = newRooms.RoomName,
                                        RoomPosition = newRooms.RoomPosition,
                                        RoomTypeName = newRooms.RoomTypeName,
                                        RoomImage = newRooms.RoomImage,
                                        SlotReadyInRoom = await _slotsRepository.CountSlotReadyByRoomId(room.Id)
                                    };

                                    output.Data = roomOut;
                                    output.Message = "Update Size Room Successfully";
                                    output.Success = true;
                                    return output;
                                }
                                output.Message = "Size Room Fail";
                                return output;
                            }
                            output.Message = "Room NOT exist";
                            return output;
                        }
                        output.Message = "You NOT permission for this Function";
                        return output;
                    }
                    output.Message = "Cyber NOT Exist";
                    return output;
                }
                output.Message = "YOU NOT LOGIN";
                return output;
            }

            catch (Exception e)
            {
                output.Message = "EditSizeRoom Exception: " + e.Message;
            }
            return output;
        }

        public async Task<PagingOutput<IEnumerable<RoomOutViewModel>>> GetListRoomByCyberId(int pageIndex, int pageSize, int cyberId)
        {
            var output = new PagingOutput<IEnumerable<RoomOutViewModel>>();
            try
            {
                var cyber = await _cybersRepository.GetCyberById(cyberId);
                if (cyber != null)
                {
                    var resultRoom = await _roomRepository.GetListRoomByCyberId(cyberId, pageIndex, pageSize);
                    if (resultRoom != null)
                    {
                        var lstRoom = new List<RoomOutViewModel>();
                        foreach (var r in resultRoom.Data.ToList())
                        {
                            var cyberRoom = await _cybersRepository.GetCyberById(r.CyberId);
                            var hardwareRoom = await _hardwareConfigRepository.GetSlotHardwareById(r.HardwareConfigId);

                            var config = await _hardwareConfigRepository.GetSlotHardwareById(r.HardwareConfigId);

                            var roomOut = new RoomOutViewModel
                            {
                                CyberId = cyberRoom.Id,
                                RoomId = r.Id,
                                CyberName = cyberRoom.CyberName,
                                HardwareConfigId = hardwareRoom.Id,
                                HardwareName = hardwareRoom.NameHardware,
                                MaxX = r.MaxX,
                                MaxY = r.MaxY,
                                PriceRoom = r.PriceRoom,
                                RoomName = r.RoomName,
                                RoomPosition = r.RoomPosition,
                                RoomTypeName = r.RoomTypeName,
                                RoomImage = r.RoomImage,
                                SlotReadyInRoom = await _slotsRepository.CountSlotReadyByRoomId(r.Id),
                                CPU = config.Cpu,
                                GPU = config.Gpu,
                                Monitor = config.Monitor,
                                Ram = config.Ram
                            };
                            lstRoom.Add(roomOut);
                        }
                        output.Index = resultRoom.Index;
                        output.PageSize = resultRoom.PageSize;
                        output.TotalItem = resultRoom.TotalItem;
                        output.TotalPage = resultRoom.TotalPage;
                        output.Data = lstRoom;
                        output.Message = "Successfully";
                        return output;
                    }
                    output.Message = "Room NOT exist";
                    return output;
                }
                output.Message = "Cyber NOT exist";
                return output;
            }
            catch (Exception e)
            {
                output.Message = "GetRoomById Exception: " + e.Message;
            }
            return output;
        }

        public async Task<ServiceResponse<RoomOutViewModel>> GetRoomById(int roomId)
        {
            var output = new ServiceResponse<RoomOutViewModel>();
            try
            {
                var room = await _roomRepository.GetRoomById(roomId);
                if (room != null)
                {
                    var cyberRoom = await _cybersRepository.GetCyberById(room.CyberId);
                    var hardwareRoom = await _hardwareConfigRepository.GetSlotHardwareById(room.HardwareConfigId);

                    var config = await _hardwareConfigRepository.GetSlotHardwareById(room.HardwareConfigId);

                    var roomOut = new RoomOutViewModel
                    {
                        CyberId = cyberRoom.Id,
                        RoomId = room.Id,
                        CyberName = cyberRoom.CyberName,
                        HardwareConfigId = hardwareRoom.Id,
                        HardwareName = hardwareRoom.NameHardware,
                        MaxX = room.MaxX,
                        MaxY = room.MaxY,
                        PriceRoom = room.PriceRoom,
                        RoomName = room.RoomName,
                        RoomPosition = room.RoomPosition,
                        RoomTypeName = room.RoomTypeName,
                        RoomImage = room.RoomImage,
                        SlotReadyInRoom = await _slotsRepository.CountSlotReadyByRoomId(room.Id),
                        CPU = config.Cpu,
                        GPU = config.Gpu,
                        Monitor = config.Monitor,
                        Ram = config.Ram
                    };
                    output.Data = roomOut;
                    output.Message = "Successfully";
                    output.Success = true;
                    return output;
                }
                output.Message = "Room NOT exist";
                return output;
            }
            catch (Exception e)
            {
                output.Message = "GetRoomById Exception: " + e.Message;
            }
            return output;
        }

        public async Task<ServiceResponse<int>> RemoveRoom(string usernameCrr, int roomId)
        {
            var output = new ServiceResponse<int>();
            try
            {
                var accountCrr = await _accountsRepository.GetAccountByUsername(usernameCrr);
                if (accountCrr != null)
                {
                    var currentUser = await _usersRepository.GetUserByAccountID(accountCrr.Id);
                    var cyber = await _cybersRepository.GetCyberByBossCyberId(currentUser.Id);
                    //check user hiện tại có quyền trong Cyber này không?
                    if (cyber.BossCyberID == currentUser.Id)
                    {
                        var room = await _roomRepository.GetRoomById(roomId);
                        if (room != null)
                        {
                            var lstSlotInRoom = (await _slotsRepository.FindBy(x => x.RoomId == room.Id)).ToList();
                            foreach(var z in lstSlotInRoom)
                            {
                                await _slotsRepository.UpdateStatusSlot(z.Id, Convert.ToInt32(SlotsEnum.SlotStatus.Unload));
                            }

                            if ((await _roomRepository.Delete(room)) != -1)
                            {
                                output.Message = "Successful";
                                output.Data = 1;
                                output.Success = true;
                                return output;
                            }
                            output.Message = "CAN NOT Remove Room";
                            return output;
                        }
                        output.Message = "Room NOT exist";
                        return output;
                    }
                    output.Message = "You NOT permission for this Function";
                    return output;
                }
                output.Message = "YOU NOT LOGIN";
                return output;
            }
            catch (Exception e)
            {
                output.Message = "RemoveRoom Exception: " + e.Message;
            }
            return output;
        }
    }
}
