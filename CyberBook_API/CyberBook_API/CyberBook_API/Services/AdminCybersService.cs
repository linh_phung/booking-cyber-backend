﻿using CyberBook_API.Dal.Repositories;
using CyberBook_API.Enum;
using CyberBook_API.Interfaces;
using CyberBook_API.Models;
using CyberBook_API.Services.Interfaces;
using CyberBook_API.ViewModel.CyberViewModel;
using CyberBook_API.ViewModel.PagingView;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace CyberBook_API.Services
{
    public class AdminCybersService : IAdminCybersService
    {
        private readonly IAdminsRepository _adminsRepository = new AdminsRepository();
        private readonly ICybersRepository _cybersRepository = new CybersRepository();
        private readonly IUsersRepository _usersRepository = new UsersRepository();
        private readonly IAccountsRepository _accountsRepository = new AccountsRepository();
        private readonly IRoomRepository _roomRepository = new RoomRepository();
        private readonly ISlotsRepository _slotsRepository = new SlotsRepository();

        public async Task<ServiceResponse<IEnumerable<CyberViewModelOut>>> AdminListCyber(string usernameCrr)
        {
            var output = new ServiceResponse<IEnumerable<CyberViewModelOut>>();
            try
            {
                var accountCrr = await _accountsRepository.GetAccountByUsername(usernameCrr);
                if (accountCrr != null)
                {
                    var currentUser = await _usersRepository.GetUserByAccountID(accountCrr.Id);
                    if ((await _adminsRepository.IsSuperAdmin(currentUser.Id)))
                    {
                        var outt = await _cybersRepository.GetAll();
                        if (outt != null)
                        {
                            var lstCyberOut = new List<CyberViewModelOut>();
                            foreach (var x in outt)
                            {
                                var lstRoomInCyber = await _roomRepository.GetListRoomByCyberId(x.Id);
                                int countSlotReadyInCyber = 0;
                                foreach (var s in lstRoomInCyber)
                                {
                                    countSlotReadyInCyber += await _slotsRepository.CountSlotReadyByRoomId(s.Id);
                                }

                                var cyberOut = new CyberViewModelOut
                                {
                                    CyberId = x.Id,
                                    Address = x.Address,
                                    BossCyberID = x.BossCyberID,
                                    BossCyberName = x.BossCyberName,
                                    BusinessLicense = x.BusinessLicense,
                                    CreatedDate = x.CreatedDate.ToString("HH:mm:ss dd/MM/yyyy"),
                                    CyberDescription = x.CyberDescription,
                                    CyberName = x.CyberName,
                                    image = x.image,
                                    lat = x.lat,
                                    lng = x.lng,
                                    PhoneNumber = x.PhoneNumber,
                                    RatingPoint = Convert.ToDouble(String.Format("{0:0.0}", x.RatingPoint / 2)),
                                    status = x.status,
                                    SlotReady = countSlotReadyInCyber,
                                    CoverImage = x.CoverImage
                                };
                                lstCyberOut.Add(cyberOut);
                            }
                            output.Data = lstCyberOut;
                            output.Success = true;
                            output.Message = "Get OK";
                            return output;
                        }
                        output.Message = "dont have any Cyber";
                        return output;
                    }
                    output.Message = "You NOT Permission for this Function";
                    return output;
                }
                output.Message = "You NOT Login";
                return output;
            }
            catch (Exception e)
            {
                output.Message = e.Message;
            }
            return output;
        }

        public async Task<ServiceResponse<CyberViewModelOut>> LockCyber(string usernameCrr, int cyberId)
        {
            var output = new ServiceResponse<CyberViewModelOut>();
            var adminsRepo = new AdminsRepository();
            try
            {
                var accountCrr = await _accountsRepository.GetAccountByUsername(usernameCrr);
                if (accountCrr != null)
                {
                    var currentUser = await _usersRepository.GetUserByAccountID(accountCrr.Id);
                    if ((await _adminsRepository.IsSuperAdmin(currentUser.Id)))
                    {
                        var cyber = await _cybersRepository.GetCyberById(cyberId);
                        if (cyber != null)
                        {
                            cyber.status = Convert.ToInt32(CybersEnum.CybersStatus.Deactivate);
                            var rs = await _cybersRepository.Update(cyber, cyber.Id);
                            if (rs != -1)
                            {
                                var lstCyberOut = new List<CyberViewModelOut>();

                                var lstRoomInCyber = await _roomRepository.GetListRoomByCyberId(cyber.Id);
                                int countSlotReadyInCyber = 0;
                                foreach (var s in lstRoomInCyber)
                                {
                                    countSlotReadyInCyber += await _slotsRepository.CountSlotReadyByRoomId(s.Id);
                                }

                                var cyberOut = new CyberViewModelOut
                                {
                                    CyberId = cyber.Id,
                                    Address = cyber.Address,
                                    BossCyberID = cyber.BossCyberID,
                                    BossCyberName = cyber.BossCyberName,
                                    BusinessLicense = cyber.BusinessLicense,
                                    CreatedDate = cyber.CreatedDate.ToString("HH:mm:ss dd/MM/yyyy"),
                                    CyberDescription = cyber.CyberDescription,
                                    CyberName = cyber.CyberName,
                                    image = cyber.image,
                                    lat = cyber.lat,
                                    lng = cyber.lng,
                                    PhoneNumber = cyber.PhoneNumber,
                                    RatingPoint = Convert.ToDouble(String.Format("{0:0.0}", cyber.RatingPoint / 2)),
                                    status = cyber.status,
                                    SlotReady = countSlotReadyInCyber,
                                    CoverImage = cyber.CoverImage
                                };
                                lstCyberOut.Add(cyberOut);

                                output.Message = "Success";
                                output.Success = true;
                                output.Data = cyberOut;

                                string messageBody = "<p> Dear anh/chị " + cyberOut.BossCyberName + ", <p>"
                + "<p>Tài khoản của bạn đã bị khóa do nhận được quá nhiều lượt đánh giá thấp nên chúng tôi buộc lòng phải tạm dừng việc hoạt động của bạn ở gocyber.xyz để giữ cho cộng đồng văn minh, trong sạch.</p>"
                + "Mọi phản hồi, thắc mắc xin vui lòng liên hệ:"
                + "<p>0846846886 – Mr.Đăng – contact@gocyber.xyz</p>";
                                string mailTo = cyberOut.Email;
                                string title = "[ GoCyber ] Thông báo về việc tài khoản bị khóa";
                                await adminsRepo.SendMail(messageBody, mailTo, title);
                                return output;
                            }
                            output.Message = "FAIL";
                            return output;
                        }
                        output.Message = "Cyber NOT exist";
                        return output;
                    }
                    output.Message = "YOU NOT PERMISSION for this Function";
                    return output;
                }
                output.Message = "YOU NOT LOGIN";
                return output;
            }
            catch (Exception ex)
            {
                output.Message = "LockCyber Exception: " + ex.Message;
            }
            return output;

        }

        public async Task<ServiceResponse<CyberViewModelOut>> RegisterCyber(CyberViewModel cyberViewModel)
        {
            var output = new ServiceResponse<CyberViewModelOut>();
            try
            {
                var emailCheck = (await _usersRepository.FindBy(x => x.Email == cyberViewModel.Email)).FirstOrDefault();

                if (emailCheck == null)
                {
                    var account1 = new Account
                    {
                        Username = cyberViewModel.Email,
                        Password = cyberViewModel.Password
                    };
                    //hash password to md5
                    string pass = account1.Password;
                    MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
                    UTF8Encoding utf8 = new UTF8Encoding();
                    byte[] data = md5.ComputeHash(utf8.GetBytes(pass));
                    var passHasConvert = Convert.ToBase64String(data);
                    account1.Password = passHasConvert;

                    var newAccount = await _accountsRepository.Create(account1);
                    if (newAccount == null)
                    {
                        output.Message = "cannot create Cyber, account Fail";
                        return output;
                    }

                    var user1 = new Users
                    {
                        Fullname = cyberViewModel.BossCyberName,
                        Dob = cyberViewModel.Dob,
                        Email = cyberViewModel.Email,
                        RoleId = (int)UsersEnum.UserRole.CyberManager,
                        Status = (int)UsersEnum.UserStatus.Block,
                        Image = "Qmdkw8rDq9YjQtaKacXYZVWLLoUGvpDZM2gtRf11TaP9eS",
                        AccountID = newAccount.Id
                    };
                    var newUser = await _usersRepository.Create(user1);

                    if (newUser == null || newUser.Id == 0)
                    {
                        output.Message = "cannot create Cyber, User FAIL";
                        return output;
                    }

                    // create cyber
                    var cyber1 = new Cyber
                    {
                        CyberName = cyberViewModel.CyberName,
                        Address = cyberViewModel.Address,
                        PhoneNumber = cyberViewModel.PhoneNumber,
                        BossCyberName = cyberViewModel.BossCyberName,
                        BossCyberID = user1.Id,
                        lat = cyberViewModel.lat,
                        lng = cyberViewModel.lng,
                        image = "QmRJhGFcqJQmxVJbLsaJNR3eZbX8oQzgcbxCZaPuueZLJ4",
                        status = (int)CybersEnum.CybersStatus.Pending,
                        BusinessLicense = cyberViewModel.BusinessLicense,
                        CreatedDate = DateTime.Now,
                        CoverImage = "no image"
                    };

                    // create cyber
                    var newCyberAccount = await _cybersRepository.Create(cyber1);
                    var lstCyberOut = new List<CyberViewModelOut>();
                    var lstRoomInCyber = await _roomRepository.GetListRoomByCyberId(cyber1.Id);
                    int countSlotReadyInCyber = 0;
                    if (newCyberAccount == null)
                    {
                        output.Message = "Create Cyber FAIL";
                        return output;
                    }
                    foreach (var s in lstRoomInCyber)
                    {
                        countSlotReadyInCyber += await _slotsRepository.CountSlotReadyByRoomId(s.Id);
                    }

                    var cyberOut = new CyberViewModelOut
                    {
                        CyberId = newCyberAccount.Id,
                        Address = newCyberAccount.Address,
                        BossCyberName = newCyberAccount.BossCyberName,
                        BusinessLicense = newCyberAccount.BusinessLicense,
                        CreatedDate = newCyberAccount.CreatedDate.ToString("HH:mm:ss dd/MM/yyyy"),
                        CyberDescription = newCyberAccount.CyberDescription,
                        CyberName = newCyberAccount.CyberName,
                        image = newCyberAccount.image,
                        lat = newCyberAccount.lat,
                        lng = newCyberAccount.lng,
                        PhoneNumber = newCyberAccount.PhoneNumber,
                        RatingPoint = Convert.ToDouble(String.Format("{0:0.0}", newCyberAccount.RatingPoint / 2)),
                        status = newCyberAccount.status,
                        SlotReady = countSlotReadyInCyber,
                        Dob = (DateTime)user1.Dob,
                        Email = user1.Email,
                        AccountId = account1.Id,
                        Username = account1.Username,
                        BossCyberID = user1.Id
                    };
                    lstCyberOut.Add(cyberOut);

                    output.Message = "Successfull";
                    output.Success = true;
                    output.Data = cyberOut;

                    //var cyberFindbyId = (await _cybersRepository.FindBy(x => x.Id == cyberOut.CyberId)).FirstOrDefault();
                    //if (cyberFindbyId != null)
                    //{
                    //    cyberFindbyId.BossCyberID = account1.Id;
                    //    await _cybersRepository.Update(cyberFindbyId, cyberFindbyId.Id);
                    //}

                    MailMessage message = new MailMessage("wegocyber@gmail.com", cyberOut.Email, "Thông báo về yêu cầu tạo tài khoản cyber manager", "Xin chào");
                    message.Body = "<p>Đăng ký thông tin cyber thành công</p>" +
                        " <p>Cảm ơn bạn đã đăng ký trở thành đối tác của GoCyber. Chúng tôi sẽ liên hệ với bạn trong thời gian sớm nhất.</p>" +
                        "<p>Mọi thắc mắc xin liên hệ:</p>" +
                        "<p style='color: red;'> 0846846886 – Mr.Đăng – contact@gocyber.xyz </p>";
                    message.BodyEncoding = System.Text.Encoding.UTF8;
                    message.SubjectEncoding = System.Text.Encoding.UTF8;
                    message.IsBodyHtml = true;

                    message.ReplyToList.Add(new MailAddress("wegocyber@gmail.com"));
                    message.Sender = new MailAddress("wegocyber@gmail.com");

                    using var smtpClient = new SmtpClient("smtp.gmail.com");
                    smtpClient.Port = 587;
                    smtpClient.EnableSsl = true;
                    smtpClient.Credentials = new NetworkCredential("wegocyber@gmail.com", "gocyber123");

                    await smtpClient.SendMailAsync(message);

                    return output;
                }
                else if (emailCheck != null)
                {
                    output.Message = "Address or Email has exist";
                    return output;
                }
                else
                {
                    output.Message = "Register failed!";
                    return output;
                }
            }
            catch (Exception ex)
            {
                output.Message = "Error: " + ex.Message;
            }
            return output;
        }

        public async Task<ServiceResponse<string>> RejectCyber(string usernameCrr, int cyberId)
        {
            var output = new ServiceResponse<string>();
            try
            {
                var accountCrr = await _accountsRepository.GetAccountByUsername(usernameCrr);
                if (accountCrr == null)
                {
                    output.Message = "You NOT LOGIN";
                    return output;
                }

                var currentUser = await _usersRepository.GetUserByAccountID(accountCrr.Id);
                if (!(await _adminsRepository.IsSuperAdmin(currentUser.Id)))
                {
                    output.Message = "You NOT Permisstion for this Function";
                    return output;
                }

                var cyber = await _cybersRepository.GetCyberById(cyberId);
                if (cyber == null)
                {
                    output.Message = "Cyber NOT Exist";
                    return output;
                }
                if (cyber.status == Convert.ToInt32(CybersEnum.CybersStatus.Pending))
                {
                    if (await _adminsRepository.RejectRegisterCyber(cyber.Id))
                    {
                        output.Message = "Reject RegisterCyber sucessfull";
                        output.Data = null;
                        output.Success = true;
                        return output;
                    }
                }
                output.Message = "Reject RegisterCyber FAIL";
                return output;
            }
            catch (Exception e)
            {
                output.Message = "Reject RegisterCyber Exception: " + e.Message;
            }
            return output;
        }

        public async Task<ServiceResponse<CyberViewModelOut>> UnLockCyber(string usernameCrr, int cyberId)
        {
            var output = new ServiceResponse<CyberViewModelOut>();
            try
            {
                var accountCrr = await _accountsRepository.GetAccountByUsername(usernameCrr);
                if (accountCrr != null)
                {
                    var currentUser = await _usersRepository.GetUserByAccountID(accountCrr.Id);
                    if ((await _adminsRepository.IsSuperAdmin(currentUser.Id)))
                    {
                        var cyber = await _cybersRepository.GetCyberById(cyberId);
                        if (cyber != null)
                        {
                            cyber.status = Convert.ToInt32(CybersEnum.CybersStatus.Active);
                            var rs = await _cybersRepository.Update(cyber, cyber.Id);
                            if (rs != -1)
                            {
                                var lstCyberOut = new List<CyberViewModelOut>();

                                var lstRoomInCyber = await _roomRepository.GetListRoomByCyberId(cyber.Id);
                                int countSlotReadyInCyber = 0;
                                foreach (var s in lstRoomInCyber)
                                {
                                    countSlotReadyInCyber += await _slotsRepository.CountSlotReadyByRoomId(s.Id);
                                }

                                var cyberOut = new CyberViewModelOut
                                {
                                    CyberId = cyber.Id,
                                    Address = cyber.Address,
                                    BossCyberID = cyber.BossCyberID,
                                    BossCyberName = cyber.BossCyberName,
                                    BusinessLicense = cyber.BusinessLicense,
                                    CreatedDate = cyber.CreatedDate.ToString("HH:mm:ss dd/MM/yyyy"),
                                    CyberDescription = cyber.CyberDescription,
                                    CyberName = cyber.CyberName,
                                    image = cyber.image,
                                    lat = cyber.lat,
                                    lng = cyber.lng,
                                    PhoneNumber = cyber.PhoneNumber,
                                    RatingPoint = Convert.ToDouble(String.Format("{0:0.0}", cyber.RatingPoint / 2)),
                                    status = cyber.status,
                                    SlotReady = countSlotReadyInCyber,
                                    CoverImage = cyber.CoverImage
                                };
                                lstCyberOut.Add(cyberOut);

                                output.Message = "Success";
                                output.Success = true;
                                output.Data = cyberOut;
                                return output;
                            }
                            output.Message = "FAIL";
                            return output;
                        }
                        output.Message = "Cyber NOT exist";
                        return output;
                    }
                    output.Message = "YOU NOT PERMISSION for this Function";
                    return output;
                }
                output.Message = "YOU NOT LOGIN";
                return output;
            }
            catch (Exception ex)
            {
                output.Message = "Error: " + ex.Message;
            }
            return output;
        }

        public async Task<ServiceResponse<CyberViewModelOut>> VerifyCyber(string usernameCrr, int cyberId)
        {
            var output = new ServiceResponse<CyberViewModelOut>();
            try
            {
                var roleUser = (await _usersRepository.FindBy(x => x.RoleId == 1)).FirstOrDefault();
                var accountCrr = await _accountsRepository.GetAccountByUsername(usernameCrr);
                if (accountCrr != null)
                {
                    var currentUser = await _usersRepository.GetUserByAccountID(accountCrr.Id);
                    if ((await _adminsRepository.IsSuperAdmin(currentUser.Id)))
                    {
                        var findIdCyber = await _cybersRepository.GetCyberById(cyberId);
                        if (findIdCyber != null)
                        {
                            var bossCyber = await _usersRepository.GetUserByUserID(findIdCyber.BossCyberID);
                            if (bossCyber == null)
                            {
                                output.Message = "Not Found boss Cyber";
                                return output;
                            }
                            bossCyber.Status = Convert.ToInt32(UsersEnum.UserStatus.Active);
                            await _usersRepository.Update(bossCyber, bossCyber.Id);

                            findIdCyber.status = Convert.ToInt32(CybersEnum.CybersStatus.Active);
                            var rs = await _cybersRepository.Update(findIdCyber, findIdCyber.Id);
                            if (rs != -1)
                            {
                                var lstCyberOut = new List<CyberViewModelOut>();

                                var lstRoomInCyber = await _roomRepository.GetListRoomByCyberId(findIdCyber.Id);
                                int countSlotReadyInCyber = 0;
                                foreach (var s in lstRoomInCyber)
                                {
                                    countSlotReadyInCyber += await _slotsRepository.CountSlotReadyByRoomId(s.Id);
                                }

                                var cyberOut = new CyberViewModelOut
                                {
                                    CyberId = findIdCyber.Id,
                                    Address = findIdCyber.Address,
                                    BossCyberID = findIdCyber.BossCyberID,
                                    BossCyberName = findIdCyber.BossCyberName,
                                    BusinessLicense = findIdCyber.BusinessLicense,
                                    CreatedDate = findIdCyber.CreatedDate.ToString("HH:mm:ss dd/MM/yyyy"),
                                    CyberDescription = findIdCyber.CyberDescription,
                                    CyberName = findIdCyber.CyberName,
                                    image = findIdCyber.image,
                                    lat = findIdCyber.lat,
                                    lng = findIdCyber.lng,
                                    PhoneNumber = findIdCyber.PhoneNumber,
                                    RatingPoint = Convert.ToDouble(String.Format("{0:0.0}", findIdCyber.RatingPoint / 2)),
                                    status = findIdCyber.status,
                                    SlotReady = countSlotReadyInCyber,
                                    Email = bossCyber.Email,
                                    CoverImage = findIdCyber.CoverImage
                                };
                                lstCyberOut.Add(cyberOut);

                                output.Message = "Success";
                                output.Success = true;
                                output.Data = cyberOut;

                                //gửi mail báo tài khoản cyber đã được duyệt

                                MailMessage message = new MailMessage("wegocyber@gmail.com", cyberOut.Email, "Thông báo về yêu cầu tạo tài khoản cyber manager", "Xin chào");
                                message.Body = "<p> Chào mừng " + cyberOut.BossCyberName + "</p>" +
                                    "<p>Cảm ơn bạn đã đăng ký sử dụng dịch vụ của GoCyber. Tài khoản của bạn đã được kích hoạt thành công.</p>" +
                                    "<p>Thông tin tài khoản: </p>" +
                                    "<p>&#183; Tài khoản: </p>" + cyberOut.Email +
                                    "<p>Bạn có thể đăng nhập để cấu hình quán game: <a href='https://www.gocyber.xyz/admin-cyber/'>Visit</a></p><br>" +
                                    "<p>Mọi thắc mắc xin liên hệ:</p>" +
                                    "<p style='color: red;'> 0846846886 – Mr.Đăng – contact@gocyber.xyz </p>";
                                message.BodyEncoding = System.Text.Encoding.UTF8;
                                message.SubjectEncoding = System.Text.Encoding.UTF8;
                                message.IsBodyHtml = true;

                                message.ReplyToList.Add(new MailAddress("wegocyber@gmail.com"));
                                message.Sender = new MailAddress("wegocyber@gmail.com");

                                using var smtpClient = new SmtpClient("smtp.gmail.com");
                                smtpClient.Port = 587;
                                smtpClient.EnableSsl = true;
                                smtpClient.Credentials = new NetworkCredential("wegocyber@gmail.com", "gocyber123");

                                await smtpClient.SendMailAsync(message);

                                return output;
                            }
                            output.Message = "Update Fail";
                            return output;
                        }
                        output.Message = "NOT  FOUND Cyber";
                        return output;
                    }
                    output.Message = "YOU NOT PERMISSION for this Function";
                    return output;
                }
                output.Message = "YOU NOT LOGIN";
                return output;
            }
            catch (Exception ex)
            {
                output.Message = "Error: " + ex.Message;
            }
            return output;
        }

        public async Task<ServiceResponse<CyberViewModelOut>> ViewDetailCyber(int cyberId)
        {
            var output = new ServiceResponse<CyberViewModelOut>();
            try
            {
                var detailCyber = await _cybersRepository.GetCyberById(cyberId);
                if (detailCyber != null)
                {
                    var lstCyberOut = new List<CyberViewModelOut>();

                    var lstRoomInCyber = await _roomRepository.GetListRoomByCyberId(cyberId);
                    int countSlotReadyInCyber = 0;
                    foreach (var s in lstRoomInCyber)
                    {
                        countSlotReadyInCyber += await _slotsRepository.CountSlotReadyByRoomId(s.Id);
                    }

                    var cyberOut = new CyberViewModelOut
                    {
                        CyberId = detailCyber.Id,
                        Address = detailCyber.Address,
                        BossCyberID = detailCyber.BossCyberID,
                        BossCyberName = detailCyber.BossCyberName,
                        BusinessLicense = detailCyber.BusinessLicense,
                        CreatedDate = detailCyber.CreatedDate.ToString("HH:mm:ss dd/MM/yyyy"),
                        CyberDescription = detailCyber.CyberDescription,
                        CyberName = detailCyber.CyberName,
                        image = detailCyber.image,
                        lat = detailCyber.lat,
                        lng = detailCyber.lng,
                        PhoneNumber = detailCyber.PhoneNumber,
                        RatingPoint = Convert.ToDouble(String.Format("{0:0.0}", detailCyber.RatingPoint / 2)),
                        status = detailCyber.status,
                        SlotReady = countSlotReadyInCyber,
                        CoverImage = detailCyber.CoverImage
                    };

                    output.Data = cyberOut;
                    output.Message = "Success";
                    output.Success = true;
                    return output;
                }
                output.Message = "can not find";
                return output;
            }
            catch (Exception ex)
            {
                output.Message = "ViewDetailCyber Exception: " + ex.Message;
            }
            return output;
        }

        public async Task<PagingOutput<IEnumerable<CyberViewModelOut>>> ViewListCyberByFilter(string usernameCrr, int pageIndex, int pageSize, string statusCyberId, string cyberName)
        {
            var output = new PagingOutput<IEnumerable<CyberViewModelOut>>();
            try
            {
                var accountCrr = await _accountsRepository.GetAccountByUsername(usernameCrr);
                if (accountCrr != null)
                {
                    var currentUser = await _usersRepository.GetUserByAccountID(accountCrr.Id);
                    if ((await _adminsRepository.IsSuperAdmin(currentUser.Id)))
                    {
                        var result = await _cybersRepository.GetAllCyberByFilter(pageIndex, pageSize, statusCyberId, cyberName);
                        if (result != null)
                        {
                            var lstCyberOut = new List<CyberViewModelOut>();

                            foreach (var x in result.Data)
                            {
                                var lstRoomInCyber = await _roomRepository.GetListRoomByCyberId(x.Id);
                                int countSlotReadyInCyber = 0;
                                foreach (var s in lstRoomInCyber)
                                {
                                    countSlotReadyInCyber += await _slotsRepository.CountSlotReadyByRoomId(s.Id);
                                }
                                string email = "NO EMAIL";
                                string username = "NO USERNAME";
                                int accountId = 0;

                                var bossCyberInfor = await _usersRepository.GetUserByUserID(x.BossCyberID);
                                if (bossCyberInfor != null)
                                {
                                    email = bossCyberInfor.Email;
                                    var accountCyberInfor = await _accountsRepository.GetAccountByID(bossCyberInfor.AccountID);
                                    accountId = accountCyberInfor.Id;
                                    username = accountCyberInfor.Username;
                                }

                                var cyberOut = new CyberViewModelOut
                                {
                                    CyberId = x.Id,
                                    Address = x.Address,
                                    BossCyberID = x.BossCyberID,
                                    BossCyberName = x.BossCyberName,
                                    BusinessLicense = x.BusinessLicense,
                                    CreatedDate = x.CreatedDate.ToString("HH:mm:ss dd/MM/yyyy"),
                                    CyberDescription = x.CyberDescription,
                                    CyberName = x.CyberName,
                                    image = x.image,
                                    lat = x.lat,
                                    lng = x.lng,
                                    PhoneNumber = x.PhoneNumber,
                                    RatingPoint = Convert.ToDouble(String.Format("{0:0.0}", x.RatingPoint / 2)),
                                    status = x.status,
                                    Email = email,
                                    AccountId = accountId,
                                    Username = username,
                                    SlotReady = countSlotReadyInCyber,
                                    CoverImage = x.CoverImage
                                };
                                lstCyberOut.Add(cyberOut);
                            }

                            output.Data = lstCyberOut;
                            output.Index = result.Index;
                            output.PageSize = result.PageSize;
                            output.TotalItem = result.TotalItem;
                            output.TotalPage = result.TotalPage;
                            output.Message = "Get List Cyber by Status OKK";
                            return output;
                        }
                        output.Message = "NO CYBER IS PENDING";
                        return output;
                    }
                    output.Message = "YOU NOT PERMISSION for this Function";
                    return output;
                }
                output.Message = "YOU NOT LOGIN";
                return output;
            }
            catch (Exception ex)
            {
                output.Message = "Error: " + ex.Message;
            }
            return output;
        }

        public async Task<PagingOutput<IEnumerable<CyberViewModelOut>>> ViewListCyberRegisterPending(string usernameCrr, int pageIndex, int pageSize)
        {
            var output = new PagingOutput<IEnumerable<CyberViewModelOut>>();
            try
            {
                var accountCrr = await _accountsRepository.GetAccountByUsername(usernameCrr);
                if (accountCrr != null)
                {
                    var currentUser = await _usersRepository.GetUserByAccountID(accountCrr.Id);
                    if ((await _adminsRepository.IsSuperAdmin(currentUser.Id)))
                    {
                        var result = await _adminsRepository.GetListCyberRegisterPending(pageIndex, pageSize);
                        if (result != null)
                        {
                            var lstCyberOut = new List<CyberViewModelOut>();

                            foreach (var x in result.Data)
                            {
                                var lstRoomInCyber = await _roomRepository.GetListRoomByCyberId(x.Id);
                                int countSlotReadyInCyber = 0;
                                foreach (var s in lstRoomInCyber)
                                {
                                    countSlotReadyInCyber += await _slotsRepository.CountSlotReadyByRoomId(s.Id);
                                }

                                var cyberOut = new CyberViewModelOut
                                {
                                    CyberId = x.Id,
                                    Address = x.Address,
                                    BossCyberID = x.BossCyberID,
                                    BossCyberName = x.BossCyberName,
                                    BusinessLicense = x.BusinessLicense,
                                    CreatedDate = x.CreatedDate.ToString("HH:mm:ss dd/MM/yyyy"),
                                    CyberDescription = x.CyberDescription,
                                    CyberName = x.CyberName,
                                    image = x.image,
                                    lat = x.lat,
                                    lng = x.lng,
                                    PhoneNumber = x.PhoneNumber,
                                    RatingPoint = Convert.ToDouble(String.Format("{0:0.0}", x.RatingPoint / 2)),
                                    status = x.status,
                                    SlotReady = countSlotReadyInCyber,
                                    CoverImage = x.CoverImage
                                };
                                lstCyberOut.Add(cyberOut);
                            }

                            output.Data = lstCyberOut;
                            output.Index = result.Index;
                            output.PageSize = result.PageSize;
                            output.TotalItem = result.TotalItem;
                            output.TotalPage = result.TotalPage;
                            output.Message = "Get List Cyber by Status OKK";
                            return output;
                        }
                        output.Message = "NO CYBER IS PENDING";
                        return output;
                    }
                    output.Message = "YOU NOT PERMISSION for this Function";
                    return output;
                }
                output.Message = "YOU NOT LOGIN";
                return output;
            }
            catch (Exception ex)
            {
                output.Message = "Error: " + ex.Message;
            }
            return output;
        }
    }
}
