﻿using CyberBook_API.Dal.Repositories;
using CyberBook_API.Enum;
using CyberBook_API.Interfaces;
using CyberBook_API.Models;
using CyberBook_API.Services.Interfaces;
using CyberBook_API.ViewModel.PagingView;
using CyberBook_API.ViewModel.RateCybersViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CyberBook_API.Services
{
    public class UserRateCyberService : IUserRateCyberService
    {
        private readonly IUserRateCyberRepository _userRateCyberRepository = new UserRateCyberRepository();
        private readonly ICybersRepository _cybersRepository = new CybersRepository();
        private readonly IUsersRepository _usersRepository = new UsersRepository();
        private readonly IAccountsRepository _accountsRepository = new AccountsRepository();
        private readonly ICyberAccountRepository _cyberAccountRepository = new CyberAccountRepository();
        private readonly IOrderRepository _orderRepository = new OrderRepository();


        public async Task<ServiceResponse<RateCyberOutViewModel>> AddUserRateCyber(string usernameCrr, RatingCyber rateCyber)
        {
            var output = new ServiceResponse<RateCyberOutViewModel>();
            try
            {
                var accountCrr = await _accountsRepository.GetAccountByUsername(usernameCrr);
                if (accountCrr != null)
                {
                    var currentUser = await _usersRepository.GetUserByAccountID(accountCrr.Id);
                    var cyber = await _cybersRepository.GetCyberById(rateCyber.CyberId);
                    if (cyber != null)
                    {
                        var order = await _orderRepository.GetOrderById(rateCyber.OrderId);
                        if (order != null)
                        {
                            if (order.StatusOrder != Convert.ToInt32(OrdersEnum.StatusOrder.Cancel)
                                && order.StatusOrder != Convert.ToInt32(OrdersEnum.StatusOrder.Reject)
                                && order.StatusOrder != Convert.ToInt32(OrdersEnum.StatusOrder.Pending))
                            {
                                var rateCyberCheck = await _userRateCyberRepository.GetRateCyberExist(currentUser.Id, cyber.Id, order.Id);
                                if (rateCyberCheck == null)
                                {
                                    var rateTemp = new RatingCyber
                                    {
                                        UserId = currentUser.Id,
                                        RatePoint = rateCyber.RatePoint,
                                        CommentContent = rateCyber.CommentContent,
                                        CyberId = rateCyber.CyberId,
                                        CreatedDate = DateTime.Now,
                                        UpdatedDate = DateTime.Now,
                                        Edited = false,
                                        OrderId = rateCyber.OrderId
                                    };
                                    var newRate = await _userRateCyberRepository.Create(rateTemp);
                                    if (newRate != null)
                                    {
                                        var lstRateCyber = await _userRateCyberRepository.GetListRateCyberByCyberId(cyber.Id);
                                        double sum = 0;
                                        int count = 0;
                                        foreach (var x in lstRateCyber)
                                        {
                                            if (x.RatePoint != 0)
                                            {
                                                sum += x.RatePoint;
                                            }
                                            count++;
                                        }
                                        cyber.RatingPoint = sum / count;
                                        await _cybersRepository.Update(cyber, cyber.Id);

                                        var rateOut = new RateCyberOutViewModel
                                        {
                                            RateId = newRate.Id,
                                            UserId = newRate.UserId,
                                            RatePoint =  Math.Round(newRate.RatePoint, 2),
                                            CommentContent = newRate.CommentContent,
                                            CyberId = newRate.CyberId,
                                            CreatedDate = Convert.ToDateTime(newRate.CreatedDate).ToString("HH:mm:ss dd/MM/yyyy"),
                                            UpdatedDate = Convert.ToDateTime(newRate.UpdatedDate).ToString("HH:mm:ss dd/MM/yyyy"),
                                            Edited = newRate.Edited,
                                            OrderId = newRate.OrderId,
                                        };

                                        output.Message = "Successfull";
                                        output.Data = rateOut;
                                        output.Success = true;
                                        return output;
                                    }
                                    output.Message = "CAN NOT Create RateCyber";
                                    return output;
                                }
                                output.Message = "Rate record has exist";
                                return output;
                            }
                            output.Message = "if you CANCEL you CANNOT rate cyber";
                            return output;
                        }
                        output.Message = "Order NOT exist";
                        return output;
                    }
                    output.Message = "Cyber NOT exist";
                    return output;
                }
                output.Message = "You NOT LOGIN";
                return output;
            }
            catch (Exception e)
            {
                output.Message = "AddUserRateCyber Exception: " + e.Message;
            }
            return output;
        }

        public async Task<PagingOutput<IEnumerable<RateCyberOutViewModel>>> GetAllRateByCyberId(int pageIndex, int pageSize, int cyberId)
        {
            var output = new PagingOutput<IEnumerable<RateCyberOutViewModel>>();
            try
            {
                var rateRecord = await _userRateCyberRepository.GetAllRateCyberByCyberId(cyberId, pageIndex, pageSize);
                if (rateRecord != null)
                {
                    var lstRate = new List<RateCyberOutViewModel>();

                    foreach(var x in rateRecord.Data.ToList())
                    {
                        var rateOut = new RateCyberOutViewModel
                        {
                            RateId = x.Id,
                            UserId = x.UserId,
                            RatePoint = Math.Round(x.RatePoint, 2),
                            CommentContent = x.CommentContent,
                            CyberId = x.CyberId,
                            CreatedDate = Convert.ToDateTime(x.CreatedDate).ToString("HH:mm:ss dd/MM/yyyy"),
                            UpdatedDate = Convert.ToDateTime(x.UpdatedDate).ToString("HH:mm:ss dd/MM/yyyy"),
                            Edited = x.Edited,
                            OrderId = x.OrderId,
                        };
                        lstRate.Add(rateOut);
                    }

                    output.Message = "get RateCyber Record Successfull";
                    output.Index = rateRecord.Index;
                    output.PageSize = rateRecord.PageSize;
                    output.TotalItem = rateRecord.TotalItem;
                    output.TotalPage = rateRecord.TotalPage;
                    output.Data = lstRate;
                    return output;
                }
                output.Message = "No Record avaiable";
                return output;
            }
            catch (Exception e)
            {
                output.Message = "GetAllRateByCyberId Exeption" + e.Message;
            }
            return output;
        }

        public async Task<ServiceResponse<RateCyberOutViewModel>> GetRateCyberById(int id)
        {
            var output = new ServiceResponse<RateCyberOutViewModel>();
            try
            {
                var rateRecord = await _userRateCyberRepository.GetRateCyberById(id);
                if (rateRecord != null)
                {
                    var rateOut = new RateCyberOutViewModel
                    {
                        RateId = rateRecord.Id,
                        UserId = rateRecord.UserId,
                        RatePoint = Math.Round(rateRecord.RatePoint, 2),
                        CommentContent = rateRecord.CommentContent,
                        CyberId = rateRecord.CyberId,
                        CreatedDate = Convert.ToDateTime(rateRecord.CreatedDate).ToString("HH:mm:ss dd/MM/yyyy"),
                        UpdatedDate = Convert.ToDateTime(rateRecord.UpdatedDate).ToString("HH:mm:ss dd/MM/yyyy"),
                        Edited = rateRecord.Edited,
                        OrderId = rateRecord.OrderId,
                    };

                    output.Message = "get RateCyber Record Successfull";
                    output.Data = rateOut;
                    output.Success = true;
                    return output;
                }
                output.Message = "No Record avaiable";
                return output;
            }
            catch (Exception e)
            {
                output.Message = "GetRateCyberById Exeption" + e.Message;
            }
            return output;
        }

        public async Task<ServiceResponse<string>> RemoveRateById(string usernameCrr, int id)
        {
            var output = new ServiceResponse<string>();
            try
            {
                var accountCrr = await _accountsRepository.GetAccountByUsername(usernameCrr);
                if (accountCrr != null)
                {
                    var currentUser = await _usersRepository.GetUserByAccountID(accountCrr.Id);
                    var rateRecord = (await _userRateCyberRepository.FindBy(x => x.Id == id && x.UserId == currentUser.Id)).FirstOrDefault();
                    if (rateRecord != null)
                    {
                        var cyber = await _cybersRepository.GetCyberById(rateRecord.CyberId);
                        if (cyber != null)
                        {
                            if ((await _userRateCyberRepository.Delete(rateRecord)) != -1)
                            {
                                var lstRateCyber = await _userRateCyberRepository.GetListRateCyberByCyberId(cyber.Id);
                                double sum = 0;
                                int count = 0;
                                foreach (var x in lstRateCyber)
                                {
                                    if (x.RatePoint != 0)
                                    {
                                        sum += x.RatePoint;
                                        count++;
                                    }
                                }
                                cyber.RatingPoint = sum / count;
                                await _cybersRepository.Update(cyber, cyber.Id);

                                output.Message = "Delete Rate Record Successfull";
                                output.Data = "Xóa thành công bản ghi đánh giá Cyber " + cyber.CyberName;
                                output.Success = true;
                                return output;
                            }
                            output.Message = "CAN NOT remove this Rate Cyber";
                            return output;
                        }
                        output.Message = "Cyber NOT exist";
                        return output;
                    }
                    output.Message = "No Record avaiable";
                    return output;
                }
                output.Message = "You NOT LOGIN";
                return output;
            }
            catch (Exception e)
            {
                output.Message = "RemoveRateById Exception: " + e.Message;
            }
            return output;
        }

        public async Task<ServiceResponse<RateCyberOutViewModel>> UpdateRateCyber(string usernameCrr, RatingCyber rateCyber)
        {
            var output = new ServiceResponse<RateCyberOutViewModel>();
            try
            {
                var accountCrr = await _accountsRepository.GetAccountByUsername(usernameCrr);
                if (accountCrr != null)
                {
                    var currentUser = await _usersRepository.GetUserByAccountID(accountCrr.Id);
                    var cyber = await _cybersRepository.GetCyberById(rateCyber.CyberId);
                    if (cyber != null)
                    {
                        var cyberAccount = await _cyberAccountRepository.GetCyberAccountDetail(cyber.Id, currentUser.Id);
                        if (cyberAccount != null)
                        {
                            var order = await _orderRepository.GetOrderById(rateCyber.OrderId);
                            if (order != null)
                            {
                                var rateCyberCheck = await _userRateCyberRepository.GetRateCyberExist(currentUser.Id, cyber.Id, order.Id);
                                if (rateCyberCheck != null)
                                {
                                    if ((await _userRateCyberRepository.Update(rateCyberCheck, rateCyberCheck.Id)) != -1)
                                    {
                                        var lstRateCyber = await _userRateCyberRepository.GetListRateCyberByCyberId(cyber.Id);
                                        double sum = 0;
                                        int count = 0;
                                        foreach (var x in lstRateCyber)
                                        {
                                            if (x.RatePoint != 0)
                                            {
                                                sum += x.RatePoint;
                                            }
                                            count++;
                                        }
                                        cyber.RatingPoint = sum / count;
                                        await _cybersRepository.Update(cyber, cyber.Id);

                                        var rateOut = new RateCyberOutViewModel
                                        {
                                            RateId = rateCyberCheck.Id,
                                            UserId = rateCyberCheck.UserId,
                                            RatePoint = Math.Round(rateCyberCheck.RatePoint, 2),
                                            CommentContent = rateCyberCheck.CommentContent,
                                            CyberId = rateCyberCheck.CyberId,
                                            CreatedDate = Convert.ToDateTime(rateCyberCheck.CreatedDate).ToString("HH:mm:ss dd/MM/yyyy"),
                                            UpdatedDate = Convert.ToDateTime(rateCyberCheck.UpdatedDate).ToString("HH:mm:ss dd/MM/yyyy"),
                                            Edited = rateCyberCheck.Edited,
                                            OrderId = rateCyberCheck.OrderId,
                                        };

                                        output.Message = "Successfull";
                                        output.Data = rateOut;
                                        output.Success = true;
                                        return output;
                                    }
                                    output.Message = "Update Rate Cyber FAIL";
                                    return output;
                                }
                                output.Message = "Rate NOT exist";
                                return output;
                            }
                            output.Message = "Order NOT exist";
                            return output;
                        }
                        output.Message = "You NOT permission this Function";
                        return output;
                    }
                    output.Message = "Cyber NOT exist";
                    return output;
                }
                output.Message = "You NOT LOGIN";
                return output;
            }
            catch (Exception e)
            {
                output.Message = "UpdateRateCyber Exception" + e.Message;
            }
            return output;
        }
    }
}
