﻿using CyberBook_API.Dal.Repositories;
using CyberBook_API.Interfaces;
using CyberBook_API.Models;
using CyberBook_API.Services.Interfaces;
using CyberBook_API.ViewModel.CyberViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CyberBook_API.Services
{
    public class CybersService : ICybersService
    {

        private readonly ICybersRepository _cybersRepository = new CybersRepository();
        private readonly ICyberRateUserRepository _cyberRateUserRepository = new CyberRateUserRepository();
        private readonly IUsersRepository _usersRepository = new UsersRepository();
        private readonly IAccountsRepository _accountsRepository = new AccountsRepository();

        public async Task<ServiceResponse<Cyber>> EditCyberInfor(string usernameCrr, CyberEditViewModel cyberIn)
        {
            var output = new ServiceResponse<Cyber>();
            try
            {
                //check user hiện tại
                var accountCrr = await _accountsRepository.GetAccountByUsername(usernameCrr);
                if (accountCrr != null)
                {
                    var currentUser = await _usersRepository.GetUserByAccountID(accountCrr.Id);
                    var cyber = await _cybersRepository.GetCyberById(cyberIn.id);
                    if (cyber != null)
                    {
                        //check user hiện tại có quyền trong Cyber này không?
                        if ((await _cybersRepository.IsBossCyber(currentUser.Id, cyber.Id)))
                        {
                            cyber.CyberName = cyberIn.CyberName;
                            cyber.Address = cyberIn.Address;
                            cyber.PhoneNumber = cyberIn.PhoneNumber;
                            cyber.CyberDescription = cyberIn.CyberDescription;
                            cyber.lat = cyberIn.lat;
                            cyber.lng = cyberIn.lng;
                            cyber.image = cyberIn.image;
                            cyber.CoverImage = cyberIn.CoverImage;
                            var rs = await _cybersRepository.Update(cyber, cyber.Id);
                            if (rs != -1)
                            {
                                output.Data = cyber;
                                output.Message = "Update EditCyberInfor OK";
                                output.Success = true;
                                return output;
                            }
                            output.Message = "Update EditCyberInfor FAIL";
                            return output;
                        }
                        output.Message = "You NOT cyber Admin";
                        return output;
                    }
                    output.Message = "Cyber NOT Exist";
                    return output;
                }
                output.Message = "You NOT LOGIN";
                return output;
            }
            catch (Exception ex)
            {
                output.Message = "Exception " + ex.Message;
            }
            return output;
        }

        public async Task<ServiceResponse<Cyber>> GetCyberById(int cyberId)
        {
            var output = new ServiceResponse<Cyber>();
            try
            {
                var cybers = await _cybersRepository.GetCyberById(cyberId);
                if (cybers != null)
                {
                    output.Success = true;
                    output.Message = "Success";
                    output.Data = cybers;
                    return output;
                }
                output.Message = "No Cyber";
                return output;
            }
            catch (Exception e)
            {
                output.Message = "GetCyberById Exception: " + e.Message;
            }
            return output;
        }

        public async Task<ServiceResponse<Cyber>> GetMyCyber(string usernameCrr)
        {
            var output = new ServiceResponse<Cyber>();
            try
            {
                var accountCrr = await _accountsRepository.GetAccountByUsername(usernameCrr);
                if (accountCrr == null)
                {
                    output.Message = "You NOT Login";
                    return output;
                }
                var currentUser = await _usersRepository.GetUserByAccountID(accountCrr.Id);
                var cyber = await _cybersRepository.GetCyberByBossCyberId(currentUser.Id);
                if (cyber == null)
                {
                    output.Message = "Cyber not exist";
                    return output;
                }
                output.Data = cyber;
                output.Message = "GetMyCyber Successfull";
                output.Success = true;
                return output;
            }
            catch (Exception e)
            {
                output.Message = "GetMyCyber Exception: " + e.Message;
            }
            return output;
        }
    }
}
