﻿using CyberBook_API.Dal.Repositories;
using CyberBook_API.Interfaces;
using CyberBook_API.Models;
using CyberBook_API.Services.Interfaces;
using CyberBook_API.ViewModel.PagingView;
using CyberBook_API.ViewModel.UserViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CyberBook_API.Services
{
    public class UsersService : IUsersService
    {
        private readonly ISlotsRepository _slotsRepository = new SlotsRepository();
        private readonly ICybersRepository _cybersRepository = new CybersRepository();
        private readonly IAccountsRepository _accountsRepository = new AccountsRepository();
        private readonly IUsersRepository _usersRepository = new UsersRepository();
        private readonly IRoomRepository _roomRepository = new RoomRepository();
        private readonly IHardwareConfigRepository _hardwareConfigRepository = new HardwareConfigRepository();
        private readonly IStatusSlotRepository _statusSlotRepository = new StatusSlotRepository();
        private readonly ICyberAccountRepository _cyberAccountRepository = new CyberAccountRepository();

        public async Task<ServiceResponse<CyberAccount>> AddNewCyberAccount(string usernameCrr, CyberAccount cyberAccount)
        {
            var output = new ServiceResponse<CyberAccount>();
            try
            {
                var accountCrr = await _accountsRepository.GetAccountByUsername(usernameCrr);
                //check user hiện tại có quyền trong Cyber này không?
                if (accountCrr != null)
                {
                    var currentUser = await _usersRepository.GetUserByAccountID(accountCrr.Id);
                    var cyber = await _cybersRepository.GetCyberById(cyberAccount.CyberId);
                    if (cyber != null)
                    {
                        if (!cyberAccount.Username.Equals("") && !cyberAccount.Password.Equals(""))
                        {
                            var cyberAccCheck = await _cyberAccountRepository.CyberAccountExist(currentUser.Id, cyber.Id, cyberAccount.Username);

                            if (cyberAccCheck == null)
                            {
                                var cyberAccountTemp = new CyberAccount
                                {
                                    UserId = currentUser.Id,
                                    CyberId = cyber.Id,
                                    PhoneNumber = cyberAccount.PhoneNumber,
                                    CyberName = cyber.CyberName,
                                    Username = cyberAccount.Username,
                                    Password = cyberAccount.Password
                                };
                                var newCyberAccount = await _cyberAccountRepository.Create(cyberAccountTemp);
                                if (newCyberAccount != null)
                                {
                                    output.Data = newCyberAccount;
                                    output.Message = "Successfull";
                                    output.Success = true;
                                    return output;
                                }
                                output.Message = "CAN NOT Create New CyberAccount";
                                return output;
                            }
                            output.Message = "this Account was exist";
                            return output;
                        }
                        output.Message = "username or password NOT allow Empty";
                        return output;
                    }
                    output.Message = "Cyber NOT exist";
                    return output;
                }
                output.Message = "User NOT exist";
                return output;
            }
            catch (Exception e)
            {
                output.Message = "AddNewCyberAccount Exception: " + e.Message;
            }
            return output;
        }

        public async Task<ServiceResponse<CyberAccount>> EditCyberAccount(string usernameCrr, CyberAccount cyberAccount)
        {
            var output = new ServiceResponse<CyberAccount>();
            try
            {
                var accountCrr = await _accountsRepository.GetAccountByUsername(usernameCrr);
                //check user hiện tại có quyền trong Cyber này không?
                if (accountCrr != null)
                {
                    var currentUser = await _usersRepository.GetUserByAccountID(accountCrr.Id);
                    var cyber = await _cybersRepository.GetCyberById(cyberAccount.CyberId);
                    if (cyber != null)
                    {
                        if (!cyberAccount.Username.Equals("") && !cyberAccount.Password.Equals(""))
                        {
                            var cyberAccCheck = await _cyberAccountRepository.CyberAccountExist(currentUser.Id, cyber.Id, cyberAccount.Username);

                            if (cyberAccCheck != null)
                            {
                                cyberAccCheck.CyberId = cyberAccount.CyberId;
                                cyberAccCheck.CyberName = cyberAccount.CyberName;
                                cyberAccCheck.Username = cyberAccount.Username;
                                cyberAccCheck.Password = cyberAccount.Password;

                                var rs = await _cyberAccountRepository.Update(cyberAccCheck, cyberAccCheck.ID);
                                if (rs != -1)
                                {
                                    output.Data = cyberAccCheck;
                                    output.Message = "Successfull";
                                    output.Success = true;
                                    return output;
                                }
                                output.Message = "CAN NOT Edit  CyberAccount";
                                return output;
                            }
                            output.Message = "CyberAccount NOT exist";
                            return output;
                        }
                        output.Message = "username or password NOT allow Empty";
                        return output;
                    }
                    output.Message = "Cyber NOT exist";
                    return output;
                }
                output.Message = "User NOT exist";
                return output;
            }
            catch (Exception e)
            {
                output.Message = "EditCyberAccount Exception: " + e.Message;
            }
            return output;
        }

        public async Task<ServiceResponse<UsersOutViewModel>> EditUserInfor(string usernameCrr, UserEditViewModel userIn)
        {
            var output = new ServiceResponse<UsersOutViewModel>();
            try
            {
                var accountCrr = await _accountsRepository.GetAccountByUsername(usernameCrr);
                if (accountCrr != null)
                {
                    var currentUser = await _usersRepository.GetUserByAccountID(accountCrr.Id);
                    if (currentUser == null)
                    {
                        output.Message = "user not exist";
                        return output;
                    }

                    currentUser.Fullname = userIn.Fullname;
                    currentUser.Address = userIn.Address;
                    currentUser.Email = userIn.Email;
                    currentUser.PhoneNumber = userIn.PhoneNumber;
                    currentUser.Bio = userIn.Bio;
                    currentUser.Image = userIn.Image;
                    currentUser.Dob = userIn.Dob;
                    var rs = await _usersRepository.Update(currentUser, currentUser.Id);
                    if (rs != -1)
                    {
                        var userOut = new UsersOutViewModel
                        {
                            AccountID = currentUser.AccountID,
                            Address = currentUser.Address,
                            Bio = currentUser.Bio,
                            Email = currentUser.Email,
                            Fullname = currentUser.Fullname,
                            Id = currentUser.Id,
                            Image = currentUser.Image,
                            PhoneNumber = currentUser.PhoneNumber,
                            RatingPoint = currentUser.RatingPoint,
                            RoleId = currentUser.RoleId,
                            Status = currentUser.Status,
                            Dob = currentUser.Dob
                        };
                        //if (!string.IsNullOrWhiteSpace(Convert.ToString(currentUser.Dob)))
                        //{
                        //    userOut.Dob = Convert.ToDateTime(currentUser.Dob).ToString("dd/MM/yyyy");
                        //}

                        output.Message = "Update Infor OK";
                        output.Success = true;
                        output.Data = userOut;
                        return output;
                    }
                    output.Message = "EditUserInfor FAIL";
                    return output;
                }
                output.Message = "You NOT LOGIN";
                return output;
            }
            catch (Exception e)
            {
                output.Message = "EditUserInfor Exception: " + e.Message;
            }
            return output;
        }

        public async Task<ServiceResponse<CyberAccount>> GetDetailCyberAccount(string usernameCrr, int cyberAccountId)
        {
            var output = new ServiceResponse<CyberAccount>();
            try
            {
                var accountCrr = await _accountsRepository.GetAccountByUsername(usernameCrr);
                if (accountCrr != null)
                {
                    var currentUser = await _usersRepository.GetUserByAccountID(accountCrr.Id);
                    var cyberAccountCheck = await _cyberAccountRepository.GetCyberAccountDetail(cyberAccountId, currentUser.Id);
                    if (cyberAccountCheck != null)
                    {
                        output.Data = cyberAccountCheck;
                        output.Message = "Successfully";
                        output.Success = true;
                        return output;
                    }
                    output.Message = "CAN NOT find your CyberAccount or You dont have Permission for view this Account Cyber";
                    return output;
                }
                output.Message = "You NOT LOGIN";
                return output;
            }
            catch (Exception e)
            {
                output.Message = "RemoveCyberAccount Exception: " + e.Message;
            }
            return output;
        }

        public async Task<ServiceResponse<UsersOutViewModel>> GetMyInfor(string usernameCrr)
        {
            var output = new ServiceResponse<UsersOutViewModel>();
            try
            {
                var accountCrr = await _accountsRepository.GetAccountByUsername(usernameCrr);
                if (accountCrr != null)
                {
                    var currentUser = await _usersRepository.GetUserByAccountID(accountCrr.Id);
                    if (currentUser != null)
                    {
                        var userOut = new UsersOutViewModel
                        {
                           AccountID = currentUser.AccountID,
                           Address = currentUser.Address,
                           Bio = currentUser.Bio,
                           Email = currentUser.Email,
                           Fullname = currentUser.Fullname,
                           Id = currentUser.Id,
                           Image = currentUser.Image,
                           PhoneNumber = currentUser.PhoneNumber,
                           RatingPoint = currentUser.RatingPoint,
                           RoleId = currentUser.RoleId,
                           Status = currentUser.Status,
                           Dob = currentUser.Dob
                        };
                        //if (!string.IsNullOrWhiteSpace(Convert.ToString(currentUser.Dob)))
                        //{
                        //    userOut.Dob = Convert.ToDateTime(currentUser.Dob).ToString("dd/MM/yyyy");
                        //}

                        output.Message = "Get Infor OK";
                        output.Success = true;
                        output.Data = userOut;
                        return output;
                    }
                    output.Message = "User Not Exist";
                    return output;
                }
                output.Message = "You NOT LOGIN";
                return output;
            }
            catch (Exception e)
            {
                output.Message = "GetMyInfor Exception: " + e.Message;
            }
            return output;
        }

        public async Task<ServiceResponse<int>> RemoveCyberAccount(string usernameCrr, int cyberAccountId)
        {
            var output = new ServiceResponse<int>();
            try
            {
                var accountCrr = await _accountsRepository.GetAccountByUsername(usernameCrr);
                if (accountCrr != null)
                {
                    var currentUser = await _usersRepository.GetUserByAccountID(accountCrr.Id);
                    var cyberAccCheck = await _cyberAccountRepository.GetCyberAccountById(cyberAccountId);
                    if (cyberAccCheck != null)
                    {
                        if (cyberAccCheck.UserId != currentUser.Id)
                        {
                            output.Message = "You NOT Permission for this Function";
                        }
                        if ((await _cyberAccountRepository.Delete(cyberAccCheck)) != -1)
                        {
                            output.Data = 1;
                            output.Message = "Successfull";
                            output.Success = true;
                            return output;
                        }
                        output.Message = "CAN NOT Remove CyberAccount";
                        return output;
                    }
                    output.Message = "CyberAccount NOT exist";
                    return output;
                }
                output.Message = "You NOT LOGIN";
                return output;
            }
            catch (Exception e)
            {
                output.Message = "RemoveCyberAccount Exception: " + e.Message;
            }
            return output;
        }

        public async Task<PagingOutput<IEnumerable<CyberAccount>>> ViewMyListCyberAccount(string usernameCrr, int pageIndex, int pageSize)
        {
            var output = new PagingOutput<IEnumerable<CyberAccount>>();
            try
            {
                var accountCrr = await _accountsRepository.GetAccountByUsername(usernameCrr);
                if (accountCrr != null)
                {
                    var currentUser = await _usersRepository.GetUserByAccountID(accountCrr.Id);
                    var result = await _cyberAccountRepository.GetListCyberAccountByUserId(currentUser.Id, pageIndex, pageSize);
                    if (result != null)
                    {
                        output = result;
                        output.Message = "Get your List CyberAccount OK";
                        return output;
                    }
                    output.Message = "dont have any cyber account";
                    return output;
                }
                output.Message = "You NOT Login";
                return output;
            }
            catch (Exception e)
            {
                output.Message = "ViewListCyberAccount Exception: " + e.Message;
            }
            return output;
        }
    }
}
