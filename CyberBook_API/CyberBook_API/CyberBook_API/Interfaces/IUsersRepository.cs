﻿using CyberBook_API.Dal;
using CyberBook_API.Models;
using CyberBook_API.ViewModel.PagingView;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CyberBook_API.Interfaces
{
    public interface IUsersRepository : IGenericRepository<Users>
    {
        Task<Users> GetUserByAccountID(int accountId);
        Task<Users> GetUserByUserID(int? userId);
        Task<bool> CheckComfirmPass(int userId, string code);
        bool IsValidPhoneNumber(string phonenumber);
        bool IsValidEmail(string email);
        Task<Users> GetUserByEmail(string email);
        Task<PagingOutput<IEnumerable<Users>>> SearchAllUserByUsername(string username, int indexPage, int pageSize);
        Task<PagingOutput<IEnumerable<Users>>> SearchAllUser(int indexPage, int pageSize);
        Task<IEnumerable<Users>> GetUserSQL(string strUserss);
    }
}
