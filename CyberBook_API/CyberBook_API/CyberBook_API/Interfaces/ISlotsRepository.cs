﻿using CyberBook_API.Dal;
using CyberBook_API.Models;
using CyberBook_API.ViewModel.PagingView;
using CyberBook_API.ViewModel.SlotViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CyberBook_API.Interfaces
{
    public interface ISlotsRepository : IGenericRepository<Slot>
    {
        Task<PagingOutput<IEnumerable<Slot>>> GetAllSlotByRoomId(int roomId, int pageIndex, int pageSize);
        Task<PagingOutput<IEnumerable<Slot>>> GetSlotByFilter(SlotsFilterInViewModel modelFilter, int pageIndex, int pageSize);
        Task<Slot> GetSlotById(int slotId);
        Task<Slot> IsSlotReady(int slotId);
        Task<Slot> UpdateStatusSlot(int slotId, int statusSlot);
        Task<int> CountSlotReadyByRoomId(int roomId);
        Task<int> CountAllSlotInRoomByRoomId(int roomId);
        Task<bool> SlotPositionExistCheck(int? roomId, int? posX, int? posY);
        Task<bool> SlotPositionIsUnload(int? roomId, int? posX, int? posY);
        Task<bool> IsHardwareNoSlot(int hardwareId);
    }
}
