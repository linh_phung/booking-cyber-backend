﻿using CyberBook_API.Dal;
using CyberBook_API.Models;
using CyberBook_API.ViewModel.PagingView;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CyberBook_API.Interfaces
{
    public interface IAdminsRepository : IGenericRepository<Users>
    {
        Task<bool> IsSuperAdmin(int userId);
        Task<PagingOutput<IEnumerable<Cyber>>> GetListCyberRegisterPending(int indexPage, int pageSize);
        Task<PagingOutput<IEnumerable<Cyber>>> GetListCyberByStatusId(int statusCyber, int indexPage, int pageSize);
        Task<Users> AdminLockUserById(int userId);
        Task<Users> AdminUnLockUserById(int userId);
        Task<bool> RejectRegisterCyber(int cyberId);
    }
}
