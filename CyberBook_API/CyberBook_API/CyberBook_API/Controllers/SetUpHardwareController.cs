﻿using CyberBook_API.Dal.Repositories;
using CyberBook_API.Interfaces;
using CyberBook_API.Models;
using CyberBook_API.Services;
using CyberBook_API.Services.Interfaces;
using CyberBook_API.ViewModel.AccountView;
using CyberBook_API.ViewModel.ConfigHardwareViewModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace CyberBook_API.Controllers
{
    [Route("api/[controller]")]
    //[Authorize]
    [ApiController]
    public class SetUpHardwareController : ControllerBase
    {
        private readonly IHardwareConfigsService _hardwareConfigsService = new HardwareConfigsService();


        /// <summary>
        /// tạo mới một cấu hình của máy       
        /// huynhnd53 30/10/2021
        /// </summary>
        /// <param name="hardwareIN"></param>
        /// <returns></returns>
        [HttpPost("CreateSlotHardwareConfig")]
        public async Task<ActionResult> CreateSlotHardwareConfig([FromBody] SlotHardwareConfig hardwareIN)
        {
            var usernameCrr = User.Claims.Where(c => c.Type == ClaimTypes.Name).Select(c => c.Value).SingleOrDefault();
            ServiceResponse<SlotHardwareConfig> response = await _hardwareConfigsService.CreateSlotHardwareConfig(usernameCrr, hardwareIN);
            if (response.Success != true)
            {
                return NotFound(response);
            }
            return Ok(response);
        }

        /// <summary>
        /// cập nhật thông tin của một cấu hình 
        /// huynhnd53  30/10/2021
        /// </summary>
        /// <param name="hardwareIN"></param>
        /// <returns></returns>
        [HttpPatch("UpdateSlotHardwareConfig")]
        public async Task<ActionResult> UpdateSlotHardwareConfig([FromBody] HardwareConfigEditViewModel hardwareIN)
        {
            var usernameCrr = User.Claims.Where(c => c.Type == ClaimTypes.Name).Select(c => c.Value).SingleOrDefault();
            ServiceResponse<SlotHardwareConfig> response = await _hardwareConfigsService.UpdateSlotHardwareConfig(usernameCrr, hardwareIN);
            if (response.Success != true)
            {
                return NotFound(response);
            }
            return Ok(response);
        }

        /// <summary>
        /// Xóa một cấu hình máy 
        /// huynhnd53 30/10/2021
        /// </summary>
        /// <param name="hardwareId"></param>
        /// <returns></returns>
        [HttpDelete("RemoveSlotHardwareConfig")]
        public async Task<ActionResult> RemoveSlotHardwareConfig(int hardwareId)
        {
            var usernameCrr = User.Claims.Where(c => c.Type == ClaimTypes.Name).Select(c => c.Value).SingleOrDefault();
            ServiceResponse<int> response = await _hardwareConfigsService.RemoveSlotHardwareConfig(usernameCrr, hardwareId);
            if (response.Success != true)
            {
                return NotFound(response);
            }
            return Ok(response);
        }

        /// <summary>
        /// lấy danh sách các cấu hình máy của cyber
        /// huynhnd53 30/10/2021
        /// </summary>
        /// <param name="cyberId"></param>
        /// <returns></returns>
        [HttpGet("GetHardwaresByCyberId")]
        public async Task<ActionResult> GetHardwaresByCyberId(int cyberId)
        {
            ServiceResponse<IEnumerable<SlotHardwareConfigOutViewModel>> response = await _hardwareConfigsService.GetHardwaresByCyberId(cyberId);
            return Ok(response);
        }

        /// <summary>
        /// lấy thông tin của một cấu hình 
        /// huynhnd53 30/10/2021
        /// </summary>
        /// <param name="hardwareId"></param>
        /// <returns></returns>
        [HttpGet("GetHardwaresById")]
        public async Task<ActionResult> GetHardwaresById(int hardwareId)
        {
            ServiceResponse<SlotHardwareConfig> response = await _hardwareConfigsService.GetHardwaresById(hardwareId);
            if (response.Success != true)
            {
                return NotFound(response);
            }
            return Ok(response);
        }

    }
}
