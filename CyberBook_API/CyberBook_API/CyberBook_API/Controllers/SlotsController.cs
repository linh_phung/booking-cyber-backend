﻿using CyberBook_API.Dal.Repositories;
using CyberBook_API.Enum;
using CyberBook_API.Interfaces;
using CyberBook_API.Models;
using CyberBook_API.Services;
using CyberBook_API.Services.Interfaces;
using CyberBook_API.ViewModel.AccountView;
using CyberBook_API.ViewModel.PagingView;
using CyberBook_API.ViewModel.SlotViewModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace CyberBook_API.Controllers
{
    [Route("api/[controller]")]
    //[Authorize]
    [ApiController]
    public class SlotsController : ControllerBase
    {
        private readonly ISlotsService _slotsService = new SlotsService();

        /// <summary>
        /// tạo mới một slot
        ///    huynhnd53 02/11/2021
        /// </summary>
        /// <param name="slotViewModelIn"></param>
        /// <returns></returns>
        //[Authorize]
        [HttpPost("CreateNewSlot")]
        public async Task<ActionResult> CreateNewSlot([FromBody] SlotCreateViewModelIn slotCreateViewModelIn)
        {
            var usernameCrr = User.Claims.Where(c => c.Type == ClaimTypes.Name).Select(c => c.Value).SingleOrDefault();
            ServiceResponse<Slot> response = await _slotsService.CreateNewSlot(usernameCrr, slotCreateViewModelIn);
            if(response.Success != true)
            {
                return NotFound(response);
            }
            return Ok(response);
        }

        /// <summary>
        /// sửa một slot 
        /// huynhnd53 02/11/2021
        /// </summary>
        /// <param name="slotViewModelIn"></param>
        /// <returns></returns>
        //[Authorize]
        [HttpPatch("EditSlot")]
        public async Task<ActionResult> EditSlot([FromBody] SlotViewModelIn slotViewModelIn)
        {
            var usernameCrr = User.Claims.Where(c => c.Type == ClaimTypes.Name).Select(c => c.Value).SingleOrDefault();
            ServiceResponse<Slot> response = await _slotsService.EditSlot(usernameCrr, slotViewModelIn);
            if (response.Success != true)
            {
                return NotFound(response);
            }
            return Ok(response);
        }

        /// <summary>
        /// Xóa đi một Slot
        /// huynhnd53 02/11/2021
        /// </summary>
        /// <param name="slotViewModelIn"></param>
        /// <returns></returns>
        //[Authorize]
        [HttpDelete("RemoveSlot")]
        public async Task<ActionResult> RemoveSlot(int slotId)
        {
            var usernameCrr = User.Claims.Where(c => c.Type == ClaimTypes.Name).Select(c => c.Value).SingleOrDefault();
            ServiceResponse<int> response = await _slotsService.RemoveSlot(usernameCrr, slotId);
            if (response.Success != true)
            {
                return NotFound(response);
            }
            return Ok(response);
        }

        /// <summary>
        /// lấy slot bằng id 
        /// huynhnd53 02/11/2021
        /// </summary>
        /// <param name="slotId"></param>
        /// <returns></returns>
        [HttpGet("GetSlotById")]
        public async Task<ActionResult> GetSlotById(int slotId)
        {
            ServiceResponse<Slot> response = await _slotsService.GetSlotById(slotId);
            if (response.Success != true)
            {
                return NotFound(response);
            }
            return Ok(response);
        }

        /// <summary>
        ///   lấy danh sách slot của một phòng
        ///   huynhnd53 02/11/2021
        /// </summary>
        /// <param name="roomId"></param>
        /// <returns></returns>
        [HttpGet("GetListSlotByRoom")]
        public async Task<ActionResult> GetListSlotByRoom(int pageIndex, int pageSize, int roomId)
        {
            PagingOutput<IEnumerable<Slot>> response = await _slotsService.GetListSlotByRoom(pageIndex, pageSize, roomId);
            return Ok(response);
        }

        [HttpPatch("EditStatusSlot")]
        public async Task<ActionResult> EditStatusSlot(int slotId, int statusSlot)
        {
            var usernameCrr = User.Claims.Where(c => c.Type == ClaimTypes.Name).Select(c => c.Value).SingleOrDefault();
            ServiceResponse<Slot> response = await _slotsService.EditStatusSlot(usernameCrr, slotId, statusSlot);
            if (response.Success != true)
            {
                return NotFound(response);
            }
            return Ok(response);
        }

        ///// <summary>
        ///// lấy danh sách slot trong một phòng theo trạng thái đã chọn
        /////  huynhnd53 02/11/2021
        ///// </summary>
        ///// <param name="roomId"></param>
        ///// <param name="statusSlot"></param>
        ///// <returns></returns>
        //[HttpGet("GetListSlotByFilter")]
        //public async Task<ActionResult> GetListSlotByFilter(PagingOutput<SlotsFilterInViewModel> paging)
        //{
        //    var output = new PagingOutput<IEnumerable<Slot>>();
        //    try
        //    {
        //        var room = await _roomRepository.GetRoomById(paging.Data.RoomId);
        //        if (room != null)
        //        {
        //            var result = await _slotsRepository.GetSlotByFilter(paging.Data, paging.Index, paging.PageSize);
        //            if (result != null)
        //            {
        //                output = result;
        //                output.Message = "successfully";
        //                return Ok(output);
        //            }
        //            output.Message = "no slot in this room";
        //            return Ok(output);
        //        }
        //        output.Message = "room not exist";
        //        return Ok(output);
        //    }
        //    catch (Exception e)
        //    {
        //        output.Message = "getlistslotbyroom exception: " + e.Message;
        //    }
        //    return Ok(output);
        //}
    }
}
