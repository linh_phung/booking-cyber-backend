﻿using CyberBook_API.Dal.Repositories;
using CyberBook_API.Interfaces;
using CyberBook_API.Models;
using CyberBook_API.Services;
using CyberBook_API.Services.Interfaces;
using CyberBook_API.ViewModel.PagingView;
using CyberBook_API.ViewModel.SearchViewModel;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace CyberBook_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AdminUserController : ControllerBase
    {
        private readonly IAdminUsersService _adminUsersService = new AdminUsersService();

        [HttpGet("ListUser")]
        public async Task<ActionResult> ListUser(int pageIndex, int pageSize)
        {
            var usernameCrr = User.Claims.Where(c => c.Type == ClaimTypes.Name).Select(c => c.Value).SingleOrDefault();
            PagingOutput<IEnumerable<Users>> response = await _adminUsersService.ListUser(usernameCrr, pageIndex, pageSize);
            return Ok(response);
        }

        [HttpGet("ViewDetailUser")]
        public async Task<ActionResult> ViewDetailUser(int userId)
        {
            var usernameCrr = User.Claims.Where(c => c.Type == ClaimTypes.Name).Select(c => c.Value).SingleOrDefault();
            ServiceResponse<Users> response = await _adminUsersService.ViewDetailUser(usernameCrr, userId);
            if (response.Success != true)
            {
                return NotFound(response);
            }
            return Ok(response);
        }

        [HttpGet("SearchUserByName")]
        public async Task<ActionResult> SearchUserByName(int pageIndex, int pageSize, string username)
        {
            var usernameCrr = User.Claims.Where(c => c.Type == ClaimTypes.Name).Select(c => c.Value).SingleOrDefault();
            PagingOutput<IEnumerable<Users>> response = await _adminUsersService.SearchUserByName(usernameCrr, pageIndex, pageSize, username);
            return Ok(response);
        }

        [HttpPatch("LockUserById")]
        public async Task<ActionResult> LockUserById(int userId)
        {
            var usernameCrr = User.Claims.Where(c => c.Type == ClaimTypes.Name).Select(c => c.Value).SingleOrDefault();
            ServiceResponse<Users> response = await _adminUsersService.LockUserById(usernameCrr, userId);
            if (response.Success != true)
            {
                return NotFound(response);
            }
            return Ok(response);
        }

        [HttpPatch("UnLockUserById")]
        public async Task<ActionResult> UnLockUserById(int userId)
        {
            var usernameCrr = User.Claims.Where(c => c.Type == ClaimTypes.Name).Select(c => c.Value).SingleOrDefault();
            ServiceResponse<Users> response = await _adminUsersService.UnLockUserById(usernameCrr, userId);
            if (response.Success != true)
            {
                return NotFound(response);
            }
            return Ok(response);
        }
    }
}
