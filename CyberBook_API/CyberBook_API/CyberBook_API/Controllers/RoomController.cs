﻿using CyberBook_API.Dal.Repositories;
using CyberBook_API.Interfaces;
using CyberBook_API.Models;
using CyberBook_API.Services;
using CyberBook_API.Services.Interfaces;
using CyberBook_API.ViewModel.AccountView;
using CyberBook_API.ViewModel.PagingView;
using CyberBook_API.ViewModel.RoomViewModel;
using CyberBook_API.ViewModel.SlotViewModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace CyberBook_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RoomController : ControllerBase
    {
        private readonly IRoomsService _roomsService = new RoomsService();

        /// <summary>
        /// tạo mới 1 Room
        /// huynhnd53 02/11/2021
        /// </summary>
        /// <param name="roomIn"></param>
        /// <returns></returns>
        //[Authorize]
        [HttpPost("CreateNewRoom")]
        public async Task<ActionResult> CreateNewRoom([FromBody] RoomCreateViewModel roomIn)
        {
            var usernameCrr = User.Claims.Where(c => c.Type == ClaimTypes.Name).Select(c => c.Value).SingleOrDefault();
            ServiceResponse<RoomOutViewModel> response = await _roomsService.CreateNewRoom(usernameCrr, roomIn);
            if(response.Success != true)
            {
                return NotFound(response);
            }
            return Ok(response);
        }

        /// <summary>
        /// Sửa thông tin 1 Room 
        /// huynhnd53 02/11/2021
        /// </summary>
        /// <param name="roomIn"></param>
        /// <returns></returns>
        //[Authorize]
        [HttpPatch("EditRoomInfor")]
        public async Task<ActionResult> EditRoomInfor([FromBody] RoomEditViewModel roomIn)
        {
            var usernameCrr = User.Claims.Where(c => c.Type == ClaimTypes.Name).Select(c => c.Value).SingleOrDefault();
            ServiceResponse<RoomOutViewModel> response = await _roomsService.EditRoomInfor(usernameCrr, roomIn);
            if (response.Success != true)
            {
                return NotFound(response);
            }
            return Ok(response);
        }

        //[Authorize]
        [HttpPatch("EditSizeRoom-Allow")]
        public async Task<ActionResult> EditSizeRoomAllow(int roomId, int maxX, int maxY)
        {
            var usernameCrr = User.Claims.Where(c => c.Type == ClaimTypes.Name).Select(c => c.Value).SingleOrDefault();
            ServiceResponse<RoomOutViewModel> response = await _roomsService.EditSizeRoomAllow(usernameCrr, roomId, maxX,maxY);
            if (response.Success != true)
            {
                return NotFound(response);
            }
            return Ok(response);
        }

        //[Authorize]
        [HttpPatch("EditSizeRoom")]
        public async Task<ActionResult> EditSizeRoom(int roomId, int maxX, int maxY)
        {
            var usernameCrr = User.Claims.Where(c => c.Type == ClaimTypes.Name).Select(c => c.Value).SingleOrDefault();
            ServiceResponse<RoomOutViewModel> response = await _roomsService.EditSizeRoom(usernameCrr, roomId, maxX, maxY);
            if (response.Success != true)
            {
                return NotFound(response);
            }
            return Ok(response);
        }

        /// <summary>
        /// Xóa đi 1 phòng 
        /// huynhnd53 02/11/2021
        /// </summary>
        /// <param name="roomIn"></param>
        /// <returns></returns>
        //[Authorize]
        [HttpDelete("RemoveRoom")]
        public async Task<ActionResult> RemoveRoom(int roomId)
        {
            var usernameCrr = User.Claims.Where(c => c.Type == ClaimTypes.Name).Select(c => c.Value).SingleOrDefault();
            ServiceResponse<int> response = await _roomsService.RemoveRoom(usernameCrr, roomId);
            if (response.Success != true)
            {
                return NotFound(response);
            }
            return Ok(response);
        }

        /// <summary>
        /// lấy thông tin của 1 room bằng id
        /// huynhnd53 02/11/2021
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("GetRoomById")]
        public async Task<ActionResult> GetRoomById(int roomId)
        {
            ServiceResponse<RoomOutViewModel> response = await _roomsService.GetRoomById(roomId);
            if (response.Success != true)
            {
                return NotFound(response);
            }
            return Ok(response);
        }

        /// <summary>
        /// lấy danh sách phòng của 1 cyber
        /// huynhnd53 02/11/2021
        /// </summary>
        /// <param name="cyberId"></param>
        /// <returns></returns>
        [HttpGet("GetListRoomByCyberId")]
        public async Task<ActionResult> GetListRoomByCyberId(int pageIndex, int pageSize, int cyberId)
        {
            PagingOutput<IEnumerable<RoomOutViewModel>> response = await _roomsService.GetListRoomByCyberId(pageIndex, pageSize, cyberId);
            return Ok(response);
        }

        //[HttpGet("GetRoomTypeByCyberId")]
        //public async Task<ActionResult> GetRoomTypeByCyberId(int cyberId)
        //{
        //    var output = new ServiceResponse<IEnumerable<RoomType>>();
        //    try
        //    {
        //        var roomType = await _roomTypeRepository.GetRoomTypeByCyberId(cyberId);
        //        if (roomType != null)
        //        {
        //            output.Data = roomType;
        //            output.Message = "Successfully";
        //            output.Success = true;
        //            return Ok(output);
        //        }
        //        output.Message = "Room NOT exist";
        //        return Ok(output);
        //    }
        //    catch (Exception e)
        //    {
        //        output.Message = "GetRoomById Exception: " + e.Message;
        //    }
        //    return Ok(output);
        //}
    }
}
