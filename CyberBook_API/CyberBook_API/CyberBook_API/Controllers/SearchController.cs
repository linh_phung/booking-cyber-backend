﻿using CyberBook_API.Dal.Repositories;
using CyberBook_API.Interfaces;
using CyberBook_API.Models;
using CyberBook_API.Services;
using CyberBook_API.Services.Interfaces;
using CyberBook_API.ViewModel;
using CyberBook_API.ViewModel.AccountView;
using CyberBook_API.ViewModel.CyberViewModel;
using CyberBook_API.ViewModel.PagingView;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace CyberBook_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SearchController : ControllerBase
    {
        private readonly ISearchService _searchService = new SearchService();

        [HttpGet("GetAllCybers")]
        public async Task<ActionResult> GetAllCybers(int pageIndex, int pageSize)
        {
            PagingOutput<IEnumerable<CyberViewModelOut>> response = await _searchService.GetAllCybers(pageIndex, pageSize);
            return Ok(response);
        }

        [HttpGet("SearchCyberByName")]
        public async Task<ActionResult> SearchCyberByName(int pageIndex, int pageSize, string searchContent)
        {
            PagingOutput<IEnumerable<CyberViewModelOut>> response = await _searchService.SearchCyberByName(pageIndex, pageSize, searchContent);
            return Ok(response);
        }

        [HttpGet("GetCyberById")]
        public async Task<ActionResult> GetCyberById(int id)
        {
            ServiceResponse<CyberViewModelOut> response = await _searchService.GetCyberById(id);
            if (response.Success != true)
            {
                return NotFound(response);
            }
            return Ok(response);

        }

        [HttpGet("ViewCyberByRate")]
        public async Task<ActionResult> SearchCyberByRate(int pageIndex, int pageSize)
        {
            PagingOutput<IEnumerable<CyberViewModelOut>> response = await _searchService.SearchCyberByRate(pageIndex, pageSize);
            return Ok(response);
        }

    }
}
