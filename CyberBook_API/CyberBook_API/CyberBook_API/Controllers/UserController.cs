﻿using CyberBook_API.Dal.Repositories;
using CyberBook_API.Interfaces;
using CyberBook_API.Models;
using CyberBook_API.Services;
using CyberBook_API.Services.Interfaces;
using CyberBook_API.ViewModel.AccountView;
using CyberBook_API.ViewModel.PagingView;
using CyberBook_API.ViewModel.SlotViewModel;
using CyberBook_API.ViewModel.UserViewModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace CyberBook_API.Controllers
{
    [Route("api/[controller]")]
    //[Authorize]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUsersService _usersService = new UsersService();

        /// <summary>
        /// Người dùng thêm mới 1 Cyber Account 
        ///  huynhnd53 02/11/2021
        /// </summary>
        /// <param name="cyberAccount"></param>
        /// <returns></returns>
        [HttpPost("AddNewCyberAccount")]
        public async Task<ActionResult> AddNewCyberAccount([FromBody] CyberAccount cyberAccount)
        {
            var usernameCrr = User.Claims.Where(c => c.Type == ClaimTypes.Name).Select(c => c.Value).SingleOrDefault();
            ServiceResponse<CyberAccount> response = await _usersService.AddNewCyberAccount(usernameCrr, cyberAccount);
            if (response.Success != true)
            {
                return NotFound(response);
            }
            return Ok(response);
        }

        /// <summary>
        /// Người dùng sửa một bản ghi Cyber Account
        /// huynhnd53 02/11/2021
        /// </summary>
        /// <param name="cyberAccount"></param>
        /// <returns></returns>
        [HttpPatch("EditCyberAccount")]
        public async Task<ActionResult> EditCyberAccount([FromBody] CyberAccount cyberAccount)
        {
            var usernameCrr = User.Claims.Where(c => c.Type == ClaimTypes.Name).Select(c => c.Value).SingleOrDefault();
            ServiceResponse<CyberAccount> response = await _usersService.EditCyberAccount(usernameCrr, cyberAccount);
            if (response.Success != true)
            {
                return NotFound(response);
            }
            return Ok(response);
        }

        /// <summary>
        /// Người dùng xóa đi CyberAccount
        /// huynhnd53 02/11/2021
        /// </summary>
        /// <param name="cyberAccount"></param>
        /// <returns></returns>
        [HttpDelete("RemoveCyberAccount")]
        public async Task<ActionResult> RemoveCyberAccount(int cyberAccountId)
        {
            var usernameCrr = User.Claims.Where(c => c.Type == ClaimTypes.Name).Select(c => c.Value).SingleOrDefault();
            ServiceResponse<int> response = await _usersService.RemoveCyberAccount(usernameCrr, cyberAccountId);
            if (response.Success != true)
            {
                return NotFound(response);
            }
            return Ok(response);
        }


        /// <summary>
        /// người dùng xem thông tin của CyberAccount của họ
        ///   huynhnd53 02/11/2021
        /// </summary>
        /// <param name="cyberAccountId"></param>
        /// <returns></returns>
        [HttpGet("GetDetailCyberAccount")]
        public async Task<ActionResult> GetDetailCyberAccount(int cyberAccountId)
        {
            var usernameCrr = User.Claims.Where(c => c.Type == ClaimTypes.Name).Select(c => c.Value).SingleOrDefault();
            ServiceResponse<CyberAccount> response = await _usersService.GetDetailCyberAccount(usernameCrr, cyberAccountId);
            if (response.Success != true)
            {
                return NotFound(response);
            }
            return Ok(response);
        }

        /// <summary>
        /// lấy ra danh sách CyberAccount của người dùng hiện tại
        /// huynhnd53 02/11/2021
        /// </summary>
        /// <returns></returns>
        [HttpGet("ViewListCyberAccount")]
        public async Task<ActionResult> ViewListCyberAccount(int pageIndex, int pageSize)
        {
            var usernameCrr = User.Claims.Where(c => c.Type == ClaimTypes.Name).Select(c => c.Value).SingleOrDefault();
            PagingOutput<IEnumerable<CyberAccount>> response = await _usersService.ViewMyListCyberAccount(usernameCrr, pageIndex, pageSize);
            return Ok(response);
        }

        [HttpPatch("EditUserInfor")]
        public async Task<ActionResult> EditUserInfor(UserEditViewModel userIn)
        {
            var usernameCrr = User.Claims.Where(c => c.Type == ClaimTypes.Name).Select(c => c.Value).SingleOrDefault();
            ServiceResponse<UsersOutViewModel> response = await _usersService.EditUserInfor(usernameCrr, userIn);
            if (response.Success != true)
            {
                return NotFound(response);
            }
            return Ok(response);
        }

        [HttpGet("GetMyInfor")]
        public async Task<ActionResult> GetMyInfor()
        {
            var usernameCrr = User.Claims.Where(c => c.Type == ClaimTypes.Name).Select(c => c.Value).SingleOrDefault();
            ServiceResponse<UsersOutViewModel> response = await _usersService.GetMyInfor(usernameCrr);
            if (response.Success != true)
            {
                return NotFound(response);
            }
            return Ok(response);
        }
    }
}
