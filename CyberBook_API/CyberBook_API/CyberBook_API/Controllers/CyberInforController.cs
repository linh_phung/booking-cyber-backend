﻿using CyberBook_API.Dal.Repositories;
using CyberBook_API.Interfaces;
using CyberBook_API.Models;
using CyberBook_API.Services;
using CyberBook_API.Services.Interfaces;
using CyberBook_API.ViewModel.CyberViewModel;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace CyberBook_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CyberInforController : Controller
    {
        private readonly ICybersService _cybersService = new CybersService();

        [HttpGet("GetCyberById")]
        public async Task<ActionResult> GetCyberById(int id)
        {
            ServiceResponse<Cyber> response = await _cybersService.GetCyberById(id);
            if(response.Success != true)
            {
                return NotFound(response);
            }
            return Ok(response);
        }

        [HttpGet("GetMyCyber")]
        public async Task<ActionResult> GetMyCyber()
        {
            var usernameCrr = User.Claims.Where(c => c.Type == ClaimTypes.Name).Select(c => c.Value).SingleOrDefault();
            ServiceResponse<Cyber> response = await _cybersService.GetMyCyber(usernameCrr);
            if (response.Success != true)
            {
                return NotFound(response);
            }
            return Ok(response);
        }

        [HttpPatch("EditCyberInfor")]
        public async Task<ActionResult> EditCyberInfor([FromBody] CyberEditViewModel cyberIn)
        {
            var usernameCrr = User.Claims.Where(c => c.Type == ClaimTypes.Name).Select(c => c.Value).SingleOrDefault();
            ServiceResponse<Cyber> response = await _cybersService.EditCyberInfor(usernameCrr, cyberIn);
            if (response.Success != true)
            {
                return NotFound(response);
            }
            return Ok(response);
        }
    }
}
