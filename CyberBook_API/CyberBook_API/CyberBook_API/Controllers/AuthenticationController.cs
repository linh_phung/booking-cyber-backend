﻿using CyberBook_API.Dal.Repositories;
using CyberBook_API.Enum;
using CyberBook_API.Interfaces;
using CyberBook_API.Models;
using CyberBook_API.Services;
using CyberBook_API.Services.Interfaces;
using CyberBook_API.ViewModel.AccountView;
using CyberBook_API.ViewModel.AccountViewModel;
using CyberBook_API.ViewModel.TokenViewModel;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace CyberBook_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthenticationController : ControllerBase
    {
        private readonly IAuthenticationService _authenticationService;

        public AuthenticationController(IConfiguration config)
        {
            _authenticationService = new AuthenticationService(config);
        }

        public IConfiguration Configuration { get; }
        //private readonly IAuthenticationService _authenticationService = new AuthenticationService();

        //private readonly JWTSettings _jwtsettings;

        //public AuthenticationController(IOptions<JWTSettings> jwtsettings)
        //{
        //    _jwtsettings = jwtsettings.Value;
        //}

        /// <summary>
        /// Login
        /// </summary>
        /// <param name="account"></param>
        /// <returns></returns>
        [HttpPost("Login")]
        public async Task<ActionResult> Login([FromBody] Account account)
        {
            ServiceResponse<AccountViewModel> response = await _authenticationService.Login(account);
            if (response.Success != true)
            {
                return Unauthorized(response);
            }
            return Ok(response);
        }

          /// <summary>
        /// Get token 
        /// </summary>
        /// <param name="account"></param>
        /// <returns></returns>
        [HttpPost("GetToken")]
        public async Task<ActionResult> GetToken([FromBody] Account account)
        {
            ServiceResponse<TokenViewModel> response = await _authenticationService.GetToken(account);
            if (response.Success != true)
            {
                return Unauthorized(response);
            }
            return Ok(response);
        }


        /// <summary>
        /// Get token 
        /// </summary>
        /// <param name="account"></param>
        /// <returns></returns>
        [HttpPost("RefreshToken")]
        public async Task<ActionResult> RefreshToken([FromBody] RefreshRequestViewModel refreshRequest)
        {
            
            return null;
        }

        [HttpPost("Reigster")]
        public async Task<ActionResult> Reigster([FromBody] AccountRegisterViewModel accountRegisterIn)
        {
            ServiceResponse<AccountViewModel> response = await _authenticationService.Reigster(accountRegisterIn);
            if (response.Success != true)
            {
                return Unauthorized(response);
            }
            return Ok(response);
        }

        [HttpPost("SendCodeResetPassword")]
        public async Task<ActionResult> SendCodeResetPassword(string email)
        {
            ServiceResponse<Users> response = await _authenticationService.SendCodeResetPassword(email);
            if (response.Success != true)
            {
                return NotFound(response);
            }
            return Ok(response);
        }

        [HttpPost("ConfirmPass")]
        public async Task<ActionResult> ConfirmPass(string email, string code)
        {
            var output = new ServiceResponse<Users>();
            ServiceResponse<Account> response = await _authenticationService.ConfirmPass(email, code);
            if (response.Success != true)
            {
                return NotFound(response);
            }
            return Ok(response);
        }

        [HttpPost("SetPassword")]
        public async Task<ActionResult> SetPassword(int accountId, string pass)
        {
            ServiceResponse<string> response = await _authenticationService.SetPassword(accountId, pass);
            if (response.Success != true)
            {
                return NotFound(response);
            }
            return Ok(response);
        }

        [HttpPost("ChangePassword")]
        public async Task<ActionResult> ChangePassword(string oldPass, string newPass)
        {
            var usernameCrr = User.Claims.Where(c => c.Type == ClaimTypes.Name).Select(c => c.Value).SingleOrDefault();
            ServiceResponse<string> response = await _authenticationService.ChangePassword(usernameCrr, oldPass, newPass);
            if (response.Success != true)
            {
                return NotFound(response);
            }
            return Ok(response);
        }
    }
}
