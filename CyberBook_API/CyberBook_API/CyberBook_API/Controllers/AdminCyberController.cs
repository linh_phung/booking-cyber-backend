﻿using CyberBook_API.Dal.Repositories;
using CyberBook_API.Enum;
using CyberBook_API.Interfaces;
using CyberBook_API.Models;
using CyberBook_API.Services;
using CyberBook_API.Services.Interfaces;
using CyberBook_API.ViewModel.CyberViewModel;
using CyberBook_API.ViewModel.PagingView;
using CyberBook_API.ViewModel.SearchViewModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace CyberBook_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AdminCyberController : ControllerBase
    {
        private readonly IAdminCybersService _adminCybersService = new AdminCybersService();
        
        
        //[Authorize]
        [HttpGet("ListCyber")]
        public async Task<ActionResult> ListCyber()
        {
            var usernameCrr = User.Claims.Where(c => c.Type == ClaimTypes.Name).Select(c => c.Value).SingleOrDefault();
            ServiceResponse<IEnumerable<CyberViewModelOut>> response = await _adminCybersService.AdminListCyber(usernameCrr);
            return Ok(response);
        }

        [HttpGet("ViewDetailCyber")]
        public async Task<ActionResult> ViewDetailCyber(int cyberId)
        {
            ServiceResponse<CyberViewModelOut> response = await _adminCybersService.ViewDetailCyber(cyberId);
            if (response.Success != true)
            {
                return NotFound(response);
            }
            return Ok(response);
        }

        [HttpPost("LockCyber")]
        public async Task<ActionResult> LockCyber(int cyberId)
        {
            var usernameCrr = User.Claims.Where(c => c.Type == ClaimTypes.Name).Select(c => c.Value).SingleOrDefault();
            ServiceResponse<CyberViewModelOut> response = await _adminCybersService.LockCyber(usernameCrr, cyberId);
            if (response.Success != true)
            {
                return NotFound(response);
            }
            return Ok(response);
        }

        [HttpPost("UnLockCyber")]
        public async Task<ActionResult> UnLockCyber(int cyberId)
        {
            var usernameCrr = User.Claims.Where(c => c.Type == ClaimTypes.Name).Select(c => c.Value).SingleOrDefault();
            ServiceResponse<CyberViewModelOut> response = await _adminCybersService.UnLockCyber(usernameCrr, cyberId);
            if (response.Success != true)
            {
                return NotFound(response);
            }
            return Ok(response);
        }

        [HttpPost("RegisterCyber")]
        public async Task<ActionResult> RegisterCyber([FromBody] CyberViewModel cyberViewModel)
        {
            ServiceResponse<CyberViewModelOut> response = await _adminCybersService.RegisterCyber(cyberViewModel);
            if (response.Success != true)
            {
                return NotFound(response);
            }
            return Ok(response);
        }

        [HttpDelete("RejectCyber")]
        public async Task<ActionResult> RejectCyber(int cyberId)
        {
            var usernameCrr = User.Claims.Where(c => c.Type == ClaimTypes.Name).Select(c => c.Value).SingleOrDefault();
            ServiceResponse<string> response = await _adminCybersService.RejectCyber(usernameCrr, cyberId);
            if (response.Success != true)
            {
                return NotFound(response);
            }
            return Ok(response);
        }

        [HttpPost("VerifyCyber")]
        public async Task<ActionResult> VerifyCyber(int cyberId)
        {
            var usernameCrr = User.Claims.Where(c => c.Type == ClaimTypes.Name).Select(c => c.Value).SingleOrDefault();
            ServiceResponse<CyberViewModelOut> response = await _adminCybersService.VerifyCyber(usernameCrr, cyberId);
            if (response.Success != true)
            {
                return NotFound(response);
            }
            return Ok(response);
        }

        [HttpGet("ViewListCyberRegisterPending")]
        public async Task<ActionResult> ViewListCyberRegisterPending(int pageIndex, int pageSize)
        {
            var usernameCrr = User.Claims.Where(c => c.Type == ClaimTypes.Name).Select(c => c.Value).SingleOrDefault();
            PagingOutput<IEnumerable<CyberViewModelOut>> response = await _adminCybersService.ViewListCyberRegisterPending(usernameCrr, pageIndex, pageSize);
            return Ok(response);
        }

        [HttpGet("ViewListCyberByFilter")]
        public async Task<ActionResult> ViewListCyberByFilter(int pageIndex, int pageSize, string statusCyberId, string cyberName)
        {
            var usernameCrr = User.Claims.Where(c => c.Type == ClaimTypes.Name).Select(c => c.Value).SingleOrDefault();
            PagingOutput<IEnumerable<CyberViewModelOut>> response = await _adminCybersService.ViewListCyberByFilter(usernameCrr, pageIndex, pageSize,statusCyberId, cyberName);
            return Ok(response);

        }
    }
}
