﻿using CyberBook_API.Dal.Repositories;
using CyberBook_API.Interfaces;
using CyberBook_API.Models;
using CyberBook_API.Services;
using CyberBook_API.Services.Interfaces;
using CyberBook_API.ViewModel.AccountView;
using CyberBook_API.ViewModel.RateUserViewModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace CyberBook_API.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    [ApiController]
    public class CyberRateUserController : ControllerBase
    {
        private readonly ICyberRateUserService _cyberRateUserService = new CyberRateUserService();

        /// <summary>
        ///  thêm một bản ghi mới cho Cyber đánh giá một User
        ///  huynhnd53
        /// </summary>
        /// <param name="ratingUser"></param>
        /// <returns></returns>
        [HttpPost("AddNewRateUser")]
        public async Task<ActionResult> AddNewRateUser([FromBody] RatingUser ratingUser)
        {
            var usernameCrr = User.Claims.Where(c => c.Type == ClaimTypes.Name).Select(c => c.Value).SingleOrDefault();
            ServiceResponse<RateUserOutViewModel> response = await _cyberRateUserService.AddNewRateUser(usernameCrr, ratingUser);
            if (response.Success != true)
            {
                return NotFound(response);
            }
            return Ok(response);
        }

        /// <summary>
        /// Update bản ghi cyber đánh giá User
        /// huynhnd53
        /// </summary>
        /// <param name="ratingUser"></param>
        /// <returns></returns>
        [HttpPatch("UpdateRateUser")]
        public async Task<ActionResult> UpdateRateUser([FromBody] RatingUser ratingUser)
        {
            var usernameCrr = User.Claims.Where(c => c.Type == ClaimTypes.Name).Select(c => c.Value).SingleOrDefault();
            ServiceResponse<RateUserOutViewModel> response = await _cyberRateUserService.UpdateRateUser(usernameCrr, ratingUser);
            if (response.Success != true)
            {
                return NotFound(response);
            }
            return Ok(response);
        }

        /// <summary>
        /// lấy bản ghi cyber đánh giá user bằng id của bản ghi
        /// huynhnd53
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("GetRateById")]
        public async Task<ActionResult> GetRateById(int id)
        {
            var usernameCrr = User.Claims.Where(c => c.Type == ClaimTypes.Name).Select(c => c.Value).SingleOrDefault();
            ServiceResponse<RateUserOutViewModel> response = await _cyberRateUserService.GetRateById(id);
            if (response.Success != true)
            {
                return NotFound(response);
            }
            return Ok(response);
        }

        /// <summary>
        /// Xóa một bản đánh giá người dùng bằng Id
        /// huynhnd53
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("RemoveRateById")]
        public async Task<ActionResult> RemoveRateById(int id)
        {
            var usernameCrr = User.Claims.Where(c => c.Type == ClaimTypes.Name).Select(c => c.Value).SingleOrDefault();
            ServiceResponse<string> response = await _cyberRateUserService.RemoveRateById(usernameCrr, id);
            if (response.Success != true)
            {
                return NotFound(response);
            }
            return Ok(response);
        }
    }
}
