﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CyberBook_API.Models;
using CyberBook_API.Interfaces;
using CyberBook_API.Dal;
using CyberBook_API.Dal.Repositories;
using Microsoft.AspNetCore.Authorization;

namespace CyberBook_API.Controllers
{
    
    [Route("api/[controller]")]
    [ApiController]
    public class CheckAndTestController : ControllerBase
    {
        private readonly IUsersRepository _usersRepository = new UsersRepository();


        [HttpGet("GetUserCommand")]
        public async Task<ActionResult> GetUserCommand(string userIn)
        {
            var user = await _usersRepository.GetUserSQL(userIn);
            return Ok(user);
        }
    }
}
