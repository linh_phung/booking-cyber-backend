﻿using CyberBook_API.Dal.Repositories;
using CyberBook_API.Enum;
using CyberBook_API.Interfaces;
using CyberBook_API.Models;
using CyberBook_API.Services;
using CyberBook_API.Services.Interfaces;
using CyberBook_API.ViewModel.AccountView;
using CyberBook_API.ViewModel.PagingView;
using CyberBook_API.ViewModel.RateCybersViewModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace CyberBook_API.Controllers
{
    [Route("api/[controller]")]
    //[Authorize]
    [ApiController]
    public class UserRateCyberController : ControllerBase
    {
        private readonly IUserRateCyberService _userRateCyberService = new UserRateCyberService();

        /// <summary>
        ///  User đánh giá một Cyber
        ///  huynhnd53
        /// </summary>
        /// <param name="rateCyber"></param>
        /// <returns></returns>
        [HttpPost("AddUserRateCyber")]
        public async Task<ActionResult> AddUserRateCyber([FromBody] RatingCyber rateCyber)
        {
            var usernameCrr = User.Claims.Where(c => c.Type == ClaimTypes.Name).Select(c => c.Value).SingleOrDefault();
            ServiceResponse<RateCyberOutViewModel> response = await _userRateCyberService.AddUserRateCyber(usernameCrr, rateCyber);
            if (response.Success != true)
            {
                return NotFound(response);
            }
            return Ok(response);
        }

        /// <summary>
        /// Update bản ghi User đánh giá Cyber
        /// huynhnd53
        /// </summary>
        /// <param name="ratingUser"></param>
        /// <returns></returns>
        [HttpPatch("UpdateRateCyber")]
        public async Task<ActionResult> UpdateRateCyber([FromBody] RatingCyber rateCyber)
        {
            var usernameCrr = User.Claims.Where(c => c.Type == ClaimTypes.Name).Select(c => c.Value).SingleOrDefault();
            ServiceResponse<RateCyberOutViewModel> response = await _userRateCyberService.UpdateRateCyber(usernameCrr, rateCyber);
            if (response.Success != true)
            {
                return NotFound(response);
            }
            return Ok(response);
        }

        /// <summary>
        /// Xóa một bản người dùng đánh giá Cyber
        /// huynhnd53
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("RemoveRateById")]
        public async Task<ActionResult> RemoveRateById(int id)
        {
            var usernameCrr = User.Claims.Where(c => c.Type == ClaimTypes.Name).Select(c => c.Value).SingleOrDefault();
            ServiceResponse<string> response = await _userRateCyberService.RemoveRateById(usernameCrr, id);
            if (response.Success != true)
            {
                return NotFound(response);
            }
            return Ok(response);
        }

        [HttpGet("GetRateCyberById")]
        public async Task<ActionResult> GetRateCyberById(int id)
        {
            ServiceResponse<RateCyberOutViewModel> response = await _userRateCyberService.GetRateCyberById(id);
            if (response.Success != true)
            {
                return NotFound(response);
            }
            return Ok(response);
        }

        [HttpGet("GetAllRateByCyberId")]
        public async Task<ActionResult> GetAllRateByCyberId(int pageIndex, int pageSize, int cyberId)
        {
            PagingOutput<IEnumerable<RateCyberOutViewModel>> paging = await _userRateCyberService.GetAllRateByCyberId(pageIndex, pageSize, cyberId);
            return Ok(paging);
        }
    }
}
