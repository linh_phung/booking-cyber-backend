﻿using CyberBook_API.Dal.Repositories;
using CyberBook_API.Enum;
using CyberBook_API.Interfaces;
using CyberBook_API.Models;
using CyberBook_API.Services;
using CyberBook_API.Services.Interfaces;
using CyberBook_API.ViewModel.OrderViewModel;
using CyberBook_API.ViewModel.PagingView;
using CyberBook_API.ViewModel.SlotViewModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace CyberBook_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ManageBookingController : ControllerBase
    {
        private readonly IManageBookingService _manageBookingService = new ManageBookingService();

        /// <summary>
        /// Lấy danh sách Order của một Cyber bằng ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("GetListBookingByCyberId")]
        public async Task<ActionResult> GetListBookingByCyberId(int pageIndex, int pageSize, int cyberId)
        {
            var usernameCrr = User.Claims.Where(c => c.Type == ClaimTypes.Name).Select(c => c.Value).SingleOrDefault();
            PagingOutput<IEnumerable<OrderViewModelOut>> response = await _manageBookingService.GetListBookingByCyberId(usernameCrr, pageIndex, pageSize, cyberId);
            return Ok(response);
        }

        /// <summary>
        /// Lấy danh sách Order của một User bằng ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("GetMyListBooking")]
        public async Task<ActionResult> GetMyListBooking(int pageIndex, int pageSize)
        {
            var usernameCrr = User.Claims.Where(c => c.Type == ClaimTypes.Name).Select(c => c.Value).SingleOrDefault();
            PagingOutput<IEnumerable<OrderViewModelOut>> response = await _manageBookingService.GetMyListBooking(usernameCrr, pageIndex, pageSize);
            return Ok(response);
        }

        /// <summary>
        /// lấy danh sách của một Order bằng ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("GetBookingById")]
        public async Task<ActionResult> GetBookingById(int orderId)
        {
            var usernameCrr = User.Claims.Where(c => c.Type == ClaimTypes.Name).Select(c => c.Value).SingleOrDefault();
            ServiceResponse<OrderViewModelOut> response = await _manageBookingService.GetBookingById(usernameCrr, orderId);
            if (response.Success != true)
            {
                return NotFound(response);
            }
            return Ok(response);
        }

        /// <summary>
        /// tạo mới một order
        /// </summary>
        /// <param name="orderViewModelIn"></param>
        /// <returns></returns>
        //[Authorize]
        [HttpPost("CreateNewOrder")]
        public async Task<ActionResult> CreateNewOrder([FromBody] OrderViewModelIn orderViewModelIn)
        {
            var usernameCrr = User.Claims.Where(c => c.Type == ClaimTypes.Name).Select(c => c.Value).SingleOrDefault();
            ServiceResponse<OrderViewModelOut> response = await _manageBookingService.CreateNewOrder(usernameCrr, orderViewModelIn);
            if (response.Success != true)
            {
                return NotFound(response);
            }
            return Ok(response);
        }

        //[Authorize]
        [HttpPatch("ChangeStatusOrder")]
        public async Task<ActionResult> ChangeStatusOrder(int orderId, int statusOrder)
        {
            var usernameCrr = User.Claims.Where(c => c.Type == ClaimTypes.Name).Select(c => c.Value).SingleOrDefault();
            ServiceResponse<OrderViewModelOut> response = await _manageBookingService.ChangeStatusOrder(usernameCrr, orderId, statusOrder);
            if (response.Success != true)
            {
                return NotFound(response);
            }
            return Ok(response);
        }

        [HttpGet("FilterOrderManager")]
        public async Task<ActionResult> FilterOrderManager(int orderStatus, string phoneNumber, int roomId, int cyberId, int pageIndex, int pageSize)
        {
            var usernameCrr = User.Claims.Where(c => c.Type == ClaimTypes.Name).Select(c => c.Value).SingleOrDefault();
            PagingOutput<IEnumerable<OrderViewModelOut>> response = await _manageBookingService.FilterOrderManager(usernameCrr, orderStatus, phoneNumber, roomId, cyberId, pageIndex, pageSize);
            return Ok(response);
        }

        [HttpGet("GetAllCyberBookedByUserId")]
        public async Task<ActionResult> GetAllCyberBookedByUserId()
        {
            var usernameCrr = User.Claims.Where(c => c.Type == ClaimTypes.Name).Select(c => c.Value).SingleOrDefault();
            ServiceResponse<IEnumerable<Cyber>> response = await _manageBookingService.GetAllCyberBookedByUserId(usernameCrr);
            return Ok(response);
        }

        [HttpGet("CheckSplit")]
        public async Task<ActionResult> CheckSplit(int orderId)
        {
            ServiceResponse<IEnumerable<Slot>> response = await _manageBookingService.CheckSplitSlotInOrder(orderId);
            return Ok(response);
        }
    }
}
