﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CyberBook_API.ViewModel.ConfigHardwareViewModel
{
    public class SlotHardwareConfigOutViewModel
    {
        public int Id { get; set; }
        public string Monitor { get; set; }
        public string Gpu { get; set; }
        public string Cpu { get; set; }
        public string Ram { get; set; }
        public string CreatedDate { get; set; }
        public string UpdateDate { get; set; }
        public string NameHardware { get; set; }
        public int CyberID { get; set; }
    }
}
