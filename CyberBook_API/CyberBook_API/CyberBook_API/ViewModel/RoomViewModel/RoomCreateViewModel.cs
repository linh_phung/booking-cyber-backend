﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CyberBook_API.ViewModel.RoomViewModel
{
    public class RoomCreateViewModel
    {
        public int? CyberId { get; set; }
        public string RoomName { get; set; }
        public string RoomTypeName { get; set; }
        public string RoomPosition { get; set; }
        public int MaxX { get; set; }
        public int MaxY { get; set; }
        public double PriceRoom { get; set; }
        public int HardwareConfigId { get; set; }
        public string RoomImage { get; set; }
    }
}
