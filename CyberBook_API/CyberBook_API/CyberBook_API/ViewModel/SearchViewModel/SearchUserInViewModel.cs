﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CyberBook_API.ViewModel.SearchViewModel
{
    public class SearchUserInViewModel
    {
        public string Username { get; set; }
    }
}
