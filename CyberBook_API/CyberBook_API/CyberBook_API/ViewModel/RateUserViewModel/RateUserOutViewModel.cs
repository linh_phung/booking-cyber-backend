﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CyberBook_API.ViewModel.RateUserViewModel
{
    public class RateUserOutViewModel
    {
        public int RateId { get; set; }
        public int? CyberId { get; set; }
        public double RatePoint { get; set; }
        public string CommentContent { get; set; }
        public int? UsersId { get; set; }
        public string CreatedDate { get; set; }
        public string UpdatedDate { get; set; }
        public bool? Edited { get; set; }
        public int? OrderId { get; set; }
    }
}
