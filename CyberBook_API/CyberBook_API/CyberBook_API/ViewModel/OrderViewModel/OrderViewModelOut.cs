﻿using CyberBook_API.Models;
using CyberBook_API.ViewModel.RateUserViewModel;
using CyberBook_API.ViewModel.RateCybersViewModel;
using CyberBook_API.ViewModel.SlotViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CyberBook_API.ViewModel.OrderViewModel
{
    public class OrderViewModelOut
    {
        public int OrderId { get; set; }
        public string StartAt { get; set; }
        public string EndAt { get; set; }
        public string CreatedDate { get; set; }
        public int? CreatedBy { get; set; }
        public int? StatusOrder { get; set; }
        public int CyberId { get; set; }
        public string CyberName { get; set; }
        public string RoomName { get; set; }
        public string EmailUser { get; set; }
        public string PhoneUser { get; set; }
        public string StatusOrderName { get; set; }
        public string ListSlotName { get; set; }
        public RateUserOutViewModel RateUser { get; set; }
        public RateCyberOutViewModel RateCyber { get; set; }
        public List<SlotsOrderViewModelOut> ListSlotsOrder { get; set; }
    }
}
