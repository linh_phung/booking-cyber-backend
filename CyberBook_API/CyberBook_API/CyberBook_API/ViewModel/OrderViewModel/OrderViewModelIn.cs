﻿using CyberBook_API.Models;
using CyberBook_API.ViewModel.SlotViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CyberBook_API.ViewModel.OrderViewModel
{
    public class OrderViewModelIn
    {
        public DateTime StartAt { get; set; }
        public DateTime EndAt { get; set; }
        public int CyberId { get; set; }
        public List<SlotsOrderViewModelIn> ListSlotsOrder { get; set; }
    }
}
