﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CyberBook_API.ViewModel.SlotViewModel
{
    public class SlotsFilterInViewModel
    {
        public string RoomId { get; set; }
        public string StatusId { get; set; }
        public string ConfigId { get; set; }
    }
}
