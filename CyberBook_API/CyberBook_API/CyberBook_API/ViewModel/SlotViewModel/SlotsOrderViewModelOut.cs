﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CyberBook_API.ViewModel.SlotViewModel
{
    public class SlotsOrderViewModelOut
    {
        public int SlotId { get; set; }
        public int? RoomId { get; set; }
        public string SlotName { get; set; }
        public int? SlotHardwareConfigId { get; set; }
        public string SlotHardwareName { get; set; }
        public int? StatusId { get; set; }
        public string StatusName { get; set; }
        public string SlotDescription { get; set; }
        public int? SlotPositionX { get; set; }
        public int? SlotPositionY { get; set; }
    }
}
