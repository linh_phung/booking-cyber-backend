﻿using CyberBook_API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CyberBook_API.ViewModel.TokenViewModel
{
    public class RefreshTokenViewModel
    {
        public int TokenId { get; set; }
        public int UserId { get; set; }
        public string Token { get; set; }
        public DateTime ExpriryDate { get; set; }
        public Users User { get; set; }
    }
}
