﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CyberBook_API.ViewModel.TokenViewModel
{
    public class RefreshRequestViewModel
    {
        public string Token { get; set; }
        public string RefreshToken { get; set; }
    }
}
