﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CyberBook_API.ViewModel.CyberViewModel
{
    public class CyberEditViewModel
    {
        public int id { get; set; }
        public string CyberName { get; set; }
        public string Address { get; set; }
        public string PhoneNumber { get; set; }
        public string CyberDescription { get; set; }
        public string lat { get; set; }
        public string lng { get; set; }
        public string image { get; set; }
        public string BusinessLicense { get; set; }
        public string CoverImage { get; set; }
    }
}
