﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CyberBook_API.ViewModel.CyberViewModel
{
    public class CyberSearchNameInViewModel
    {
        public string CyberName { get; set; }
    }
}
