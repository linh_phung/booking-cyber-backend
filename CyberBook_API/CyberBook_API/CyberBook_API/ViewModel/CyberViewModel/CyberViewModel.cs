﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CyberBook_API.ViewModel.CyberViewModel
{
    public class CyberViewModel
    {
        public string CyberName { get; set; }
        public string Address { get; set; }
        public string PhoneNumber { get; set; }
        public string BossCyberName { get; set; }
        public string lat { get; set; }
        public string lng { get; set; }
        public string BusinessLicense { get; set; }
        public string CreatedDate { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public DateTime Dob { get; set; }
    }
}
