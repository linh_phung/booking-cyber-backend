﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CyberBook_API.ViewModel.CyberViewModel
{
    public class CyberViewModelOut
    {
        public int CyberId { get; set; }
        public string CyberName { get; set; }
        public string Address { get; set; }
        public string PhoneNumber { get; set; }
        public string CyberDescription { get; set; }
        public double RatingPoint { get; set; }
        public string BossCyberName { get; set; }
        public int BossCyberID { get; set; }
        public string lat { get; set; }
        public string lng { get; set; }
        public int status { get; set; }
        public string image { get; set; }
        public string BusinessLicense { get; set; }
        public string CreatedDate { get; set; }
        public int SlotReady { get; set; }
        public DateTime Dob { get; set; }
        public string Email { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public int AccountId { get; set; }
        public string CoverImage { get; set; }
    }
}
