﻿using CyberBook_API.Interfaces;
using CyberBook_API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CyberBook_API.Dal.Repositories
{
    public class StatusOrderRepository : GenericRepository<StatusOrder>, IStatusOrderRepository
    {
    }
}
