﻿using CyberBook_API.Enum;
using CyberBook_API.Interfaces;
using CyberBook_API.Models;
using CyberBook_API.ViewModel.PagingView;
using Microsoft.Graph;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;

namespace CyberBook_API.Dal.Repositories
{
    public class AdminsRepository : GenericRepository<Models.Users>, IAdminsRepository
    {
        private readonly ICybersRepository _cybersRepository = new CybersRepository();
        private readonly IUsersRepository _usersRepository = new UsersRepository();
        private readonly IAccountsRepository _accountsRepository = new AccountsRepository();

        public async Task SendMail(string messageBody, string mailTo, string title)
        {
            MailMessage message = new MailMessage("wegocyber@gmail.com", mailTo, title, "Xin chào");
            message.Body = messageBody;
            message.BodyEncoding = System.Text.Encoding.UTF8;
            message.SubjectEncoding = System.Text.Encoding.UTF8;
            message.IsBodyHtml = true;

            message.ReplyToList.Add(new MailAddress("wegocyber@gmail.com"));
            message.Sender = new MailAddress("wegocyber@gmail.com");

            using var smtpClient = new SmtpClient("smtp.gmail.com");
            smtpClient.Port = 587;
            smtpClient.EnableSsl = true;
            smtpClient.Credentials = new NetworkCredential("wegocyber@gmail.com", "gocyber123");

            await smtpClient.SendMailAsync(message);
        }

        public async Task<Models.Users> AdminLockUserById(int userId)
        {
            var users = (await FindBy(x => x.Id == userId)).FirstOrDefault();
            if (users != null)
            {
                users.Status = Convert.ToInt32(UsersEnum.UserStatus.Block);
                if ((await Update(users, users.Id)) != -1)
                {
                    return users;
                }
            }
            return null;
        }

        public async Task<Models.Users> AdminUnLockUserById(int userId)
        {
            var users = (await FindBy(x => x.Id == userId)).FirstOrDefault();
            if (users != null)
            {
                users.Status = Convert.ToInt32(UsersEnum.UserStatus.Active);
                if ((await Update(users, users.Id)) != -1)
                {
                    return users;
                }
            }
            return null;
        }


        public async Task<PagingOutput<IEnumerable<Cyber>>> GetListCyberByStatusId(int statusCyber, int indexPage, int pageSize)
        {
            var lstCybers = (await _cybersRepository.FindBy(x => x.status == statusCyber)).ToList();
            if (lstCybers.Count > 0)
            {
                var cybers = from c in lstCybers
                             orderby c.CreatedDate descending
                             select c;
                var data = cybers.Skip((indexPage - 1) * pageSize)
                    .Take(pageSize)
                    .Select(x => new Cyber()
                    {
                        Id = x.Id,
                        Address = x.Address,
                        BossCyberID = x.BossCyberID,
                        CreatedDate = x.CreatedDate,
                        BossCyberName = x.BossCyberName,
                        BusinessLicense = x.BusinessLicense,
                        CyberDescription = x.CyberDescription,
                        CyberName = x.CyberName,
                        image = x.image,
                        lat = x.lat,
                        lng = x.lng,
                        PhoneNumber = x.PhoneNumber,
                        RatingPoint = x.RatingPoint,
                        status = x.status,
                        CoverImage = x.CoverImage
                    }).ToList();
                int totalPage = lstCybers.Count / pageSize;
                if (lstCybers.Count % pageSize > 0)
                {
                    totalPage = (lstCybers.Count / pageSize) + 1;
                }

                var paging = new PagingOutput<IEnumerable<Cyber>>
                {
                    Index = indexPage,
                    PageSize = pageSize,
                    TotalItem = lstCybers.Count,
                    TotalPage = totalPage,
                    Data = data
                };
                return paging;
            }
            return null;
        }

        public async Task<PagingOutput<IEnumerable<Cyber>>> GetListCyberRegisterPending(int indexPage, int pageSize)
        {
            var lstCybers = (await _cybersRepository.FindBy(x => x.status == Convert.ToInt32(CybersEnum.CybersStatus.Pending))).ToList();
            if (lstCybers.Count > 0)
            {
                var cybers = from c in lstCybers
                             orderby c.CreatedDate descending
                             select c;
                var data = cybers.Skip((indexPage - 1) * pageSize)
                    .Take(pageSize)
                    .Select(x => new Cyber()
                    {
                        Id = x.Id,
                        Address = x.Address,
                        BossCyberID = x.BossCyberID,
                        CreatedDate = x.CreatedDate,
                        BossCyberName = x.BossCyberName,
                        BusinessLicense = x.BusinessLicense,
                        CyberDescription = x.CyberDescription,
                        CyberName = x.CyberName,
                        image = x.image,
                        lat = x.lat,
                        lng = x.lng,
                        PhoneNumber = x.PhoneNumber,
                        RatingPoint = x.RatingPoint,
                        status = x.status,
                        CoverImage = x.CoverImage
                    }).ToList();
                int totalPage = lstCybers.Count / pageSize;
                if (lstCybers.Count % pageSize > 0)
                {
                    totalPage = (lstCybers.Count / pageSize) + 1;
                }

                var paging = new PagingOutput<IEnumerable<Cyber>>
                {
                    Index = indexPage,
                    PageSize = pageSize,
                    TotalItem = lstCybers.Count,
                    TotalPage = totalPage,
                    Data = data
                };
                return paging;
            }
            return null;
        }

        public async Task<bool> IsSuperAdmin(int userId)
        {
            var user = (await FindBy(x => x.Id == userId && x.RoleId == Convert.ToInt32(UsersEnum.UserRole.Admin)));
            if (user != null)
            {
                return true;
            }
            return false;
        }

        public async Task<bool> RejectRegisterCyber(int cyberId)
        {
            var cyber = await _cybersRepository.GetCyberById(cyberId);
            var bossCyber = await _usersRepository.GetUserByUserID(cyber.BossCyberID);
            var accountCyber = await _accountsRepository.GetAccountByID(bossCyber.AccountID);
            cyber.status = Convert.ToInt32(CybersEnum.CybersStatus.Reject);
            if ((await _cybersRepository.Update(cyber, cyber.Id)) != -1
                && (await _usersRepository.Delete(bossCyber)) != -1
                && (await _accountsRepository.Delete(accountCyber)) != -1
                )
            {
                return true;
            }
            return false;
        }
    }
}
