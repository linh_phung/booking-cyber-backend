﻿using CyberBook_API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CyberBook_API.Interfaces;
using CyberBook_API.ViewModel.PagingView;
using System.Text.RegularExpressions;
using System.Collections;
using CyberBook_API.Enum;

namespace CyberBook_API.Dal.Repositories
{
    public class UsersRepository : GenericRepository<Users>, IUsersRepository
    {
        public async Task<bool> CheckComfirmPass(int userId, string code)
        {
            var u = (await FindBy(x => x.Id == userId)).FirstOrDefault();
            if (u != null)
            {
                if (u.ComfirmPassword == code)
                {
                    return true;
                }
            }
            return false;
        }

        public async Task<Users> GetUserByAccountID(int accountId)
        {
            return (await FindBy(x => x.AccountID == accountId)).FirstOrDefault();
        }

        public async Task<Users> GetUserByEmail(string email)
        {
            return (await FindBy(x => x.Email.Equals(email))).FirstOrDefault();
        }

        public async Task<Users> GetUserByUserID(int? userId)
        {
            return (await FindBy(x => x.Id == userId)).FirstOrDefault();
        }


        public bool IsValidEmail(string email)
        {
            if (email.Trim().EndsWith("."))
            {
                return false; // suggested by @TK-421
            }
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }

        public bool IsValidPhoneNumber(string phonenumber)
        {
            if (Regex.Match(phonenumber, @"^(\+[0-9])$").Success)
                return true;
            else
                return false;
        }

        public async Task<PagingOutput<IEnumerable<Users>>> SearchAllUser(int indexPage, int pageSize)
        {
            var lstUser = (await FindBy(x => x.RoleId == Convert.ToInt32(UsersEnum.UserRole.UserNormal))).ToList();
            if (lstUser.Count > 0)
            {
                var users = from c in lstUser
                            orderby c.Fullname descending
                            select c;
                var data = users.Skip((indexPage - 1) * pageSize)
                    .Take(pageSize)
                    .Select(x => new Users()
                    {
                        Id = x.Id,
                        AccountID = x.AccountID,
                        Address = x.Address,
                        Bio = x.Bio,
                        Dob = x.Dob,
                        Email = x.Email,
                        Fullname = x.Fullname,
                        Image = x.Image,
                        PhoneNumber = x.PhoneNumber,
                        RatingPoint = x.RatingPoint,
                        RoleId = x.RoleId,
                        Status = x.Status
                    }).ToList();
                int totalPage = lstUser.Count / pageSize;
                if (lstUser.Count % pageSize > 0)
                {
                    totalPage = (lstUser.Count / pageSize) + 1;
                }

                var paging = new PagingOutput<IEnumerable<Users>>
                {
                    Index = indexPage,
                    PageSize = pageSize,
                    TotalItem = lstUser.Count,
                    TotalPage = totalPage,
                    Data = data
                };
                return paging;
            }
            return null;
        }

        public async Task<PagingOutput<IEnumerable<Users>>> SearchAllUserByUsername(string username, int indexPage, int pageSize)
        {
            var lstUser = new List<Users>();
            if (string.IsNullOrWhiteSpace(username))
            {
                lstUser = (await FindBy(x => x.RoleId == Convert.ToInt32(UsersEnum.UserRole.UserNormal))).ToList();
            }
            else
            {
                lstUser = (await FindBy(x => (x.Fullname.Contains(username) || x.Email.Contains(username))
                && x.RoleId == Convert.ToInt32(UsersEnum.UserRole.UserNormal))).ToList();
            }

            if (lstUser.Count > 0)
            {
                var users = from c in lstUser
                            orderby c.Fullname descending
                            select c;
                var data = users.Skip((indexPage - 1) * pageSize)
                    .Take(pageSize)
                    .Select(x => new Users()
                    {
                        Id = x.Id,
                        AccountID = x.AccountID,
                        Address = x.Address,
                        Bio = x.Bio,
                        Dob = x.Dob,
                        Email = x.Email,
                        Fullname = x.Fullname,
                        Image = x.Image,
                        PhoneNumber = x.PhoneNumber,
                        RatingPoint = x.RatingPoint,
                        RoleId = x.RoleId,
                        Status = x.Status
                    }).ToList();
                int totalPage = lstUser.Count / pageSize;
                if (lstUser.Count % pageSize > 0)
                {
                    totalPage = (lstUser.Count / pageSize) + 1;
                }

                var paging = new PagingOutput<IEnumerable<Users>>
                {
                    Index = indexPage,
                    PageSize = pageSize,
                    TotalItem = lstUser.Count,
                    TotalPage = totalPage,
                    Data = data
                };
                return paging;
            }
            return null;
        }

        public async Task<IEnumerable<Users>> GetUserSQL(string strUserss)
        {
            string[] param = new String[] { strUserss };

            var sql = $"EXEC GetUserByUsernameChecker @username = { Convert.ToString(param[0]) }";
            var user = (await SQLCommand(sql, param)).ToList();
            return user;
        }

    }
}
