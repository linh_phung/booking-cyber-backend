﻿using CyberBook_API.Dal.Repositories;
using CyberBook_API.Models;
using CyberBook_API.Services;
using CyberBook_API.ViewModel.CyberViewModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;

namespace CyberBook_Test
{
    [TestClass]
    public class AdminUsersService_Test
    {
        private readonly AccountsRepository _accountsRepository = new AccountsRepository();
        private readonly UsersRepository _usersRepository = new UsersRepository();

        [Fact]
        public void ListUser_CheckLogin_NotLogin()
        {
            // Arrange
            var adminUserService = new AdminUsersService();
            string usernameCrr = "linhphung12";
            int pageIndex = 1;
            int pageSize = 10;

            // Act
            var result = adminUserService.ListUser(usernameCrr, pageIndex, pageSize);

            // Assert
            Assert.IsNull(result.Result.Data);
        }

        [Fact]
        public void ListUser_CheckLogin_LoginSuccess()
        {
            // Arrange
            var adminUserService = new AdminUsersService();
            string usernameCrr = "linhphung";
            int pageIndex = 1;
            int pageSize = 10;

            // Act
            var result = adminUserService.ListUser(usernameCrr, pageIndex, pageSize);

            // Assert
            Assert.IsNotNull(result.Result.Data);
        }

        [Fact]
        public void SearchAllUser_ListUserHasUsername_ReturnList()
        {
            // Arrange
            int pageIndex = 1;
            int pageSize = 10;

            // Act
            var result = _usersRepository.SearchAllUser(pageIndex, pageSize);

            // Assert
            Assert.IsNotNull(result.Result.Data);
        }

        [Fact]
        public void GetUserByAccountID_AccountExist_ReturnUser()
        {
            // Arrange
            var accountId = 1;

            // Act
            var result = _usersRepository.GetUserByAccountID(accountId);

            // Assert
            Assert.IsNotNull(result);
        }

        [Fact]
        public void LockUserById_BlockSuccess_ReturnSuccess()
        {
            // Arrange
            var adminUserService = new AdminUsersService();
            string usernameCrr = "linhphung";
            int userId = 10;

            // Act
            var result = adminUserService.LockUserById(usernameCrr, userId);

            // Assert
            Assert.AreEqual(result.Result.Message.ToString(), "Block Successfull");
        }

        [Fact]
        public void LockUserById_UserNotExist_ReturnNull()
        {
            // Arrange
            var adminUserService = new AdminUsersService();
            string usernameCrr = "linhphung";
            int userId = 1000;

            // Act
            var result = adminUserService.LockUserById(usernameCrr, userId);

            // Assert
            Assert.AreEqual(result.Result.Message.ToString(), "Block FAIL");
        }

        [Fact]
        public void SearchUserByName_SearchUserPaging_ReturnSuccess()
        {
            // Arrange
            var adminUserService = new AdminUsersService();
            string usernameCrr = "linhphung";
            int pageIndex = 1;
            int pageSize = 10;
            string username = "Nam Trung";

            // Act
            var result = adminUserService.SearchUserByName(usernameCrr, pageIndex, pageSize, username);

            // Assert
            Assert.AreEqual(result.Result.Message.ToString(), "Success");
        }

        [Fact]
        public void SearchUserByName_NotFindUser_ReturnNull()
        {
            // Arrange
            var adminUserService = new AdminUsersService();
            string usernameCrr = "linhphung";
            int pageIndex = 1;
            int pageSize = 10;
            string username = "Nam123";

            // Act
            var result = adminUserService.SearchUserByName(usernameCrr, pageIndex, pageSize, username);

            // Assert
            Assert.AreEqual(result.Result.Message.ToString(), "No user");
        }

        [Fact]
        public void ViewDetailUser_CheckLoginWithWrongUsername_NotLogin()
        {
            // Arrange
            var adminUserService = new AdminUsersService();
            string usernameCrr = "linhphung123";
            int userId = 10;

            // Act
            var result = adminUserService.ViewDetailUser(usernameCrr, userId);

            // Assert
            Assert.AreEqual(result.Result.Message.ToString(), "YOU NOT LOGIN");
        }

        [Fact]
        public void ViewDetailUser_ShowDetailCyberSuccess_ReturnCyber()
        {
            // Arrange
            var adminUserService = new AdminUsersService();
            string usernameCrr = "linhphung";
            int userId = 10;

            // Act
            var result = adminUserService.ViewDetailUser(usernameCrr, userId);

            // Assert
            Assert.AreEqual(result.Result.Message.ToString(), "Success");
        }

        [Fact]
        public void ViewDetailUser_CannotFindCyber_ReturnNull()
        {
            // Arrange
            var adminUserService = new AdminUsersService();
            string usernameCrr = "linhphung";
            int userId = 1000;

            // Act
            var result = adminUserService.ViewDetailUser(usernameCrr, userId);

            // Assert
            Assert.AreEqual(result.Result.Message.ToString(), "can not find");
        }
    }
}

