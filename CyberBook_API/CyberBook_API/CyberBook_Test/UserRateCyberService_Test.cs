﻿using CyberBook_API.Models;
using CyberBook_API.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;

namespace CyberBook_Test
{
    [TestClass]
    public class UserRateCyberService_Test
    {
        [Fact]
        public void AddUserRateCyber_IfNotLogin_ReturnNull()
        {
            // Arrange
            var userRateCyberService = new UserRateCyberService();
            string usernameCrr = "dungba123";
            var rateCyber = new RatingCyber
            {

            };

            // Act
            var result = userRateCyberService.AddUserRateCyber(usernameCrr, rateCyber);

            // Assert
            Assert.AreEqual(result.Result.Message.ToString(), "You NOT LOGIN");
        }

        [Fact]
        public void AddUserRateCyber_IfCyberNotExist_ReturnNull()
        {
            // Arrange
            var userRateCyberService = new UserRateCyberService();
            string usernameCrr = "dungba";
            var rateCyber = new RatingCyber
            {
                Id = 1,
                UserId = 1,
                CyberId = 1,
                OrderId = 1,
                CommentContent = "zzzzzzzaaaaaaaa",
                Edited = true,
                CreatedDate = DateTime.Now,
                RatePoint = 4,
                UpdatedDate = DateTime.Now
            };

            // Act
            var result = userRateCyberService.AddUserRateCyber(usernameCrr, rateCyber);

            // Assert
            Assert.AreEqual("Cyber NOT exist", result.Result.Message.ToString());
        }

        [Fact]
        public void AddUserRateCyber_IfCancelOrCannotRateCyber_ReturnNull()
        {
            // Arrange
            var userRateCyberService = new UserRateCyberService();
            string usernameCrr = "dungba";
            var rateCyber = new RatingCyber
            {
                Id = 10,
                UserId = 3,
                CyberId = 2,
                OrderId = 1013,
                CommentContent = "zzzzzzzaaaaaaaa",
                Edited = true,
                CreatedDate = DateTime.Now,
                RatePoint = 4,
                UpdatedDate = DateTime.Now
            };

            // Act
            var result = userRateCyberService.AddUserRateCyber(usernameCrr, rateCyber);

            // Assert
            Assert.AreEqual("if you CANCEL you CANNOT rate cyber", result.Result.Message.ToString());
        }

        [Fact]
        public void GetAllRateByCyberId_IfNoRecordavaiable_ReturnNull()
        {
            // Arrange
            var userRateCyberService = new UserRateCyberService();
            int pageIndex = 1;
            int pageSize = 99;
            int cyberId = 50;

            // Act
            var result = userRateCyberService.GetAllRateByCyberId(pageIndex, pageSize, cyberId);

            // Assert
            Assert.AreEqual("No Record avaiable", result.Result.Message.ToString());
            Assert.IsNull(result.Result.Data);
        }

        [Fact]
        public void GetAllRateByCyberId_IfGetRateCyberSuccess_ReturnRateCyber()
        {
            // Arrange
            var userRateCyberService = new UserRateCyberService();
            int pageIndex = 1;
            int pageSize = 99;
            int cyberId = 2;

            // Act
            var result = userRateCyberService.GetAllRateByCyberId(pageIndex, pageSize, cyberId);

            // Assert
            Assert.AreEqual("get RateCyber Record Successfull", result.Result.Message.ToString());
            Assert.IsNotNull(result.Result.Data);
        }

        [Fact]
        public void GetAllRateByCyberId_IfGetRateCyberByIdSuccess_ReturnRateCyber()
        {
            // Arrange
            var userRateCyberService = new UserRateCyberService();
            int cyberId = 2;

            // Act
            var result = userRateCyberService.GetRateCyberById(cyberId);

            // Assert
            Assert.AreEqual("get RateCyber Record Successfull", result.Result.Message.ToString());
        }

        [Fact]
        public void GetAllRateByCyberId_IfGetRateCyberByIdNotExist_ReturnNull()
        {
            // Arrange
            var userRateCyberService = new UserRateCyberService();
            int cyberId = 9999;

            // Act
            var result = userRateCyberService.GetRateCyberById(cyberId);

            // Assert
            Assert.AreEqual("No Record avaiable", result.Result.Message.ToString());
        }

        [Fact]
        public void RemoveRateById_IfNotLogin_ReturnNull()
        {
            // Arrange
            var userRateCyberService = new UserRateCyberService();
            string usernameCrr = "dungba_notExist";
            int id = 9999;

            // Act
            var result = userRateCyberService.RemoveRateById(usernameCrr, id);

            // Assert
            Assert.AreEqual("You NOT LOGIN", result.Result.Message.ToString());
        }

        [Fact]
        public void RemoveRateById_IfNoRecordAvaiable_ReturnNull()
        {
            // Arrange
            var userRateCyberService = new UserRateCyberService();
            string usernameCrr = "dungba";
            int id = 9999;

            // Act
            var result = userRateCyberService.RemoveRateById(usernameCrr, id);

            // Assert
            Assert.AreEqual("No Record avaiable", result.Result.Message.ToString());
        }

        [Fact]
        public void UpdateRateCyber_IfNotLogin_ReturnNull()
        {
            // Arrange
            var userRateCyberService = new UserRateCyberService();
            string usernameCrr = "dungba_notExist";
            var rateCyber = new RatingCyber
            {
                Id = 1,
                UserId = 1,
                CyberId = 1,
                Edited = true,
                CommentContent = "zzzzzzzzzzzzzzz",
                CreatedDate = DateTime.Now,
                OrderId = 1,
                RatePoint = 3.5,
                UpdatedDate = DateTime.Now
            };

            // Act
            var result = userRateCyberService.UpdateRateCyber(usernameCrr, rateCyber);

            // Assert
            Assert.AreEqual("You NOT LOGIN", result.Result.Message.ToString());
        }

        [Fact]
        public void UpdateRateCyber_IfCyberNotExist_ReturnNull()
        {
            // Arrange
            var userRateCyberService = new UserRateCyberService();
            string usernameCrr = "dungba";
            var rateCyber = new RatingCyber
            {
                Id = 1,
                UserId = 1,
                CyberId = 1,
                Edited = true,
                CommentContent = "zzzzzzzzzzzzzzz",
                CreatedDate = DateTime.Now,
                OrderId = 1,
                RatePoint = 3.5,
                UpdatedDate = DateTime.Now
            };

            // Act
            var result = userRateCyberService.UpdateRateCyber(usernameCrr, rateCyber);

            // Assert
            Assert.AreEqual("Cyber NOT exist", result.Result.Message.ToString());
        }


    }
}
