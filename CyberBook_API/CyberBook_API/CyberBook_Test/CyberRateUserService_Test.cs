﻿using CyberBook_API.Models;
using CyberBook_API.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;

namespace CyberBook_Test
{
    [TestClass]
    public class CyberRateUserService_Test
    {
        [Fact]
        public void Reigster_NotLogin_ReturnNull()
        {
            // Arrange
            var cyberRateUserService = new CyberRateUserService();
            string usernameCrr = "";
            var ratingUser = new RatingUser
            {
                RatePoint = 5
            };

            // Act
            var result = cyberRateUserService.AddNewRateUser(usernameCrr, ratingUser);

            // Assert
            Assert.AreEqual(result.Result.Message.ToString(), "You NOT LOGIN");
        }

        [Fact]
        public void GetCyberById_NotFindCyber_ReturnNull()
        {
            // Arrange
            var cyberRateUserService = new CyberRateUserService();
            int rateId = 1000;

            // Act
            var result = cyberRateUserService.GetRateById(rateId);

            // Assert
            Assert.AreEqual(result.Result.Message.ToString(), "Rating not exist");
        }

        [Fact]
        public void RemoveRateById_NotLogin_ReturnNull()
        {
            // Arrange
            var cyberRateUserService = new CyberRateUserService();
            string username = "llaasd";
            int rateId = 1000;

            // Act
            var result = cyberRateUserService.RemoveRateById(username, rateId);

            // Assert
            Assert.AreEqual(result.Result.Message.ToString(), "You NOT LOGIN");
        }

        [Fact]
        public void UpdateRateUser_NotLogin_ReturnNull()
        {
            // Arrange
            var cyberRateUserService = new CyberRateUserService();
            string username = "llaasd";
            var ratingUser = new RatingUser
            {
                RatePoint = 5
            };

            // Act
            var result = cyberRateUserService.UpdateRateUser(username, ratingUser);

            // Assert
            Assert.AreEqual(result.Result.Message.ToString(), "You NOT LOGIN");
        }
    }
}
