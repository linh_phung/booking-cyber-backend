﻿using CyberBook_API.Models;
using CyberBook_API.Services;
using CyberBook_API.ViewModel.UserViewModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;

namespace CyberBook_Test
{
    [TestClass]
    public class UsersService_Test
    {
        [Fact]
        public void AddNewCyberAccount_IfUserNotExist_ReturnNull()
        {
            // Arrange
            var usersService = new UsersService();
            string usernameCrr = "dungba1zzz";
            var cyberAccount = new CyberAccount
            {
                CyberId = 1
            };

            // Act
            var result = usersService.AddNewCyberAccount(usernameCrr, cyberAccount);

            // Assert
            Assert.AreEqual(result.Result.Message.ToString(), "User NOT exist");
        }

        [Fact]
        public void AddNewCyberAccount_IfCyberNotExist_ReturnNull()
        {
            // Arrange
            var usersService = new UsersService();
            string usernameCrr = "dungba";
            var cyberAccount = new CyberAccount
            {
                CyberId = 100
            };

            // Act
            var result = usersService.AddNewCyberAccount(usernameCrr, cyberAccount);

            // Assert
            Assert.AreEqual(result.Result.Message.ToString(), "Cyber NOT exist");
        }

        [Fact]
        public void AddNewCyberAccount_IfUserNameNull_ReturnNull()
        {
            // Arrange
            var usersService = new UsersService();
            string usernameCrr = "";
            var cyberAccount = new CyberAccount
            {
            };

            // Act
            var result = usersService.AddNewCyberAccount(usernameCrr, cyberAccount);

            // Assert
            Assert.AreEqual(result.Result.Data, null);
        }

        [Fact]
        public void EditCyberAccount_IfUserNotExist_ReturnNull()
        {
            // Arrange
            var usersService = new UsersService();
            string usernameCrr = "dungqasfq";
            var cyberAccount = new CyberAccount
            {
                CyberId = 1
            };

            // Act
            var result = usersService.EditCyberAccount(usernameCrr, cyberAccount);

            // Assert
            Assert.AreEqual(result.Result.Message.ToString(), "User NOT exist");
        }

        [Fact]
        public void EditCyberAccount_IfCyberNotExist_ReturnNull()
        {
            // Arrange
            var usersService = new UsersService();
            string usernameCrr = "dungba";
            var cyberAccount = new CyberAccount
            {
                CyberId = 2233, // cyber not exist in db
                UserId = 1,
                CyberName = "aaaaaaaaaaa",
                ID = 1,
                Password = "123",
                PhoneNumber = "01111111111",
                Username = "dungba1"
            };

            // Act
            var result = usersService.EditCyberAccount(usernameCrr, cyberAccount);

            // Assert
            Assert.AreEqual(result.Result.Message.ToString(), "Cyber NOT exist");
        }

        [Fact]
        public void EditCyberAccount_IfCyberAccountNotExist_ReturnNull()
        {
            // Arrange
            var usersService = new UsersService();
            string usernameCrr = "dungba";
            var cyberAccount = new CyberAccount
            {
                CyberId = 2, // cyber not exist in db
                UserId = 1,
                CyberName = "aaaaaaaaaaa",
                ID = 1,
                Password = "123",
                PhoneNumber = "01111111111",
                Username = "dungba1"
            };

            // Act
            var result = usersService.EditCyberAccount(usernameCrr, cyberAccount);

            // Assert
            Assert.AreEqual(result.Result.Message.ToString(), "CyberAccount NOT exist");
        }

        [Fact]
        public void EditCyberAccount_IfUsernameNotExist_ReturnNull()
        {
            var usersService = new UsersService();
            string usernameCrr = "dungba";
            var cyberAccount = new CyberAccount
            {
                CyberId = 2, // cyber not exist in db
                UserId = 1,
                CyberName = "aaaaaaaaaaa",
                ID = 1,
                Password = "",
                PhoneNumber = "01111111111",
                Username = ""
            };

            // Act
            var result = usersService.EditCyberAccount(usernameCrr, cyberAccount);

            // Assert
            Assert.AreEqual(result.Result.Message.ToString(), "username or password NOT allow Empty");
        }

        [Fact]
        public void EditCyberAccount_IfEditSuccess_ReturnCyber()
        {
            var usersService = new UsersService();
            string usernameCrr = "dungba";
            var cyberAccount = new CyberAccount
            {
                CyberId = 2, // cyber not exist in db
                UserId = 1,
                CyberName = "aaaaaaaaaaa",
                ID = 1,
                Password = "",
                PhoneNumber = "01111111111",
                Username = ""
            };

            // Act
            var result = usersService.EditCyberAccount(usernameCrr, cyberAccount);

            // Assert
            Assert.AreEqual(result.Result.Message.ToString(), "username or password NOT allow Empty");
        }

        [Fact]
        public void EditUserInfor_IfNotLogin_ReturnNull()
        {
            var usersService = new UsersService();
            string usernameCrr = "dungbazzzzzzzz";
            var userEditViewModel = new UserEditViewModel
            {
                UserId = 100
            };

            // Act
            var result = usersService.EditUserInfor(usernameCrr, userEditViewModel);

            // Assert
            Assert.AreEqual(result.Result.Message.ToString(), "You NOT LOGIN");
        }

        [Fact]
        public void EditUserInfor_IfEditFail_ReturnNull()
        {
            var usersService = new UsersService();
            string usernameCrr = "phunglinh@gm.com";
            var userEditViewModel = new UserEditViewModel
            {
                Fullname = "Phùng Linh",
                UserId = 1117,
                Address = "ooooooooooooo"
            };

            // Act
            var result = usersService.EditUserInfor(usernameCrr, userEditViewModel);

            // Assert
            Assert.AreEqual(result.Result.Message.ToString(), "Update Infor OK");
        }

        [Fact]
        public void GetDetailCyberAccount_IfNotLogin_ReturnNull()
        {
            var usersService = new UsersService();
            string usernameCrr = "phunglinh@gm.com1";
            int cyberAccountId = 1;

            // Act
            var result = usersService.GetDetailCyberAccount(usernameCrr, cyberAccountId);

            // Assert
            Assert.AreEqual(result.Result.Message.ToString(), "You NOT LOGIN");
        }

        [Fact]
        public void GetDetailCyberAccount_IfCanNotFindFind_ReturnNull()
        {
            var usersService = new UsersService();
            string usernameCrr = "phunglinh@gm.com";
            int cyberAccountId = 1001; // cyber not exist in db

            // Act
            var result = usersService.GetDetailCyberAccount(usernameCrr, cyberAccountId);

            // Assert
            Assert.AreEqual(result.Result.Message.ToString(), "CAN NOT find your CyberAccount or You dont have Permission for view this Account Cyber");
        }

        [Fact]
        public void GetDetailCyberAccount_IfFindSuccess_ReturnCyber()
        {
            // Arrange
            var usersService = new UsersService();
            string usernameCrr = "linhphung";
            int cyberAccountId = 1; // cyber not exist in db

            // Act
            var result = usersService.GetDetailCyberAccount(usernameCrr, cyberAccountId);

            // Assert
            Assert.AreEqual(result.Result.Message.ToString(), "Successfully");
        }

        [Fact]
        public void GetMyInfor_IfNotLogin_ReturnNull()
        {
            // Arrange
            var usersService = new UsersService();
            string usernameCrr = "phunglinh@gm.com1";

            // Act
            var result = usersService.GetMyInfor(usernameCrr);

            // Assert
            Assert.AreEqual(result.Result.Message.ToString(), "You NOT LOGIN");
        }

        [Fact]
        public void GetMyInfor_IfGetInforSuccess_ReturnCyber()
        {
            // Arrange
            var usersService = new UsersService();
            string usernameCrr = "phunglinh@gm.com";

            // Act
            var result = usersService.GetMyInfor(usernameCrr);

            // Assert
            Assert.AreEqual(result.Result.Message.ToString(), "Get Infor OK");
        }

        [Fact]
        public void RemoveCyberAccount_IfNotLogin_ReturnNull()
        {
            // Arrange
            var usersService = new UsersService();
            string usernameCrr = "phunglinh@gm.com1";
            int cyberAccountId = 1078;

            // Act
            var result = usersService.RemoveCyberAccount(usernameCrr, cyberAccountId);

            // Assert
            Assert.AreEqual(result.Result.Message.ToString(), "You NOT LOGIN");
        }

        [Fact]
        public void RemoveCyberAccount_IfCyberAccountNotExist_ReturnNull()
        {
            // Arrange
            var usersService = new UsersService();
            string usernameCrr = "phunglinh@gm.com";
            int cyberAccountId = 107811;

            // Act
            var result = usersService.RemoveCyberAccount(usernameCrr, cyberAccountId);

            // Assert
            Assert.AreEqual(result.Result.Message.ToString(), "CyberAccount NOT exist");
        }

        [Fact]
        public void RemoveCyberAccount_IfRemoveCyberAccountSuccess_ReturnTrue()
        {
            // Arrange
            var usersService = new UsersService();
            string usernameCrr = "phunglinh@gm.com";
            int cyberAccountId = 7;

            // Act
            var result = usersService.RemoveCyberAccount(usernameCrr, cyberAccountId);

            // Assert
            Assert.AreEqual("Successfull", result.Result.Message.ToString());
        }

        [Fact]
        public void ViewMyListCyberAccount_IfNotLogin_ReturnNull()
        {
            // Arrange
            var usersService = new UsersService();
            string usernameCrr = "phunglinh@gm.com111";
            int pageIndex = 1;
            int pageSize = 10;

            // Act
            var result = usersService.ViewMyListCyberAccount(usernameCrr, pageIndex, pageSize);

            // Assert
            Assert.AreEqual("You NOT Login", result.Result.Message.ToString());
        }

        [Fact]
        public void ViewMyListCyberAccount_IfNoCyberAccount_ReturnList()
        {
            // Arrange
            var usersService = new UsersService();
            string usernameCrr = "phunglinh@gm.com";
            int pageIndex = 1;
            int pageSize = 10;

            // Act
            var result = usersService.ViewMyListCyberAccount(usernameCrr, pageIndex, pageSize);

            // Assert
            Assert.AreEqual("dont have any cyber account", result.Result.Message.ToString());
        }

        [Fact]
        public void ViewMyListCyberAccount_IfGetSuccess_ReturnList()
        {
            // Arrange
            var usersService = new UsersService();
            string usernameCrr = "linhphung";
            int pageIndex = 1;
            int pageSize = 10;

            // Act
            var result = usersService.ViewMyListCyberAccount(usernameCrr, pageIndex, pageSize);

            // Assert
            Assert.AreEqual("Get your List CyberAccount OK", result.Result.Message.ToString());
            Assert.IsTrue(result.Result.Data.Count() > 0);
        }
    }
}
