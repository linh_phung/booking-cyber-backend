﻿using CyberBook_API.Models;
using CyberBook_API.Services;
using CyberBook_API.ViewModel.ConfigHardwareViewModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;

namespace CyberBook_Test
{
    [TestClass]
    public class HardwareConfigsService_Test
    {
        [Fact]
        public void CreateSlotHardwareConfig_IfCyberNotExist_ReturnNull()
        {
            // Arrange
            var hardwareConfigsService = new HardwareConfigsService();
            string usernameCrr = "dungba1";
            var slotHardwareConfig = new SlotHardwareConfig
            {

            };

            // Act
            var result = hardwareConfigsService.CreateSlotHardwareConfig(usernameCrr, slotHardwareConfig);

            // Assert
            Assert.AreEqual(result.Result.Message.ToString(), "Cyber NOT exist");
        }

        [Fact]
        public void GetHardwaresByCyberId_IfSuccess_ReturnCyber()
        {
            // Arrange
            var hardwareConfigsService = new HardwareConfigsService();
            int cyberId = 2;

            // Act
            var result = hardwareConfigsService.GetHardwaresByCyberId(cyberId);

            // Assert
            Assert.AreEqual(result.Result.Message.ToString(), "Successful");
        }

        [Fact]
        public void GetHardwaresById_IfHardwareNotExist_ReturnNull()
        {
            // Arrange
            var hardwareConfigsService = new HardwareConfigsService();
            int cyberId = 20000;

            // Act
            var result = hardwareConfigsService.GetHardwaresById(cyberId);

            // Assert
            Assert.AreEqual(result.Result.Message.ToString(), "hardware not exist");
        }

        [Fact]
        public void RemoveSlotHardwareConfig_NotLogin_ReturnNull()
        {
            // Arrange
            var hardwareConfigsService = new HardwareConfigsService();
            string usernameCrr = "";
            int hardwareId = 1;

            // Act
            var result = hardwareConfigsService.RemoveSlotHardwareConfig(usernameCrr, hardwareId);

            // Assert
            Assert.AreEqual(result.Result.Message.ToString(), "You NOT LOGIN");
        }

        [Fact]
        public void RemoveSlotHardwareConfig_CyberNotExist_ReturnNull()
        {
            // Arrange
            var hardwareConfigsService = new HardwareConfigsService();
            string usernameCrr = "dungbui@gmail.com";
            int hardwareId = 1;

            // Act
            var result = hardwareConfigsService.RemoveSlotHardwareConfig(usernameCrr, hardwareId);

            // Assert
            Assert.AreEqual(result.Result.Message.ToString(), "You NOT LOGIN");
        }

        [Fact]
        public void UpdateSlotHardwareConfig_NotLogin_ReturnNull()
        {
            // Arrange
            var hardwareConfigsService = new HardwareConfigsService();
            string usernameCrr = "dungbui@gmail.fasdfas";
            var hardwareConfigEditViewModel = new HardwareConfigEditViewModel
            {
                Id = 1,
                Monitor = "ViewSonic XG2705 27 inch FHD 144Hz",
                Gpu = "NVIDIA GeForce RTX 3090",
                Cpu = "AMD Ryzen 9 5950X",
                Ram = "Corsair Vengeance LED",
                NameHardware = "Cấu hình chung",
                CyberID = 1
            };

            // Act
            var result = hardwareConfigsService.UpdateSlotHardwareConfig(usernameCrr, hardwareConfigEditViewModel);

            // Assert
            Assert.AreEqual(result.Result.Message.ToString(), "You NOT LOGIN");
        }

        //[Fact]
        //public void UpdateSlotHardwareConfig()
        //{
        //    // Arrange
        //    var hardwareConfigsService = new HardwareConfigsService();
        //    string usernameCrr = "dungbui@gmail.com";
        //    var slotHardwareConfig = new SlotHardwareConfig
        //    {
        //        Id = 2,
        //        Monitor = "BenQ EX2780Q",
        //        Gpu = "NVIDIA GeForce RTX 3080 Ti",
        //        Cpu = "Intel Core i9-10900K Processor",
        //        Ram = "Corsair Vengeance LED",
        //        NameHardware = "Cấu hình chung",
        //        CyberID = 2
        //    };

        //    // Act
        //    var result = hardwareConfigsService.UpdateSlotHardwareConfig(usernameCrr, slotHardwareConfig);

        //    // Assert
        //    Assert.AreEqual(result.Result.Message.ToString(), "You NOT LOGIN");
        //}
    }
}
