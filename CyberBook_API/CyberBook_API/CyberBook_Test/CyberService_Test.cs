﻿using CyberBook_API.Services;
using CyberBook_API.ViewModel.CyberViewModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;

namespace CyberBook_Test
{
    [TestClass]
    public class CyberService_Test
    {
        [Fact]
        public void EditCyberInfor_NotLogin_ReturnNull()
        {
            // Arrange
            var cybersService = new CybersService();
            string usernameCrr = "";
            var cyberEditViewModel = new CyberEditViewModel
            {
                CyberName = "zagsdfa"
            };

            // Act
            var result = cybersService.EditCyberInfor(usernameCrr, cyberEditViewModel);

            // Assert
            Assert.AreEqual(result.Result.Message.ToString(), "You NOT LOGIN");
        }

        [Fact]
        public void GetCyberById_CyberExist_ReturnCyber()
        {
            // Arrange
            var cybersService = new CybersService();
            int cyberId = 2;

            // Act
            var result = cybersService.GetCyberById(cyberId);

            // Assert
            Assert.AreEqual(result.Result.Message.ToString(), "Success");
        }

        [Fact]
        public void GetCyberById_CyberNotExist_NoCyber()
        {
            // Arrange
            var cybersService = new CybersService();
            int cyberId = 3242;

            // Act
            var result = cybersService.GetCyberById(cyberId);

            // Assert
            Assert.AreEqual(result.Result.Message.ToString(), "No Cyber");
        }

        [Fact]
        public void GetMyCyber_IfCyberNotExist_NotLogin()
        {
            // Arrange
            var cybersService = new CybersService();
            string usernameCrr = "";

            // Act
            var result = cybersService.GetMyCyber(usernameCrr);

            // Assert
            Assert.AreEqual(result.Result.Message.ToString(), "You NOT Login");
        }

        [Fact]
        public void GetMyCyber_GetSucess_ReturnCyber()
        {
            // Arrange
            var cybersService = new CybersService();
            string usernameCrr = "dungba";

            // Act
            var result = cybersService.GetMyCyber(usernameCrr);

            // Assert
            Assert.AreEqual(result.Result.Message.ToString(), "GetMyCyber Successfull");
        }
    }
}
