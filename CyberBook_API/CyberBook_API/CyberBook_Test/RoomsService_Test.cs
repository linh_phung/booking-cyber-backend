﻿using CyberBook_API.Services;
using CyberBook_API.ViewModel.RoomViewModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;

namespace CyberBook_Test
{
    [TestClass]
    public class RoomsService_Test
    {
        [Fact]
        public void CreateNewOrder_IfNotLogin_ReturnNull()
        {
            // Arrange
            var roomsService = new RoomsService();
            string usernameCrr = "dungba1zzz";
            var roomCreateViewModel = new RoomCreateViewModel
            {
                CyberId = 1
            };

            // Act
            var result = roomsService.CreateNewRoom(usernameCrr, roomCreateViewModel);

            // Assert
            Assert.AreEqual(result.Result.Message.ToString(), "YOU NOT LOGIN");
        }

        [Fact]
        public void CreateNewOrder_IfCyberNotExist_ReturnNull()
        {
            // Arrange
            var roomsService = new RoomsService();
            string usernameCrr = "dungba1";
            var roomCreateViewModel = new RoomCreateViewModel
            {
                CyberId = 1
            };

            // Act
            var result = roomsService.CreateNewRoom(usernameCrr, roomCreateViewModel);

            // Assert
            Assert.AreEqual(result.Result.Message.ToString(), "Cyber NOT exist");
        }

        [Fact]
        public void CreateNewOrder_IfSucces_ReturnRoomInCyber()
        {
            // Arrange
            var roomsService = new RoomsService();
            string usernameCrr = "dungba1";
            var roomCreateViewModel = new RoomCreateViewModel
            {
                CyberId = 20,
                RoomTypeName = "Phòng test",
                HardwareConfigId = 2,
                MaxX = 10,
                MaxY = 17,
                PriceRoom = 20000,
                RoomName = "Room Test",
                RoomPosition = "zzzzzzzz"
            };

            // Act
            var result = roomsService.CreateNewRoom(usernameCrr, roomCreateViewModel);

            // Assert
            Assert.IsNotNull(result);
        }

        [Fact]
        public void EditRoomInfor_NotLogin_ReturnNull()
        {
            // Arrange
            var roomsService = new RoomsService();
            string usernameCrr = "dungbafsdf1";
            var roomCreateViewModel = new RoomCreateViewModel
            {
                CyberId = 20,
                RoomTypeName = "Phòng test",
                HardwareConfigId = 2,
                MaxX = 10,
                MaxY = 17,
                PriceRoom = 20000,
                RoomName = "Room Test",
                RoomPosition = "zzzzzzzz"
            };

            // Act
            var result = roomsService.CreateNewRoom(usernameCrr, roomCreateViewModel);

            // Assert
            Assert.AreEqual(result.Result.Message.ToString(), "YOU NOT LOGIN");
        }

        [Fact]
        public void EditSizeRoom_NotLogin_ReturnNull()
        {
            // Arrange
            var roomsService = new RoomsService();
            string usernameCrr = "dungbafsdf1";
            int roomId = 1;
            int maxX = 10;
            int maxY = 10;

            // Act
            var result = roomsService.EditSizeRoom(usernameCrr, roomId, maxX, maxY);

            // Assert
            Assert.AreEqual(result.Result.Message.ToString(), "You NOT LOGIN");
        }

        [Fact]
        public void EditSizeRoomAllow_NotLogin_ReturnNull()
        {
            // Arrange
            var roomsService = new RoomsService();
            string usernameCrr = "dungbafsdf1";
            int roomId = 1;
            int maxX = 10;
            int maxY = 10;

            // Act
            var result = roomsService.EditSizeRoomAllow(usernameCrr, roomId, maxX, maxY);

            // Assert
            Assert.AreEqual(result.Result.Message.ToString(), "YOU NOT LOGIN");
        }

        [Fact]
        public void GetRoomById_IfRoomNotExist_ReturnNull()
        {
            // Arrange
            var roomsService = new RoomsService();
            int roomId = 13245;

            // Act
            var result = roomsService.GetRoomById(roomId);

            // Assert
            Assert.AreEqual(result.Result.Message.ToString(), "Room NOT exist");
        }

        [Fact]
        public void GetRoomById_IfSuccess_ReturnRoom()
        {
            // Arrange
            var roomsService = new RoomsService();
            int roomId = 2;

            // Act
            var result = roomsService.GetRoomById(roomId);

            // Assert
            Assert.IsNotNull(result);
        }

        [Fact]
        public void RemoveRoom_NotLogin_ReturnNull()
        {
            // Arrange
            var roomsService = new RoomsService();
            string usernameCrr = "dungba4235423";
            int roomId = 2;

            // Act
            var result = roomsService.RemoveRoom(usernameCrr, roomId);

            // Assert
            Assert.AreEqual(result.Result.Message.ToString(), "YOU NOT LOGIN");
        }

        [Fact]
        public void RemoveRoom_IfRemoveSuccess_ReturnStatus()
        {
            // Arrange
            var roomsService = new RoomsService();
            string usernameCrr = "dungba";
            int roomId = 14;

            // Act
            var result = roomsService.RemoveRoom(usernameCrr, roomId);

            // Assert
            Assert.AreEqual(result.Result.Message.ToString(), "Successful");
        }

        [Fact]
        public void RemoveRoom_IfRoomNotExist_ReturnNull()
        {
            // Arrange
            var roomsService = new RoomsService();
            string usernameCrr = "dungba";
            int roomId = 23124;

            // Act
            var result = roomsService.RemoveRoom(usernameCrr, roomId);

            // Assert
            Assert.AreEqual(result.Result.Message.ToString(), "Room NOT exist");
        }
    }
}
