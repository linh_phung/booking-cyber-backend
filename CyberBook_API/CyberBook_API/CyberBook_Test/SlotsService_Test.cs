﻿using CyberBook_API.Services;
using CyberBook_API.ViewModel.SlotViewModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;

namespace CyberBook_Test
{
    [TestClass]
    public class SlotsService_Test
    {
        [Fact]
        public void CreateNewSlot_IfCyberHasExist_ReturnNull()
        {
            // Arrange
            var slotsService = new SlotsService();
            string usernameCrr = "phunglinh@gm.com";
            var slotCreateViewModelIn = new SlotCreateViewModelIn
            {
                CyberId = 1,
                RoomId = 1,
                SlotDescription = "",
                SlotName = "",
                SlotPositionX = 10,
                SlotPositionY = 17,
            };

            // Act
            var result = slotsService.CreateNewSlot(usernameCrr, slotCreateViewModelIn);

            // Assert
            Assert.AreEqual("Cyber NOT exist", result.Result.Message.ToString());
        }

        [Fact]
        public void CreateNewSlot_IfPermisstion_ReturnNull()
        {
            // Arrange
            var slotsService = new SlotsService();
            string usernameCrr = "phunglinh@gm.com";
            var slotCreateViewModelIn = new SlotCreateViewModelIn
            {
                CyberId = 2,
                RoomId = 1,
                SlotDescription = "",
                SlotName = "",
                SlotPositionX = 10,
                SlotPositionY = 17,
            };

            // Act
            var result = slotsService.CreateNewSlot(usernameCrr, slotCreateViewModelIn);

            // Assert
            Assert.AreEqual("You NOT permission for this Function", result.Result.Message.ToString());
        }

        [Fact]
        public void EditSlot_IfRoomNotExist_ReturnNull()
        {
            // Arrange
            var slotsService = new SlotsService();
            string usernameCrr = "phunglinh@gm.com";
            var slotViewModelIn = new SlotViewModelIn
            {
                CyberId = 2,
                RoomId = 3
            };

            // Act
            var result = slotsService.EditSlot(usernameCrr, slotViewModelIn);

            // Assert
            Assert.AreEqual("You NOT permission for this Function", result.Result.Message.ToString());
        }

        [Fact]
        public void EditStatusSlot_IfNotPermission_ReturnNull()
        {
            // Arrange
            var slotsService = new SlotsService();
            string usernameCrr = "phunglinh@gm.com";
            int slotId = 1;
            int statusSlot = 1;

            // Act
            var result = slotsService.EditStatusSlot(usernameCrr, slotId, statusSlot);

            // Assert
            Assert.AreEqual("You NOT permission for this Function", result.Result.Message.ToString());
        }

        [Fact]
        public void EditStatusSlot_IfSuccess_EditSuccess()
        {
            // Arrange
            var slotsService = new SlotsService();
            string usernameCrr = "dungba";
            int slotId = 1237;
            int statusSlot = 4;

            // Act
            var result = slotsService.EditStatusSlot(usernameCrr, slotId, statusSlot);

            // Assert
            Assert.AreEqual("Successful", result.Result.Message.ToString());
        }

        [Fact]
        public void GetListSlotByRoom_IfSuccess_ReturnSuccess()
        {
            // Arrange
            var slotsService = new SlotsService();
            int pageIndex = 1;
            int pageSize = 10;
            int roomId = 1;

            // Act
            var result = slotsService.GetListSlotByRoom(pageIndex, pageSize, roomId);

            // Assert
            Assert.AreEqual("Successfully", result.Result.Message.ToString());
        }

        [Fact]
        public void GetListSlotByRoom_IfRoomNotExist_ReturnNull()
        {
            // Arrange
            var slotsService = new SlotsService();
            int pageIndex = 1;
            int pageSize = 10;
            int roomId = 100; //room not exist

            // Act
            var result = slotsService.GetListSlotByRoom(pageIndex, pageSize, roomId);

            // Assert
            Assert.AreEqual("Room NOT exist", result.Result.Message.ToString());
        }

        [Fact]
        public void GetListSlotByRoom_IfSuccess_ReturnList()
        {
            // Arrange
            var slotsService = new SlotsService();
            int pageIndex = 1;
            int pageSize = 10;
            int roomId = 4;

            // Act
            var result = slotsService.GetListSlotByRoom(pageIndex, pageSize, roomId);

            // Assert
            Assert.AreEqual("Successfully", result.Result.Message.ToString());
            Assert.IsNotNull(result.Result.Data);
        }

        [Fact]
        public void GetSlotById_IfSuccess_ReturnNull()
        {
            // Arrange
            var slotsService = new SlotsService();
            int slotId = 1;

            // Act
            var result = slotsService.GetSlotById(slotId);

            // Assert
            Assert.AreEqual("Successful", result.Result.Message.ToString());
            Assert.IsNotNull(result.Result.Data);
        }

        [Fact]
        public void GetSlotById_IfSlotNotExist_ReturnNull()
        {
            // Arrange
            var slotsService = new SlotsService();
            int slotId = 1000; // slotId not exist

            // Act
            var result = slotsService.GetSlotById(slotId);

            // Assert
            Assert.AreEqual("Slot NOT exist", result.Result.Message.ToString());
        }

        [Fact]
        public void RemoveSlot_IfRemoveSuccess_RemoveOk()
        {
            // Arrange
            var slotsService = new SlotsService();
            string usernameCrr = "dungba";
            int slotId = 1237;

            // Act
            var result = slotsService.RemoveSlot(usernameCrr, slotId);

            // Assert
            Assert.AreEqual("RemoveSlot OK", result.Result.Message.ToString());
        }

        [Fact]
        public void RemoveSlot_IfCannotRemove_ReturnNull()
        {
            // Arrange
            var slotsService = new SlotsService();
            string usernameCrr = "dungba";
            int slotId = 99999; // slotId not exist

            // Act
            var result = slotsService.RemoveSlot(usernameCrr, slotId);

            // Assert
            Assert.AreEqual("Slot NOT exist", result.Result.Message.ToString());
        }
    }
}
