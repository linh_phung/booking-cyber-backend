﻿using CyberBook_API.Services;
using CyberBook_API.ViewModel.OrderViewModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;

namespace CyberBook_Test
{
    [TestClass]
    public class ManageBookingService_Test
    {
        [Fact]
        public void CreateSlotHardwareConfig_IfNotLogin_ReturnNull()
        {
            // Arrange
            var manageBookingService = new ManageBookingService();
            string usernameCrr = "dungba1dgsbsfd";
            int orderId = 1;
            int statusOrder = 2;

            // Act
            var result = manageBookingService.ChangeStatusOrder(usernameCrr, orderId, statusOrder);

            // Assert
            Assert.AreEqual(result.Result.Message.ToString(), "YOU NOT LOGIN");
        }

        [Fact]
        public void CreateSlotHardwareConfig_IfCyberNotExist_ReturnNull()
        {
            // Arrange
            var manageBookingService = new ManageBookingService();
            string usernameCrr = "dungba1";
            int orderId = 1;
            int statusOrder = 2;

            // Act
            var result = manageBookingService.ChangeStatusOrder(usernameCrr, orderId, statusOrder);

            // Assert
            Assert.AreEqual(result.Result.Message.ToString(), "Order NOT exist");
        }

        [Fact]
        public void CreateNewOrder_IfNotLogin_ReturnNull()
        {
            // Arrange
            var manageBookingService = new ManageBookingService();
            string usernameCrr = "dungba1zaaa";
            var orderViewModelIn = new OrderViewModelIn
            {
                CyberId = 1342
            };

            // Act
            var result = manageBookingService.CreateNewOrder(usernameCrr, orderViewModelIn);

            // Assert
            Assert.AreEqual(result.Result.Message.ToString(), "You NOT Login");
        }

        [Fact]
        public void CreateNewOrder_IfCyberNotExist_ReturnNull()
        {
            // Arrange
            var manageBookingService = new ManageBookingService();
            string usernameCrr = "dungba1";
            var orderViewModelIn = new OrderViewModelIn
            {
                CyberId = 1
            };

            // Act
            var result = manageBookingService.CreateNewOrder(usernameCrr, orderViewModelIn);

            // Assert
            Assert.AreEqual(result.Result.Message.ToString(), "Cyber Not exist");
        }


    }
}
