﻿using CyberBook_API.Dal.Repositories;
using CyberBook_API.Enum;
using CyberBook_API.Models;
using CyberBook_API.Services;
using CyberBook_API.ViewModel.CyberViewModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;

namespace CyberBook_Test
{
    public class AdminCybersService_Test
    {
        [TestClass]
        public class AccountTest
        {
            private readonly AccountsRepository _accountsRepository = new AccountsRepository();
            private readonly CybersRepository _cybersRepository = new CybersRepository();
            private readonly AdminsRepository _adminsRepository = new AdminsRepository();
            private readonly UsersRepository _usersRepository = new UsersRepository();

            [Fact]
            public void AdminListCyber_ListCyberOut_ReturnList()
            {
                // Arrange
                var output = new ServiceResponse<IEnumerable<CyberViewModelOut>>();

                // Act
                var result = _cybersRepository.GetAll();

                // Assert
                Assert.IsTrue(result.Result.Count() > 0);
            }

            [Fact]
            public void AdminListCyber_CheckPermisstion_NotPermission()
            {
                // Arrange
                var output = new ServiceResponse<IEnumerable<CyberViewModelOut>>();

                // Act
                var result = _adminsRepository.IsSuperAdmin(1000);

                // Assert
                Assert.IsTrue(result.Result);
            }

            [Fact]
            public void AdminListCyber_CheckLogin_NotLogin()
            {
                // Arrange
                var output = new ServiceResponse<IEnumerable<CyberViewModelOut>>();
                var usernameCrr = "linhphung";

                // Act
                var result = _accountsRepository.GetAccountByUsername(usernameCrr);

                // Assert
                Assert.IsNull(output.Data);
            }

            [Fact]
            public void LockCyber_CheckLogin_NotLogin()
            {
                // Arrange
                var adminCybersService = new AdminCybersService();
                string usernameCrr = "";
                int cyberId = 1;

                // Act
                var result = adminCybersService.LockCyber(usernameCrr, cyberId);

                // Assert
                Assert.IsNull(result.Result.Data);
                Assert.AreEqual(result.Result.Message.ToString(), "YOU NOT LOGIN");
            }

            [Fact]
            public void RegisterCyber_CyberNotExist_CreateCyberUserAccount()
            {
                // Arrange
                var adminCybersService = new AdminCybersService();
                var cyberViewModel = new CyberViewModel
                {
                    CyberName = "Gaming X",
                    Address = "139 Đ. Cầu Giấy, Quan Hoa, Cầu Giấy, Hà Nội",
                    PhoneNumber = "024 3386 3939",
                    BossCyberName = "Nam Trung",
                    lat = "21.0318531",
                    lng = "105.7969269",
                    BusinessLicense = "zzzzzzzz",
                    CreatedDate = Convert.ToString(DateTime.Now),
                    Email = "namtrung1@gmail.com",
                    Password = "123",
                    Dob = DateTime.Now
                };

                // Act
                var result = adminCybersService.RegisterCyber(cyberViewModel);

                // Assert
                Assert.IsNotNull(result.Result.Data);
            }

            //[Fact]
            //public void RegisterCyber_EmailHasExist_CreateCyberUserAccount()
            //{
            //    // Arrange
            //    var adminCybersService = new AdminCybersService();
            //    var cyberViewModel = new CyberViewModel
            //    {
            //        CyberName = "Gaming X",
            //        Address = "139 Đ. Cầu Giấy, Quan Hoa, Cầu Giấy, Hà Nội",
            //        PhoneNumber = "024 3386 3939",
            //        BossCyberName = "Nam Trung",
            //        lat = "21.0318531",
            //        lng = "105.7969269",
            //        BusinessLicense = "zzzzzzzz",
            //        CreatedDate = Convert.ToString(DateTime.Now),
            //        Email = "phungduclinh.99@gmail.com",
            //        Password = "123",
            //        Dob = DateTime.Now
            //    };

            //    // Act
            //    var result = adminCybersService.RegisterCyber(cyberViewModel);

            //    // Assert
            //    Assert.IsNull(result.Result.Data);
            //    //Assert.Equals(result.Result.Message.ToString(), "Address or Email has exist");
            //}

            [Fact]
            public void RegisterCyber_NullAll_CreateCyberUserAccount()
            {
                // Arrange
                var adminCybersService = new AdminCybersService();
                var cyberViewModel = new CyberViewModel
                {
                    CyberName = null,
                    Address = null,
                    PhoneNumber = null,
                    BossCyberName = null,
                    lat = null,
                    lng = null,
                    BusinessLicense = null,
                    CreatedDate = null,
                    Email = null,
                    Password = null,
                    Dob = DateTime.Now
                };

                // Act
                var result = adminCybersService.RegisterCyber(cyberViewModel);

                // Assert
                Assert.IsNull(result.Result.Data);
                //Assert.Equals(result.Result.Message, "Address or Email has exist");
            }

            [Fact]
            public void RejectCyber_NotLogin_ReturnNull()
            {
                // Arrange
                var adminCybersService = new AdminCybersService();
                string usernameCrr = "";
                int cyberId = 1;

                // Act
                var result = adminCybersService.RejectCyber(usernameCrr, cyberId);

                // Assert
                Assert.IsNull(result.Result.Data);
            }

            [Fact]
            public void RejectCyber_NotPermission_ReturnNull()
            {
                // Arrange
                var adminCybersService = new AdminCybersService();
                int userId = 1084;

                // Act
                var result = _adminsRepository.IsSuperAdmin(userId);

                // Assert
                Assert.IsTrue(result.Result);
            }

            //[Fact]
            //public void RejectCyber_CyberHasExist_RejectSuccess()
            //{
            //    // Arrange
            //    int cyberId = 1;

            //    // Act
            //    var result = _adminsRepository.RejectRegisterCyber(cyberId);

            //    // Assert
            //    Assert.IsTrue(result.Result);
            //}

            [Fact]
            public void UnLockCyber_CheckLogin_NotLogin()
            {
                // Arrange
                var adminCybersService = new AdminCybersService();
                string usernameCrr = "";
                int cyberId = 1;

                // Act
                var result = adminCybersService.UnLockCyber(usernameCrr, cyberId);

                // Assert
                Assert.IsNull(result.Result.Data);
            }

            [Fact]
            public void VerifyCyber_CheckLogin_NotLogin()
            {
                // Arrange
                var adminCybersService = new AdminCybersService();
                string usernameCrr = "";
                int cyberId = 1;

                // Act
                var result = adminCybersService.VerifyCyber(usernameCrr, cyberId);

                // Assert
                Assert.IsNull(result.Result.Data);
                Assert.AreEqual(result.Result.Message.ToString(), "YOU NOT LOGIN");
            }

            [Fact]
            public void VerifyCyber_Success_ReturnCyber()
            {
                // Arrange
                var cyberId = 1000;

                // Act
                var result = _usersRepository.GetUserByUserID(cyberId);

                // Assert
                Assert.IsNull(result.Result);
            }

            [Fact]
            public void ViewDetailCyber_IfCyberExist_ReturnDetailCyber()
            {
                // Arrange
                var adminCybersService = new AdminCybersService();
                int cyberId = 1;

                // Act
                var result = adminCybersService.ViewDetailCyber(cyberId);

                // Assert
                Assert.IsNotNull(result.Result);
            }

        }
    }
}
